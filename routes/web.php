<?php

use App\Model\Product;
use App\Model\ProductItem;
use App\Model\ProductItemBox;
use App\Model\SaleDetail;

Route::get('/',function(){ return redirect()->route('login'); });

Route::get('/test',function(){
    $books_no = [];
    $gameCode = '90003';

    $count = 0;
    foreach(SaleDetail::where('game_code',$gameCode)->get() as $detail){
        $books_no[] = strlen($detail->code) == 20 ? substr($detail->code, 10,7) : substr($detail->code, 5, 7);
    }

    //foreach(ProductItem::where('game_code',$gameCode)->get() as $item){
    //    if($item->book_no != $books_no[$item->book_no]){
    //        echo $item->book_no;
    //    }
    //}

    ProductItem::where('game_code',$gameCode)->whereIn('book_no',$books_no)->update([
        'is_sold' => 1
    ]);

    return 'done';

    // ProductItem::where('game_code','90005')->whereIn('book_no',
    //     SaleDetail::where('game_code','90005')->pluck('book_no'))->update(['is_sale' => 1]);
});

Route::group(['namespace' => 'Admin\Auth'], function(){

    Route::get('login', ['as' => 'login', 'uses' => 'AuthController@showLoginForm']);
    Route::post('login', ['as' => 'login.post', 'uses' => 'AuthController@login']);
    Route::post('logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);
});

Route::group(['middleware' => 'auth', 'namespace' => 'Admin'],function(){
    Route::get('/language/set-locale/{locale}',['uses' => 'SystemSet\Language\LanguageController@setLocale', 'as' => 'language.set-locale']);

    Route::get('/dashboard',['uses' => 'Dashboard\DashboardController@index','as' => 'dashboard']);

    Route::resource('/product/stock','Product\StockController', ['as' => 'product']);
    Route::resource('/products','Product\ProductController');

    Route::group(['namespace' => 'Stock'], function () {
        Route::resource('/stocks','StockController');
        Route::get('/box/bar-code',['uses' => 'StockController@getBoxBarCode','as' => 'box.barcode']);
        Route::get('/stocklog-detail/{refNo}',['uses' => 'StockController@getStockLogDetailByRefNo','as' => 'stocklog.detail']);
        Route::get('/stock-download-pdf',['uses' => 'StockController@generatePdf', 'as' => 'stock.pdf.download']);
        Route::get('/stock-download-excel',['uses' => 'StockController@generateExcel', 'as' => 'stock.excel.download']);
        Route::get('/get-book-list',['uses' => 'StockController@getBookList','as' => 'stocklog.get-book-list']);
    });

    Route::group(['namespace' => 'Sale'], function () {
        Route::get('/sales/get-product-by-book-code', ['uses' => 'SaleController@getProductByBookCode', 'as' => 'sale.get-product']);
        Route::get('/sales/get-products-by-box-code', ['uses' => 'SaleController@getProductByBoxCode', 'as' => 'sale.get-products']);
        Route::resource('/sales','SaleController');
        Route::get('/sale-detail/{refNo}',['uses' => 'SaleController@getSaleDetailByRefNo','as' => 'sale.detail']);
        Route::get('/sale-download-pdf',['uses' => 'SaleController@generatePdf', 'as' => 'sale.pdf.download']);
        Route::get('/sale-download-excel',['uses' => 'SaleController@generateExcel', 'as' => 'sale.excel.download']);

    });

    Route::resource('/awards','Award\AwardController');

    Route::group(['namespace' => 'Manager'],function(){
        Route::resource('/managers/roles', 'ManagerRoleController',['as' => 'managers']);
        Route::resource('/managers/password','ManagerEditPasswordController',['as' => 'managers']);
        Route::resource('/managers', 'ManagerController');
    });

    Route::group(['namespace' => 'ScanAward'], function(){
        Route::get('scan-export', ['uses' => 'ScanAwardController@exportExcel', 'as' => 'scan-export']);
        Route::get('scan-award', ['uses' => 'ScanAwardController@index', 'as' => 'scan-award']);
        Route::get('scan-summary', ['uses' => 'ScanAwardController@getScanSummary', 'as' => 'scan-summary']);

        Route::get('scan-summary/download', ['uses' => 'ScanAwardController@downloadScanSummary', 'as' => 'download-scan-summary']);
    });

    Route::group(['namespace' => 'User'],function(){

        Route::group(['middleware' => ['permission:view-user-account']], function () {
            Route::resource('/users/accounts', 'AccountController',['as' => 'users']);
        });

        Route::group(['middleware' => ['permission:view-user-account|view-user-account-log']], function () {
            Route::resource('/users/sale-network', 'SaleNetworkController',['as' => 'users']);
        });

        Route::group(['middleware' => ['permission:view-user-account-log']], function () {
            Route::resource('/users/account/logs', 'AccountLogController',['as' => 'users']);
        });

        Route::get('/user/log/download/{userId}',['uses' => 'AccountLogController@download','as' => 'user.logs.download']);
    });

    Route::group(['namespace' => 'Purchase'],function(){
        Route::get('/purchase', ['uses' => 'PurchaseController@index', 'as' => 'purchase']);
        Route::get('scan-purchase/download', ['uses' => 'PurchaseController@downloadScanPurchase', 'as' => 'download-scan-purchase']);
    });

});

Route::get('/generate-barcode',function(){
    $game_code = 'P0001';
    $batch = '19001';
    $step = 50;
    $s_book_no = 1;

    $barcodes = [];

    for($i = 1; $i<=200;$i++){
        $box = str_pad($i, 5, '0', STR_PAD_LEFT);
        $s_book = str_pad($s_book_no, 7, '0', STR_PAD_LEFT);
        $e_book = str_pad($s_book_no + $step - 1, 7, '0', STR_PAD_LEFT);
        $s_book_no = $s_book_no + $step;

        $code = $game_code.$batch.$box.$s_book.$e_book; echo "<br>";
        echo $code.'<br>';
        // $barcodes[] =
        //     [
        //         'text' => $code,
        //         'size' => 30,
        //         'orientation' => 'horizontal',
        //         'code_type' => 'code39',
        //         'print' => true,
        //         'sizefactor' => 1,
        //         'filename' => $code.'.jpeg'
        //     ];
    }

    // $bar = App::make('BarCode');
    // foreach($barcodes as $barcode) {
    //     $barcontent = $bar->barcodeFactory()->renderBarcode(
    //                                 $text=$barcode["text"],
    //                                 $size=$barcode['size'],
    //                                 $orientation=$barcode['orientation'],
    //                                 $code_type=$barcode['code_type'], // code_type : code128,code39,code128b,code128a,code25,codabar
    //                                 $print=$barcode['print'],
    //                                 $sizefactor=$barcode['sizefactor'],
    //                                 $filename = $barcode['filename']
    //                         )->filename($barcode['filename']);

    //     echo '<img alt="testing" src="'.$barcontent.'"/>';  echo "<br>";
    // }

});

//** this route run the link to transfer return bonus card */
   Route::get('/bonus-transfer', ['uses' => 'API\Balance\BalanceController@transferBonus', 'as' => 'transfer-bonus']);
//** end */
