<?php

use Illuminate\Http\Request;
use App\Http\Services\OrderService;

Route::get('/','API\Auth\AuthController@login');

Route::post('login', 'API\Auth\AuthController@login');

Route::middleware('auth:api')->group( function () {
    Route::post('logout','API\Auth\AuthController@logout');
    Route::post('change-password', 'API\Auth\AuthController@changePassword');

    Route::group(['namespace' => 'API'],function(){

        Route::get('verify-award', 'Award\AwardController@verifyAward');
        Route::get('verify-award-v1', 'Award\AwardController@verifyAward');
        // Route::post('win-to-main', 'Award\AwardController@winToMain');
        // Route::get('balance','Award\AwardController@getBalance');
        // Route::get('transfer-log', 'Award\AwardController@getTransferLog');

        Route::get('get-username-by-account-number','Balance\BalanceController@getUsernameByAccountNumber');
        Route::post('win-to-main', 'Balance\BalanceController@winToMain');
        Route::get('balance','Balance\BalanceController@getBalance');
        Route::get('transfer-log', 'Balance\BalanceController@getTransferLog');
        Route::post('transfer-to-other-account', 'Balance\BalanceController@transferToOtherAccount');
        
        
        Route::get('scan-report', 'Report\ReportController@getAllScanned');
        Route::get('scan-detail', 'Report\ReportController@getScanDetail');

        Route::get('scan-purchase', 'Purchase\PurchaseController@cardPurchase');
        Route::get('purchase-report', 'Purchase\PurchaseController@getPurchaseReport');
    });
	
});


