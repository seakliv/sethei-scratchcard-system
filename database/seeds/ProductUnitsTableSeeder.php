<?php

use Illuminate\Database\Seeder;
use App\Model\ProductUnit;

class ProductUnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductUnit::create(['name' => 'Books', 'code' => 'bks']);
    }
}
