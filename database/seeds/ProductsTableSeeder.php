<?php

use Illuminate\Database\Seeder;
use App\Model\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'Kla Klok Sethei',
            'game_code' => 'P0001',
            'unit_id' => 1,
            'ticket_price' => 2500,
            'ticket_qty' => 100,
        ]);
    }
}
