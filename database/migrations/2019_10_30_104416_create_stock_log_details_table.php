<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockLogDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_log_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference_no',100)->index();
            $table->string('code');
            $table->string('game_code',5);
            $table->string('batch_no',5);
            $table->string('box_code',5);
            $table->string('start_book_no',7);
            $table->string('end_book_no',7);
            $table->integer('book_qty');
            $table->integer('unit_id');

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_log_details');
    }
}
