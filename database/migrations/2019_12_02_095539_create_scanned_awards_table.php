<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScannedAwardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scanned_awards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('award_level');
            $table->string('award_amount', 50);
            $table->string('security_code');
            $table->integer('user_id')->nullable()->index();
            $table->integer('award_id')->nullable()->index();
            $table->string('abstract')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scanned_awards');
    }
}
