<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code',22);
            $table->string('game_code',5)->index();
            $table->string('batch_no',5);
            $table->string('box_code',5);
            $table->string('book_no',7);
            $table->integer('ticket_qty')->default(0);
            $table->double('ticket_price')->default(0.0);
            $table->boolean('is_sold')->default(FALSE);
            $table->boolean('is_activated')->default(FALSE);
            $table->integer('activated_by')->nullable();
            $table->integer('customer_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_items');
    }
}
