<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAwardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('awards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('is_claimed')->default(FALSE)->index();
            $table->integer('claimed_by')->nullable()->index();
            $table->integer('imported_by')->nullable();

            $table->char('game_code',5)->index();
            $table->string('batch_no',5);
            $table->char('book_no',7);
            $table->string('ticket_no',3);
            $table->string('award_verification_code');
            $table->string('box_barcode');
            $table->string('logistic_code',50);
            $table->string('security_code');
            $table->unsignedTinyInteger('award_level');
            $table->string('award_amount',50);
            $table->char('a1',2);
            $table->char('a2',2);
            $table->char('a3',2);
            $table->char('b1',2);
            $table->char('b2',2);
            $table->char('b3',2);
            $table->char('b4',2);
            $table->char('b5',2);
            $table->char('b6',2);
            $table->string('m1',50);
            $table->string('m2',50);
            $table->string('m3',50);
            $table->string('m4',50);
            $table->string('m5',50);
            $table->string('m6',50);
            $table->string('z1',50);
            $table->timestamp('scanned_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('awards');
    }
}
