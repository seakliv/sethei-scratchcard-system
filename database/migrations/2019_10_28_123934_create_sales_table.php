<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference_no',100)->nulllable()->index();
            $table->integer('total_product_qty')->default(0);
            $table->double('grand_price')->default(0.0);
            $table->double('total_price')->default(0.0);
            $table->double('discount')->default(0.0);

            $table->integer('customer_id')->nullable();
            $table->timestamp('sale_date')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
