<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductItemBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_item_boxes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code',29);
            $table->string('game_code',5)->index();
            $table->string('batch_no',5);
            $table->string('box_code',5);
            $table->string('start_book_no',7);
            $table->string('end_book_no',7);
            $table->integer('book_qty');

            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_item_boxes');
    }
}
