<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference_no',100)->nulllable()->index();
            $table->string('code');
            $table->string('game_code',5);
            $table->string('batch_no',5);
            $table->string('box_code',5);
            $table->string('book_no',7);
            $table->double('price')->default(0.0);
            $table->integer('ticket_qty')->default(0);
            $table->double('ticket_price')->default(0.0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_details');
    }
}
