<?php

namespace App\Console\Commands\Import;

use Illuminate\Console\Command;
use App\Model\SystemAccount as modelSystemAccount;
use DB;

class SystemAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:systemAccount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::connection('mysql_5d')->table('d_system_accounts')->orderBy('id','asc')->chunk(100,function($accounts){
            $data = [];
            foreach($accounts as $row){
                $data[] = [
                    'id' => $row->id,
                    'type_id' => $row->type_id,
                    'category_id' => $row->category_id,
                    'title' => $row->title,
                    'number' => $row->number,
                    'name' => $row->name,
                    'balance' => $row->balance,
                    'sort' => $row->sort,
                    'state' => $row->state,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => $row->updated_at != 0 ? $row->updated_at : \Carbon\Carbon::now(),
                ];
            }
            modelSystemAccount::insert($data);
        });
    }
}
