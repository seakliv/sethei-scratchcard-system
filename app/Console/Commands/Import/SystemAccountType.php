<?php

namespace App\Console\Commands\Import;

use Illuminate\Console\Command;
use DB;
use App\Model\SystemAccountType as systemAccountTypeModel;

class SystemAccountType extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:systemAccountType';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = [
            'Cash account',
            'Bank account',
            'Third party account'
        ];
        foreach($data as $row){
            systemAccountTypeModel::create([
                'name' => $row
            ]);
        }
    }
}
