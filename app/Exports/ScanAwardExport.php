<?php

namespace App\Exports;

use App\Model\Sale;
use App\Model\SaleDetail;
use App\Model\Product;
use App\Model\ScannedAward;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use \Maatwebsite\Excel\Sheet;


class ScanAwardExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = [];
        $scannedAwards = ScannedAward::orderBy('id', 'DESC')
                ->when(request()->award_level, function($q){
                    $q->where('award_level', request()->award_level);
                })
                ->when(request()->ticket_number, function($q){
                    $q->where('security_code', 'like', request()->ticket_number.'%');
                })
                ->when(request()->award_label, function($q){
                    $q->where('abstract', request()->award_label);
                })
                ->when(request()->customer_id, function($q){
                    $q->where('user_id', request()->customer_id);
                })
                ->when(request()->started_date && request()->ended_date, function($q){
                    $q->whereBetween('created_at',[request()->started_date,request()->ended_date]);
                })
                ->get();

        foreach($scannedAwards as $index => $scannedAwards){
                $data[] = array(
                    $scannedAwards->customer ? $scannedAwards->customer->username : '',
                    substr($scannedAwards->security_code,0,20),
                    $scannedAwards->award_amount,
                    $scannedAwards->award_level,
                    $scannedAwards->abstract,
                    $scannedAwards->created_at,
                );
        }

        $collection = collect($data);
        return $collection;
    }

    public function getStructureData(){

    }

    public function headings(): array
    {
        return [
            'Customer Name',
            'Ticket No',
            'Award Amount',
            'Award Level',
            'Label',
            'Scanned Time',
        ];
    }

    public function registerEvents(): array
    {
        return [

            Sheet::macro('mergeCells', function (Sheet $sheet, string $cellRange) {
                $sheet->getDelegate()->mergeCells($cellRange);
            }),

            Sheet::macro('setOrientation', function (Sheet $sheet, $orientation) {
                $sheet->getDelegate()->getPageSetup()->setOrientation($orientation);
            }),

            Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
                $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
            }),

            BeforeExport::class  => function(BeforeExport $event) {
                $event->writer->setCreator('Patrick');
            },

            AfterSheet::class => function(AfterSheet $event) {

                $event->sheet->styleCells(
                    'A1:M1',
                    [
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],

                        'font' => array(
                            'name'      =>  'Calibri',
                            'size'      =>  12,
                            'bold'      =>  true,
                            'color' => ['argb' => 'FFFFFFFF'],

                        ),

                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => '00000000']
                        ]

                    ]
                );
            },
        ];
    }
}
