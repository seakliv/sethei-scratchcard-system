<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use \Maatwebsite\Excel\Sheet;
use App\Model\sale;
use App\Model\Customer;
class CustomerExport implements FromCollection , WithHeadings,ShouldAutoSize,WithEvents
{
    use Exportable;

    public $sheetData;

    public function __construct(){
	    $this->sheetData = [];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $this->sheetData = collect($this->getStructureData());
        return $this->sheetData;
    }
    public function getStructureData(){

       $data = [];
       $cus_id = request()->id;

       if($cus_id){

        $sale = Sale::with('saleDetails','customer','user')->where('customer_id',$cus_id)->first();
       
        foreach($sale->saleDetails as $index => $detail){
            $data [] = [
                'code' =>  $index == 0 && isset($sale->customer) ? $sale->customer->code : '',
                'username' => $index == 0 && isset($sale->customer) ? $sale->customer->username : '', 
                'phone' => $index == 0 && isset($sale->customer) ?  $sale->customer->phone : '',
                'province' => $index == 0 && isset($sale->customer->province) ? $sale->customer->province->name : '',
                'district' => $index == 0 && isset($sale->customer->province) ? $sale->customer->district->name : '',
                'address' => $index == 0 && isset($sale->customer) ? $sale->customer->address : '',
                'sale by' => $index == 0 && isset($sale->user) ? $sale->user->username : '',
                'reference_no' => $index == 0 ? $sale->reference_no : '',
                'game code' => isset($detail) ? $detail->game_code : '',
                'game name' => isset($detail) ? $detail->product->name : '',
                'batch no' => isset($detail) ? $detail->batch_no : '',
                'box code' => isset($detail) ? $detail->box_code : '',
                'book no' => isset($detail) ? $detail->book_no : '',
                'price' => isset($detail) ? $detail->price : ''
               
                ];
            }
       }else {

        $customers = Customer::orderBy('code','ASC')
                ->when(request()->code, function($q){
                    $q->where('code',request()->code);
                })
                ->when(request()->username, function($q){
                    $q->where('username','like', '%'.request()->username.'%');
                })
                ->when(request()->province_id, function($q){
                    $q->where('province_id', request()->province_id);
                })
                ->when(request()->state != '' &&  request()->state != 'any', function($q){
                    $q->where('state',request()->state);
                })->get();

                foreach($customers as $customer){
                    $sale = Sale::with('saleDetails','customer','user')->where('customer_id',$customer->id)->get();
                    foreach($sale as $detail){
                        foreach($detail->saleDetails as $index => $item){
                            $data [] = [
                                'code' =>  $index == 0 && $customer->code ? $customer->code : '' ,
                                'username' => $index == 0 && $customer->username ? $customer->username : '', 
                                'phone' => $index == 0 && $customer->phone ? $customer->phone : '',
                                'province' => $index == 0 && isset($customer->province) ? $customer->province->name : '',
                                'district' => $index == 0 && isset($customer->district) ? $customer->district->name : '',
                                'address' => $index == 0 && $customer->address ? $customer->address : '' ,
                                'sale by' => $index == 0 && $detail->user ? $detail->user->username : '',
                                'reference_no' => $index == 0 ? $detail->reference_no : '',
                                'game code' => isset($item) ? $item->game_code : '',
                                'game name' => isset($item) ? $item->product->name : '',
                                'batch no' => isset($item) ? $item->batch_no : '',
                                'box code' => isset($item) ? $item->box_code : '',
                                'book no' => isset($item) ? $item->book_no : '',
                                'price' => isset($item) ? $item->price : ''
                            
                                ];
                            }
                        }
                    }
       
        }
        
       return $data;
    }

    public function headings(): array
    { 
        $column_name = [
            'Code','UserName','Phone' , 'Province','District', 'Address', 'Sale By','Reference No', 'Game Code', 'Game Name', 'Batch No',
            'Box Code', 'Book No','Price'
        ];

        return $column_name;
    }

    public function registerEvents(): array
    {
        return [
            
            Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
                $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
            }),

            AfterSheet::class => function(AfterSheet $event) {

                $event->sheet->styleCells(
                    'A1:N1',
                    [
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                       
                        'font' => array(
                            'name'      =>  'Calibri',
                            'size'      =>  12,
                            'bold'      =>  true,
                            'color' => ['argb' => 'FFFFFFFF'],
                            
                        ),
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => '00000000']
                        ]

                    ]
                );

                // dd($this->sheetData);
                // $event->sheet->getDelegate()->mergeCells('A2:A3')->mergeCells('A4:A5');

                // $event->sheet->getDelegate()->setMergeCells(['A'.$row.':A'.$row1,'B'.$row.':B'.$row1,'C'.$row.':C'.$row1]);
            },
        ];
    }
}
