<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Model\StockLog;
use \Maatwebsite\Excel\Sheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;


class StockExport implements WithMultipleSheets
{
    use Exportable;

    public function sheets(): array
    {
        return [
            new StockSummaryExport(),
            new StockDetailExport(),
        ];
    }
}

class StockSummaryExport implements FromCollection, WithHeadings,ShouldAutoSize,WithEvents, WithTitle{

    public function collection()
    {
        $collection = collect($this->getStructureData());

        return $collection;
    }

    public function getStructureData(){

        $data = [];

        $stockLog = StockLog::with('user','stockLogDetails')
            ->when(request()->reference_no, function($q){
                $q->where('reference_no',request()->reference_no);
            })
            ->when(request()->started_date && request()->ended_date, function($q){
                $q->whereBetween('stock_in_date',[request()->started_date,request()->ended_date]);
            })
            ->when(request()->created_by && request()->created_by != 'any', function($q){
                $q->where('created_by',request()->created_by);
            })->get();


        foreach($stockLog as $detail){
            foreach($detail->stockLogDetails as $key => $log){
                $data[]= array(

                    'Reference_No' => $key == 0 ? $detail->reference_no : '',
                    'Stock In Date' => $key == 0 ? $detail->stock_in_date : '',
                    'Stock In By' => $key == 0 ? $detail->user->username : '',
                    'Serial Code' => isset($log) ? $log->game_code.$log->batch_no.$log->box_code.$log->start_book_no.$log->end_book_no : '',
                    'Game Code'   => isset($log) ? $log->game_code : '',
                    'Batch No' => isset($log) ? $log->batch_no : '',
                    'Box Code' => isset($log) ? $log->box_code : '',
                    'Start Book No' => isset($log) ? $log->start_book_no : '',
                    'End Book No' => isset($log) ? $log->end_book_no : '',
                    'Qty' => isset($log) ? $log->book_qty : '0',

                );
            }
        }

        return $data;

    }

    public function headings(): array
    {
        $column_name = [ 'Reference No', 'Stock In Date', 'Stock In By' , 'Serial Code', 'Game Code' , 'Batch No', 'Box Code', 'Start Book No',
                         'End Book No', 'Qty' ];

        return $column_name;

    }

    public function registerEvents(): array
    {
        return [

            Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
                $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
            }),

            AfterSheet::class => function(AfterSheet $event) {

                $event->sheet->styleCells(
                    'A1:J1',
                    [
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                        'font' => array(
                            'name'      =>  'Calibri',
                            'size'      =>  12,
                            'bold'      =>  true,
                            'color' => ['argb' => 'FFFFFFFF'],

                        ),
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => '00000000']
                        ]

                    ]
                );

                //     $event->sheet->getDelegate()->setMergeCells(['A'.$row.':A'.$row1,'B'.$row.':B'.$row1,'C'.$row.':C'.$row1]);
            },
        ];
    }

    public function title(): string
    {
        return 'Stock Summary';
    }
}

class StockDetailExport implements FromCollection, WithHeadings,ShouldAutoSize,WithEvents, WithTitle{

    public function collection()
    {
        $collection = collect($this->getStructureData());

        return $collection;
    }

    public function getStructureData(){

        $data = [];

        $stockLog = StockLog::with('user','stockLogDetails')
                            ->when(request()->reference_no, function($q){
                                $q->where('reference_no',request()->reference_no);
                            })
                            ->when(request()->started_date && request()->ended_date, function($q){
                                $q->whereBetween('stock_in_date',[request()->started_date,request()->ended_date]);
                            })
                            ->when(request()->created_by && request()->created_by != 'any', function($q){
                                $q->where('created_by',request()->created_by);
                            })->get();


        foreach($stockLog as $detail){
            foreach($detail->stockLogDetails as $key => $log){
                foreach($log->productItems as $index => $item){
                    $data[]= array(

                        'Reference No' => $key == 0 && $index == 0 ? $detail->reference_no : '',
                        'Stock In Date' => $key == 0 && $index == 0 ? $detail->stock_in_date : '',
                        'Stock In By' => $key == 0 && $index == 0 ? $detail->user->username : '',
                        'Serial Code' => $index == 0 ? $log->game_code.$log->batch_no.$log->box_code.$log->start_book_no.$log->end_book_no : '',
                        'Book Serial Code'   => isset($log) ? $item->code : '',
                        'Book No' => isset($log) ? $item->book_no : '',
                        'Ticket Qty' => isset($log) ? $item->ticket_qty : '',
                        'Ticket Price' => isset($log) ? $item->ticket_price : '',

                    );
                }
            }
        }

        return $data;

    }

    public function headings(): array
    {
        $column_name = [ 'Reference No', 'Stock In Date', 'Stock In By' , 'Serial Code', 'Book Serial Code' , 'Book No', 'Ticket Qty', 'Ticket Price'];

        return $column_name;

    }

    public function registerEvents(): array
    {
        return [

            Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
                $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
            }),

            AfterSheet::class => function(AfterSheet $event) {

                $event->sheet->styleCells(
                    'A1:F1',
                    [
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],
                        'font' => array(
                            'name'      =>  'Calibri',
                            'size'      =>  12,
                            'bold'      =>  true,
                            'color' => ['argb' => 'FFFFFFFF'],

                        ),
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => '00000000']
                        ]

                    ]
                );
            },
        ];
    }

    public function title(): string
    {
        return 'Stock Detail';
    }
}
