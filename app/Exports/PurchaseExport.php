<?php

namespace App\Exports;

use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use \Maatwebsite\Excel\Sheet;
use App\Model\Purchase;

class PurchaseExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = [];
        $purchases = Purchase::with('user')

                                ->when(request()->game_code, function($q){
                                        $q->where('game_code', request()->game_code);
                                    })
                                ->when(request()->user_id, function($q){
                                        $q->where('user_id', request()->user_id);
                                    })
                                ->when(request()->batch_no, function($q){
                                        $q->where('batch_no', request()->batch_no);
                                    })
                                ->when(request()->book_no, function($q){
                                        $q->where('book_no', request()->book_no);
                                    })
                                ->when(request()->started_date && request()->ended_date, function($q){
                                        $q->whereBetween('created_at',[request()->started_date,request()->ended_date]);
                                })
                                ->get();
                        
      //  dd($scannedAwards);
        foreach($purchases as $index => $purchase){

                $data[] = array(
                    $purchase->user ? $purchase->user->username : '',
                    $purchase->user ? $purchase->user->account_number : '',
                    $purchase->user ? $purchase->user->province : '',
                    $purchase->game_code,
                    $purchase->batch_no ,
                    $purchase->book_no,
                    $purchase->serial_no,
                    $purchase->depo_name,
                    $purchase->created_at

                );
        }

        $collection = collect($data);
        return $collection;
    }

    public function getStructureData(){

    }

    public function headings(): array
    {
        return [
            'Freelancer Name',
            'Account Number',
            'Province',
            'Game Code',
            'Batch No',
            'Book No',
            'Serial No',
            'Depo Name',
            'Scanned At'
        ];
    }

    public function registerEvents(): array
    {
        return [

            Sheet::macro('mergeCells', function (Sheet $sheet, string $cellRange) {
                $sheet->getDelegate()->mergeCells($cellRange);
            }),

            Sheet::macro('setOrientation', function (Sheet $sheet, $orientation) {
                $sheet->getDelegate()->getPageSetup()->setOrientation($orientation);
            }),

            Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
                $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
            }),

            BeforeExport::class  => function(BeforeExport $event) {
                $event->writer->setCreator('Patrick');
            },

            AfterSheet::class => function(AfterSheet $event) {
            },
        ];
    }
}
