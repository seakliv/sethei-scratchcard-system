<?php

namespace App\Exports;

use App\Model\Sale;
use App\Model\SaleDetail;
use App\Model\Product;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use \Maatwebsite\Excel\Sheet;


class SaleExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = [];
        $sales = Sale::orderBy('reference_no', 'DESC')
                ->when(request()->reference_no, function($q){
                    $q->where('reference_no', request()->reference_no);
                })
                ->when(request()->customer_id, function($q){
                    $q->where('customer_id', request()->customer_id);
                })
                ->when(request()->started_date && request()->ended_date, function($q){
                    $q->whereBetween('sale_date',[request()->started_date,request()->ended_date]);
                })
                ->get();

        foreach($sales as $index => $sale){
            foreach($sale->saleDetails as $key => $item){
                $data[] = array(
                    $key == 0 ? $sale->reference_no : '',
                    $key == 0 ? $sale->sale_date : '',
                    $key == 0 ? $sale->customer->username : '',
                    $key == 0 ? $sale->discount : '',
                    $item->code,
                    $item->game_code,
                    $item->batch_no,
                    $item->box_code,
                    $item->book_no,
                    1,
                    $item->ticket_qty,
                    $item->ticket_price,
                    $item->price
                );
            }
        }

        $collection = collect($data);
        return $collection;
    }

    public function getStructureData(){

    }

    public function headings(): array
    {
        return [
            'Reference No',
            'Sale Date',
            'Customer Name',
            'Invoice Discount (%)',
            'Serial Code',
            'Game Code',
            'Batch No',
            'Box Code',
            'Book No',
            'Book Qty',
            'Ticket Qty',
            'Ticket Price',
            'Total Price'

        ];
    }

    public function registerEvents(): array
    {
        return [

            Sheet::macro('mergeCells', function (Sheet $sheet, string $cellRange) {
                $sheet->getDelegate()->mergeCells($cellRange);
            }),

            Sheet::macro('setOrientation', function (Sheet $sheet, $orientation) {
                $sheet->getDelegate()->getPageSetup()->setOrientation($orientation);
            }),

            Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
                $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
            }),

            BeforeExport::class  => function(BeforeExport $event) {
                $event->writer->setCreator('Patrick');
            },

            AfterSheet::class => function(AfterSheet $event) {

                $event->sheet->styleCells(
                    'A1:M1',
                    [
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        ],

                        'font' => array(
                            'name'      =>  'Calibri',
                            'size'      =>  12,
                            'bold'      =>  true,
                            'color' => ['argb' => 'FFFFFFFF'],

                        ),

                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => '00000000']
                        ]

                    ]
                );
            },
        ];
    }
}
