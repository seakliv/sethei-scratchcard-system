<?php

namespace App\Exports;

use App\Model\Sale;
use App\Model\SaleDetail;
use App\Model\Product;
use App\Model\ScannedAward;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use \Maatwebsite\Excel\Sheet;


class ScanSummaryExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = [];
        $scannedAwards = ScannedAward::with('customer')

                                ->when(request()->award_level, function($q){
                                        $q->where('award_level', request()->award_level);
                                    })
                                ->when(request()->customer_id, function($q){
                                        $q->where('user_id', request()->customer_id);
                                    })
                                ->when(request()->award_label, function($q){
                                        $q->where('abstract', request()->award_label);
                                    })
                                ->when(request()->ticket_number, function($q){
                                        $q->where('security_code', 'like', request()->ticket_number.'%');
                                    })
                                ->when(request()->started_date && request()->ended_date, function($q){
                                        $q->whereBetween('created_at',[request()->started_date,request()->ended_date]);
                                })
                                ->select(
                                    'user_id',
                                    DB::raw('count(id) as total_ticket'),
                                    DB::raw('sum(award_amount) as total_amount')
                                )
                                ->groupBy('user_id')
                                ->get();

        foreach($scannedAwards as $index => $scannedAwards){
                $data[] = array(
                    $scannedAwards->customer ? $scannedAwards->customer->username : '',
                    $scannedAwards->customer ? $scannedAwards->customer->account_number : '',
                    $scannedAwards->customer ? $scannedAwards->customer->province : '',
                    $scannedAwards->total_ticket,
                    $scannedAwards->total_amount,
                    $scannedAwards->total_amount / 4000,
                );
        }

        $collection = collect($data);
        return $collection;
    }

    public function getStructureData(){

    }

    public function headings(): array
    {
        return [
            'Customer Name',
            'Account Number',
            'Province',
            'Qty',
            'Total (R)',
            'Total ($)',
        ];
    }

    public function registerEvents(): array
    {
        return [

            Sheet::macro('mergeCells', function (Sheet $sheet, string $cellRange) {
                $sheet->getDelegate()->mergeCells($cellRange);
            }),

            Sheet::macro('setOrientation', function (Sheet $sheet, $orientation) {
                $sheet->getDelegate()->getPageSetup()->setOrientation($orientation);
            }),

            Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
                $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
            }),

            BeforeExport::class  => function(BeforeExport $event) {
                $event->writer->setCreator('Patrick');
            },

            AfterSheet::class => function(AfterSheet $event) {
            },
        ];
    }
}
