<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Model\SystemAccountLog;



class SystemAccountLogExport implements FromView ,ShouldAutoSize, WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $log_type = request()->log_type ?? 'all';
        $begin_time = request()->begin_time ?? '';
        $end_time = request()->end_time ?? '';
        $account = request()->account ?? 'all';
        $s = microtime(true);
        $logs = SystemAccountLog::with('manager','account','toSystemAccount','toUser'); 
      
        if($log_type!='all'){
            $logs->where('log_type',$log_type);
        }
        if($account != 'all'){
            $logs->where('account_id',$account);
        }
        if($begin_time && $end_time){
            $logs->whereBetween('created_at',[$begin_time,$end_time]);
        }
        $logs = $logs->orderBy('created_at','DESC')->get();

        return view('finance.log.export-system-account-log', [
            'logs' => $logs
        ]);

    }
    
    public function columnFormats(): array
    {
        return [];
    }

}
