<?php
namespace App\Constants;

class ScratchCard {
    const SECURITY_KEY = "U2V0aGVpIExvdHRlcnkgQ2FtYm9kaWE=";

    const SECURITY_CODE_LENGTH = 30;
    const GAME_CODE_LENGTH = 5;
    const BATCH_NO_LENGTH = 5;
    const BOX_CODE_LENGTH = 5;
    const BOOK_NO_LENGTH = 7;
    const TICKET_NO_LENGTH = 3;
    const AWARD_CODE_LENGTH = 10;

    public static function getDetailBoxBarCode($code = null){
        $detail = [];

        if($code && strlen($code) == 29){
            $detail = [
                'game_code' => (new self)->getBoxBarCode($code,'game_code'),
                'batch_no' => (new self)->getBoxBarCode($code,'batch_no'),
                'box_code' => (new self)->getBoxBarCode($code,'box_code'),
                'start_book_no' => (new self)->getBoxBarCode($code,'start_book_no'),
                'end_book_no' => (new self)->getBoxBarCode($code,'end_book_no')
            ];
        }

        return $detail;
    }

    public static function getDetailSecurityCode($code = null){
        $detail = [];

        if($code && strlen($code) == 30){
            $detail = [
                'game_code' => (new self)->getSecurityCode($code,'game_code'),
                'batch_no' => (new self)->getSecurityCode($code,'batch_no'),
                'book_no' => (new self)->getSecurityCode($code,'book_no'),
                'ticket_no' => (new self)->getSecurityCode($code,'ticket_no'),
                'award_verification_code' => (new self)->getSecurityCode($code,'award_code')
            ];
        }

        return $detail;
    }

    private function getSecurityCode($code,$key){
        $str = '';
        switch($key){
            case "game_code": $str = substr($code,0,self::GAME_CODE_LENGTH); break;

            case "batch_no": $str = substr($code,5,self::BATCH_NO_LENGTH); break;

            case "book_no": $str = substr($code,10,self::BOOK_NO_LENGTH); break;

            case "ticket_no": $str = substr($code,17,self::TICKET_NO_LENGTH); break;

            case "award_code": $str = substr($code,20,self::AWARD_CODE_LENGTH); break;

            default: $str = ""; break;
        }
        return $str;
    }

    private function getBoxBarCode($code,$key){
        $str = '';
        switch($key){
            case "game_code": $str = substr($code,0,self::GAME_CODE_LENGTH); break;

            case "batch_no": $str = substr($code,5,self::BATCH_NO_LENGTH); break;

            case "box_code": $str = substr($code,10,self::BOX_CODE_LENGTH); break;

            case "start_book_no": $str = substr($code,15,self::BOOK_NO_LENGTH); break;

            case "end_book_no": $str = substr($code,22,self::BOOK_NO_LENGTH); break;

            default: $str = ""; break;
        }
        return $str;
    }

    public static function encryptSecurityCode($code = null){
        $encryptedCode = '';

        if($code){
            $encryptedCode = md5($code.self::SECURITY_KEY);
        }

        return $encryptedCode;
    }


    //---- Use this for checking book-number for game ---///
    public static function getDetailBookCode($code = null){
        $detail = [];
// dd(strlen($code));
        if($code && (strlen($code) == 20 || strlen($code) == 15)){
            $detail = [
                'game_code' => (new self)->getBookCode($code,'game_code'),
                'batch_no' => (new self)->getBookCode($code,'batch_no'),
                'book_no' => (new self)->getBookCode($code,'book_no'),
            ];
        }

        return $detail;
    }

    private function getBookCode($code,$key){
        $str = '';
        switch($key){
            case "game_code": $str = substr($code,0,self::GAME_CODE_LENGTH); break;

            case "batch_no": $str = substr($code,5,self::BATCH_NO_LENGTH); break;

            case "book_no": $str = substr($code,10,self::BOOK_NO_LENGTH); break;

            default: $str = ""; break;
        }
        return $str;
    }
     //---- end ----//
}
