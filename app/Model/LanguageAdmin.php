<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LanguageAdmin extends Model
{
    protected $connection = 'mysql_5d';
    protected $fillable = [
        'id',
        'module',
        'english',
        'chinese',
        'khmer',
        'label',
        'abstract',
    ];
}
