<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductSoldItem extends Model
{
    protected $fillable = [
        'game_code', 'batch_no', 'box_code', 'book_no', 'is_activated', 'activated_by', 'customer_id'
    ];
}
