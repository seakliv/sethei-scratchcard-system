<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Customer;
use App\User;

class Sale extends Model
{
    protected $fillable = [
        'reference_no', 'total_product_qty', 'grand_price', 'discount', 'total_price', 'customer_id', 'sale_date', 'created_by', 'updated_by'
    ];

    // public function customer(){
    //     return $this->belongsTo(Customer::class, 'customer_id');
    // }

    public function customer(){
        return $this->belongsTo(User::class, 'customer_id');
    }

    public function saleDetails(){
        return $this->hasMany(SaleDetail::class, 'reference_no', 'reference_no');
    }
    public function user(){
        return $this->belongsTo(Manager::class,'created_by');
    }
}
