<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StockLogDetail extends Model
{
    protected $fillable = [

        'reference_no','code','game_code','batch_no','box_code','start_book_no','end_book_no','book_qty',
        'unit_id','created_by','updated_by'
    ];

    public function product_unit(){
        return $this->belongsTo(ProductUnit::class,'unit_id','id');
    }
    public function product(){
        return $this->belongsTo(Product::class,'game_code','game_code');
    }
    public function productItems(){
        return $this->hasMany(ProductItem::class,'game_code','game_code')->whereBatchNo($this->batch_no)->whereBoxCode($this->box_code);
    }
}
