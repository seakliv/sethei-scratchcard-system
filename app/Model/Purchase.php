<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Purchase extends Model
{
    protected $fillable = [
        'id',
        'game_code',
        'batch_no',
        'book_no',
        'serial_no',
        'user_id',
        'depo_name'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');         
    }
}
