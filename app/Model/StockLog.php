<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StockLog extends Model
{
    protected $fillable = [
        'reference_no', 'stock_in_date', 'total_product_qty', 'total_book_qty', 'created_by', 'updated_by',
    ];

    public function user(){
        return $this->belongsTo(Manager::class,'created_by');
    }

    public function product(){
        return $this->belongsTo(Product::class,'game_code','game_code');
    }

    public function stockLogDetails(){
        return $this->hasMany(StockLogDetail::class,'reference_no','reference_no');
    }

}

