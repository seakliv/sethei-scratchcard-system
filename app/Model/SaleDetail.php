<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SaleDetail extends Model
{
    protected $fillable = [
        'reference_no', 'code', 'game_code', 'batch_no', 'box_code', 'book_no', 'price', 'ticket_qty', 'ticket_price', 'created_by', 'updated_by'
    ];

    public function sale(){
        return $this->belongsTo(Sale::class);
    }

    public function product(){
        return $this->belongsTo(Product::class,'game_code','game_code');
    }
}
