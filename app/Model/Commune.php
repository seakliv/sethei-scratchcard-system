<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{
    protected $connection = 'mysql_5d';
    protected $fillable = [
        'id',
        'district_id',
        'code',
        'name'
    ];

    public function district(){
        return $this->belongsTo(District::class);
    }
}
