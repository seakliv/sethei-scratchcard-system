<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $connection = 'mysql_5d';
    protected $fillable = [
        'id',
        'name',
        'symbol',
        'unit',
    ];
}
