<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'id', 'name', 'game_code', 'unit_id', 'ticket_price', 'ticket_qty',
        'created_by', 'updated_by'
    ];

    public function product_unit(){
        return $this->belongsTo(ProductUnit::class,'unit_id','id');
    }

    public function product_items(){
        return $this->hasMany(ProductItem::class,'game_code','game_code');
    }

    public function available_product_items(){
        return $this->hasMany(ProductItem::class,'game_code','game_code')->whereIsActivated(FALSE)->whereIsSold(FALSE);
    }
}
