<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SystemAccountType extends Model
{
    protected $fillable = [
        'id',
        'name'
    ];
}
