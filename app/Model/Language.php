<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $connection = 'mysql_5d';
    protected $fillable = [
        'id',
        'language',
        'language_field',
        'sort'
    ];
}
