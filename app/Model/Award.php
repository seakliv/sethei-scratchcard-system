<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Award extends Model
{
    protected $fillable = [
        'is_claimed', 'claimed_by', 'game_code', 'batch_no', 'book_no', 'ticket_no', 
        'award_verification_code','box_barcode','logistic_code', 'security_code', 'award_level', 'award_amount',
        'a1','a2','a3','b1','b2','b3','b4','b5','b6','m1','m2','m3','m4','m5','m6','z1','imported_by','scanned_at'
    ];

    public function user(){
        return $this->belongsTo(\App\User::class,'claimed_by','id');
    }
}
