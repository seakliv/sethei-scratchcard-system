<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $connection = 'mysql_5d';
    protected $fillable = [
        'id',
        'code',
        'name'
    ];

    public function districts(){
        return $this->hasMany(District::class);
    }

    public function customers(){
        return $this->hasMany(Customer::class);
    }
}
