<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SystemAccountCategory extends Model
{
    protected $fillable = [
        'id',
        'name',
        'sort',
        'type_id',
];

public function type(){
    return $this->belongsTo(SystemAccountType::class,'type_id','id');
}
}
