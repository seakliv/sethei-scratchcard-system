<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;
use App\Constants\UserAccountConstant;
class UserAccount extends Model
{
    protected $connection = 'mysql_5d';
    protected $table = 'd_user_accounts';
    
    protected $fillable = [
            'id',
            'user_id',
            'type_id',
            'category_id',
            'title',
            'number',
            'name',
            'balance',
            'frozen',
            'state',
            'sort',
            'created_at',
            'updated_at'
        ];

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public static function createCashAccount($userId = null){
        $userId = $userId ?? auth()->id();
        return UserAccount::create([
            'title' => 'Default Account',
            'number' => 'Default Account',
            'name' => 'Default Account',
            'balance' => 0,
            'frozen' => 0,
            'state' => 1,
            'user_id' => $userId,
            'type_id' => UserAccountConstant::CASH_ACCOUNT
        ]);
    }
    public static function createBonusAccount($userId = null){
        $userId = $userId ?? auth()->id();
        return UserAccount::create([
            'title' => 'Bonus Account',
            'number' => 'Bonus Account',
            'name' => 'Bonus Account',
            'balance' => 0,
            'frozen' => 0,
            'state' => 1,
            'user_id' => $userId,
            'type_id' => UserAccountConstant::BONUS_ACCOUNT
        ]);
    }
    public static function createProfitAccount($userId = null){
        $userId = $userId ?? auth()->id();
        return UserAccount::create([
            'title' => 'Profit Account',
            'number' => 'Profit Account',
            'name' => 'Profit Account',
            'balance' => 0,
            'frozen' => 0,
            'state' => 1,
            'user_id' => $userId,
            'type_id' => UserAccountConstant::PROFIT_ACCOUNT
        ]);
    }

    public static function createCardBonusAccount($userId = null){
        $userId = $userId ?? auth()->id();
        return UserAccount::create([
            'title' => 'Card Bonus Account',
            'number' => 'Card Bonus Account',
            'name' => 'Card Bonus Account',
            'balance' => 0,
            'frozen' => 0,
            'state' => 1,
            'user_id' => $userId,
            'type_id' => UserAccountConstant::CARD_BONUS_ACCOUNT
        ]);
    }
}
