<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class ProductItem extends Model
{
    protected $fillable = [
        'code', 'game_code', 'batch_no', 'box_code', 'book_no', 'ticket_price', 'ticket_qty', 'is_sold', 'is_activated', 'activated_by', 'customer_id'
    ];

    public function product(){
        return $this->belongsTo(Product::class,'game_code','game_code');
    }

    public function customer(){
        return $this->belongsTo(User::class);
    }
}
