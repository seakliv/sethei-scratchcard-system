<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'id',
        'username',
        'code',
        'phone',
        'national_id_no',
        'address',
        'province_id',
        'district_id',
        'state',
        'created_by',
        'updated_by'
    ];

    public function province(){
        return $this->belongsTo(Province::class);        
    }

    public function district(){
        return $this->belongsTo(District::class, 'district_id');        
    }

    public function sales(){
        return $this->hasMany(Sale::class);
    }
}
