<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $connection = 'mysql_5d';
    protected $fillable = [
        'id',
        'name',
        'code',
        'province_id'
    ];

    public function province(){
        return $this->belongsTo(Province::class);
    }
}
