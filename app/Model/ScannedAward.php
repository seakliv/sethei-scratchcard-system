<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
class ScannedAward extends Model
{
    protected $fillable = [
        'award_level',
        'award_amount',
        'security_code',
        'user_id',
        'award_id',
        'abstract'
    ];

    public function customer(){
        return $this->belongsTo(User::class, 'user_id');         
    }
}
