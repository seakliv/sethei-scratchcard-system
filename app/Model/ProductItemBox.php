<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductItemBox extends Model
{
    protected $fillable = [ 'code',
        'game_code', 'batch_no', 'box_code', 'start_book_no', 'end_book_no', 
        'book_qty', 'created_by', 'updated_by'
    ];
}
