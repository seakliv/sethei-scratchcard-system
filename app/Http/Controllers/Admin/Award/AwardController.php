<?php

namespace App\Http\Controllers\Admin\Award;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Excel;
use App\Imports\AwardsImport;
use App\Model\Award;
use App\Constants\ScratchCard;

class AwardController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-award-data')->only(['index']);
        $this->middleware('permission:import-award-data')->only(['store']);
    }

    public function index()
    {
        $awards = Award::with('user')->orderBy('game_code','ASC')

                        ->when(request()->game_code, function($q){
                            $q->where('game_code',request()->game_code);
                        })
                        ->when(request()->batch_no, function($q){
                            $q->where('batch_no',request()->batch_no);
                        })
                        ->when(request()->book_no, function($q){
                            $q->where('book_no',request()->book_no);
                        })
                        ->when(request()->ticket_no, function($q){
                            $q->where('ticket_no',request()->ticket_no);
                        })
                        ->when(request()->is_claimed != '' &&  request()->is_claimed != 'any', function($q){
                            $q->where('is_claimed',request()->is_claimed);
                        })
                        ->when(request()->award_level != '' , function($q){
                            $q->where('award_level',request()->award_level);
                        })
                        ->when(request()->logistic_code != '' , function($q){
                            $q->where('logistic_code',request()->logistic_code);
                        })
                        ->when(request()->security_code != '' , function($q){
                            $q->where('security_code',ScratchCard::encryptSecurityCode(request()->security_code));
                        })

                        ->orderBy('batch_no','ASC')
                        ->orderBy('book_no','ASC')
                        ->orderBy('ticket_no','ASC')
                        ->paginate(100);

        return view('award.index',compact('awards'));
    }

    public function store(Request $request)
    {
        ini_set('memory_limit',-1);
        ini_set('max_execution_time',0);

        $file = $request->file;

        Excel::import(new AwardsImport, $file);

        return response()->json([
            'success' => true,
            'message' => 'Completed!!!',
        ]);

    }

}
