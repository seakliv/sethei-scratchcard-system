<?php

namespace App\Http\Controllers\Admin\Purchase;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Purchase;
use App\User;

use App\Exports\ScanSummaryExport;
use App\Exports\PurchaseExport;

use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class PurchaseController extends Controller
{
    public function index(){
        
        $purchases = Purchase::with('user')
                    ->when(request()->game_code, function($q){
                        $q->where('game_code', request()->game_code);
                    })
                    ->when(request()->batch_no, function($q){
                        $q->where('batch_no', request()->batch_no);
                    })
                    ->when(request()->book_no, function($q){
                        $q->where('book_no', request()->book_no);
                    })
                    ->when(request()->serial_no, function($q){
                        $q->where('serial_no', request()->serial_no);
                    })
                    ->when(request()->user_id, function($q){    
                        $q->where('user_id', request()->user_id);
                    })
                    ->paginate('20');

            $users = User::whereState(1)->whereUserType(2)->whereComDirect(0)
                    ->where('is_freelancer', 1)
                    ->where('l1_id',4548)
                    ->when(auth()->id() == 44, function($q){
                        $q->orWhere('id','985');
                    })
                    // ->when(auth()->id() == 44, function($q){ //use this for user test app1
                    //     $q->orWhere('id','3894');
                    // })
                    ->pluck('username', 'id');   

        return View('purchase.scan-purchase', \compact('purchases', 'users'));
    }

    public function downloadScanPurchase(){
        return Excel::download(new PurchaseExport, 'purchase.xlsx');
    }

}
