<?php

namespace App\Http\Controllers\Admin\Stock;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Constants\ScratchCard;

use App\Model\Manager;
use App\Model\Product;
use App\Model\ProductItemBox;
use App\Model\ProductItem;
use App\Model\StockLog;
use App\Model\StockLogDetail;
use App\Exports\StockExport;
use PDF;

use DB;
use Validator;
use Excel;


class StockController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-stock')->only(['index']);
        $this->middleware('permission:stock-modification')->only(['update','delete']);
        $this->middleware('permission:add-new-stock')->only(['create','store']);
        $this->middleware('permission:export-stock')->only(['generatePdf','generateExcel']);
    }

    public function index()
    {

        $stockLogs = StockLog::with('user','stockLogDetails')
                            ->when(request()->reference_no, function($q){
                                $q->where('reference_no',request()->reference_no);
                            })
                            ->when(request()->started_date && request()->ended_date, function($q){
                                $q->whereBetween('stock_in_date',[request()->started_date,request()->ended_date]);
                            })
                            ->when(request()->created_by && request()->created_by != 'any', function($q){
                                $q->where('created_by',request()->created_by);
                            })
                            ->paginate(30);

        $managers = Manager::whereIn('id',StockLog::distinct('created_by')->pluck('created_by'))->get()->keyBy('id');
        return view('stock.index',compact('stockLogs','managers'));
    }

    public function create()
    {
        $refNo = $this->getGeneratedStockReferenceNo();
        return view('stock.add-update',compact('refNo'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'boxBarcodes' => 'required'
        ]);

        if($validator->fails()){
            return back()->withError('No content added!!!');
        }
        if(!$this->saveDB($request->all())){
            return back()->withError('Something went wrong! Please try again.');
        };

        return redirect()->route('stocks.index')->withSuccess('You have just added a Stocks successfully!');
    }

    public function edit($refNo)
    {

        $stockLogs = StockLog::with('stockLogDetails')->where('reference_no',$refNo)->first();

        if($stockLogs->count() <= 0){
            return back()->withError('Something went wrong! Please try again.');
        }

        $stock = [
            'reference_no' => $refNo,
            'stock_in_date' => $stockLogs->stock_in_date
        ];

        foreach($stockLogs->stockLogDetails as $log){
            $stock['data'][] = $log;
        }

        return view('stock.add-update',compact('stock','refNo'));
    }

    public function update(Request $request, $refNo)
    {
        $validator = Validator::make($request->all(), [
            'boxBarcodes' => 'required',
            'removeBoxBarcodes' => 'required'
        ]);

        if($validator->fails()){
            if($validator->errors()->has('boxBarcodes') && $validator->errors()->has('removeBoxBarcodes') ){
                return back()->withError('No changes!!!');
            }
        }

        if(!$this->saveDB($request->all(),$refNo)){
            return back()->withError('Something went wrong! Please try again.');
        };
        return redirect()->route('stocks.index')->withSuccess('You have just added a Stocks successfully!');
    }


    public function destroy($refNo)
    {

        DB::beginTransaction();
        try{

            $stockLog = StockLog::where('reference_no',$refNo)->first();
            $stockLogDetails = StockLogDetail::where('reference_no',$refNo)->get();

            if($stockLogDetails && $stockLog){

                foreach($stockLogDetails as $detail){
                    $productItemBox = ProductItemBox::where('game_code',$detail->game_code)
                                                    ->where('batch_no',$detail->batch_no)
                                                    ->where('box_code',$detail->box_code)
                                                    ->first();

                    $productItems = ProductItem::where('game_code',$detail->game_code)
                                                ->where('batch_no',$detail->batch_no)
                                                ->where('box_code',$detail->box_code)
                                                ->get();

                    if($productItemBox && $productItems){

                        $productItems->each->delete();
                        $productItemBox->delete();
                    }
                }

                $stockLogDetails->each->delete();
                $stockLog->delete();

                $result = TRUE;
                DB::commit();
            }

        }catch(Exception $ex){
            $result = FALSE;
            DB::rollback();
        }
        return response()->json(['success' => $result]);
    }

    public function saveDB($data,$refNo = null){

        DB::beginTransaction();
        try{

            $refNo = $refNo ?: $this->getGeneratedStockReferenceNo();
            $stockInDate = request()->action == 'update' ? request()->stock_in_date : date('Y-m-d H:i:s');

            // === Add item to stock ===
            if(isset($data['boxBarcodes'])){
                sort($data['boxBarcodes']);

                foreach($data['boxBarcodes'] as $barcode){

                    $boxBarCodeDetail = $this->getBoxBarCodeDetail($barcode);

                    if($boxBarCodeDetail && !ProductItemBox::whereCode($boxBarCodeDetail['code'])->first()){

                        $this->saveProductItem($boxBarCodeDetail);
                        $this->saveProductItemBox($boxBarCodeDetail);
                        $this->saveStockLogDetail($boxBarCodeDetail,$refNo);


                    }else{

                        return false;
                    }
                }
                $this->saveStockLog($refNo,$stockInDate);
            }

            // === Delete item from stock ===
            if(isset($data['removeBoxBarcodes'])){
                foreach(json_decode($data['removeBoxBarcodes']) as $item){
                    $this->removeProductItemStock($item);
                }
            }

            // === Update or add log for stock ===
            $this->saveStockLog($refNo,$stockInDate);



            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
        }

        return true;
    }

    public function getBookList(){

        $data = ProductItem::with('product')->whereGameCode(request()->game_code)
                            ->whereBatchNo(request()->batch_no)
                            ->whereBoxCode(request()->box_code)
                            ->get();

        return response()->json(['success' => true, 'data' => $data,'message' => 'Success']);
    }

    public function getBoxBarCode(Request $request){

        $validator = Validator::make($request->all(), [
            'barcode' => 'required|min:29|max:29'
        ]);

        if($validator->fails()) {
            return response()->json(['success' => false, 'message' => 'Please provide a valid box barcode!!!']);
        }

        if($this->checkIfProductBoxBarcodeExists($request->barcode)){
            return response()->json(['success' => false, 'message' => 'This product code was already added! Please try a new one.']);
        }

        // P0001190010003000014510001500
        // P0001190010003100015010001550
        $data = $this->getBoxBarCodeDetail($request->barcode);
        if(!$data){
            $boxBarcode = ScratchCard::getDetailBoxBarCode($request->barcode);
            return response()->json(['success' => false, 'message' => 'There is no product match with this product code '.$boxBarcode['game_code'].'.']);
        }

        if(!$data){
            $boxBarcode = ScratchCard::getDetailBoxBarCode($request->barcode);
            return response()->json(['success' => false, 'message' => 'There is no product match with this product code '.$boxBarcode['game_code'].'.']);
        }

        return response()->json(['success' => true, 'data' => $data,'message' => 'Success']);
    }

    public function saveProductItemBox($boxBarCodeDetail){

        $productItemBox = new ProductItemBox();

        $boxBarCodeDetail['created_by'] = auth()->user()->id;
        $boxBarCodeDetail['updated_by'] = auth()->user()->id;

        $productItemBox->fill($boxBarCodeDetail);
        $productItemBox->save();

    }

    public function saveProductItem($boxBarCodeDetail){

        for($i = 0 ; $i < $boxBarCodeDetail['book_qty'] ; $i++){

            $productItem = new ProductItem();

            $boxBarCodeDetail['book_no'] = bookNumberFormat($boxBarCodeDetail['start_book_no'] + $i);
            $boxBarCodeDetail['code'] =  $boxBarCodeDetail['game_code'].$boxBarCodeDetail['batch_no'].$boxBarCodeDetail['book_no'].'001';

            $productItem->fill($boxBarCodeDetail);
            $productItem->save();
        }
    }

    public function saveStockLog($refNo,$stockInDate){

            $stockLog = StockLog::whereReferenceNo($refNo)->first() ?: new StockLog();;
            $stockLogDetail = StockLogDetail::where('reference_no',$refNo)->get();

            if($stockLogDetail->count() > 0){

                $product_qty = $stockLogDetail->groupBy('game_code')->count();
                $book_qty = $stockLogDetail->sum('book_qty');

                $stockLog->reference_no = $refNo;
                $stockLog->stock_in_date = $stockInDate;
                $stockLog->total_product_qty = $product_qty;
                $stockLog->total_book_qty = $book_qty;
                $stockLog->created_by = request()->action == 'update' ? $stockLog->created_by : auth()->user()->id;
                $stockLog->updated_by = auth()->user()->id;
                $stockLog->save();
            }else{
               $stockLog->delete();
            }


    }

    public function saveStockLogDetail($boxBarCodeDetail,$refNo){

        $stockLogDetail = new StockLogDetail();

        $product = Product::where('game_code',$boxBarCodeDetail['game_code'])->first();

        $boxBarCodeDetail['reference_no'] = $refNo;
        $boxBarCodeDetail['unit_id'] = isset($product) ? $product->unit_id : '';
        $boxBarCodeDetail['created_by'] = auth()->user()->id;
        $boxBarCodeDetail['updated_by'] = auth()->user()->id;

        $stockLogDetail->fill($boxBarCodeDetail);
        $stockLogDetail->save();

    }

    public function removeProductItemStock($item){

        $baseCondition = 'game_code = "' . $item->game_code . '" ' .
                     'and batch_no = "' . $item->batch_no . '" ' .
                     'and box_code = "' . $item->box_code. '"';
        $bookCondition = 'start_book_no = "' . $item->start_book_no . '" ' .
                        'and end_book_no = "' . $item->end_book_no . '"';

        $productItems = ProductItem::whereRaw($baseCondition)->whereBetween('book_no',[$item->start_book_no, $item->end_book_no])->get();
        $productItemBoxs = ProductItemBox::whereRaw($baseCondition)->whereRaw($bookCondition)->get();
        $stockLogDetails = StockLogDetail::whereRaw($baseCondition)->whereRaw($bookCondition)->get();

        if($productItems && $productItemBoxs && $stockLogDetails){
            $productItems->each->delete();
            $productItemBoxs->each->delete();
            $stockLogDetails->each->delete();
        }
    }

    public function getBoxBarCodeDetail($barCode){

        $data = null;
        $boxBarcode = ScratchCard::getDetailBoxBarCode($barCode);

        if(count($boxBarcode) > 0){

            $product = Product::with('product_unit')->where('game_code',$boxBarcode['game_code'])->first();

            if($product){
                $qty = $boxBarcode['end_book_no'] - $boxBarcode['start_book_no'] + 1;

                $data = [
                    'code' => $barCode,
                    'game_code' => $boxBarcode['game_code'],
                    'game_name' => isset($product) ? $product->name : '',
                    'batch_no' => $boxBarcode['batch_no'],
                    'box_code' => $boxBarcode['box_code'],
                    'start_book_no' =>  $boxBarcode['start_book_no'],
                    'end_book_no' =>  $boxBarcode['end_book_no'],
                    'ticket_price' => isset($product) ? $product->ticket_price : 0,
                    'ticket_qty' => isset($product) ? $product->ticket_qty : 0,
                    'book_qty' => $qty,
                    'unit' => isset($product->product_unit) ? $product->product_unit->name : ''
                ];
            }
        }

        return $data;
    }

    public function checkIfProductBoxBarcodeExists($boxBarcode){

        $boxBarcode = $this->getBoxBarCodeDetail($boxBarcode);
        $box = ProductItemBox::whereGameCode($boxBarcode['game_code'])
                            ->whereBatchNo($boxBarcode['batch_no'])
                            ->whereBoxCode($boxBarcode['box_code'])
                            ->first();

        return $box ? TRUE : FALSE;
    }

    public function getGeneratedStockReferenceNo(){

        $latestLog = StockLog::orderBy('id','DESC')->first();

        $refNo = $latestLog
                    ? 'STOCK'.date("Ymd").addPrefixStringPad(substr($latestLog->reference_no, -5)+1,5,'0',STR_PAD_LEFT)
                    : 'STOCK'.date("Ymd").addPrefixStringPad(1,5,'0',STR_PAD_LEFT);
	 return $refNo;
    }
    public function getStockLogDetailByRefNo($refNo){

        $data = [];

        $stockLog = StockLog::with('stockLogDetails','product')->where('reference_no',$refNo)->first();

        foreach($stockLog->stockLogDetails as $detail){

            $data [] = [
                'reference_no' => $refNo,
                'code' => $detail->code,
                'game_code' => $detail->game_code,
                'game_name' => $detail->product->name,
                'batch_no' => $detail->batch_no,
                'box_code' => $detail->box_code,
                'start_book_no' => $detail->start_book_no,
                'end_book_no' => $detail->end_book_no,
                'qty' => $detail->book_qty,
                'unit' => $detail->product->product_unit->name,
                'total_book_qty' => $stockLog->stockLogDetails->sum('book_qty'),
                'total_ticket_qty' => $stockLog->stockLogDetails->sum('book_qty') * $detail->product->ticket_qty,
                'stock_in_date' => $stockLog->stock_in_date,
            ];

        }

        return response()->json(['success' => true, 'data' => $data,'message' => 'Success']);
    }

    public function generatePdf(){

        $stockLog =  StockLog::with('stockLogDetails')->where('reference_no',request()->refNo)->first();
        // return view('Pdf.stock.index',compact('stockLog'));
        $pdf = PDF::loadView('Pdf.stock.index', ['stockLog' => $stockLog]);
        $pdf->getDomPDF()->set_option("enable_php", true);

        return $pdf->download(request()->refNo.'.pdf');
    }

    public function generateExcel(){

        $fileName = '';


        if(request()->reference_no){
            $fileName = 'Stock-Report ('.request()->reference_no.').xlsx';
        }
        elseif(request()->started_date && request()->ended_date){
            $fileName = 'Stock-Report ('.request()->started_date.' – '.request()->ended_date.').xlsx';
        }else{
            $fileName = 'Stock-Report.xlsx';
        }

        return Excel::download(new StockExport, $fileName);
    }

}
