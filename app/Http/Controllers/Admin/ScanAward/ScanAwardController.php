<?php

namespace App\Http\Controllers\Admin\ScanAward;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ScannedAward;
use App\User;
use App\Exports\ScanAwardExport;
use App\Exports\ScanSummaryExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ScanAwardController extends Controller
{
    public function index(){
       $awardScans = ScannedAward::orderBy('created_at', 'DESC')
                                ->when(request()->award_level, function($q){
                                        $q->where('award_level', request()->award_level);
                                    })
                                ->when(request()->customer_id, function($q){
                                        $q->where('user_id', request()->customer_id);
                                    })
                                ->when(request()->award_label, function($q){
                                        $q->where('abstract', request()->award_label);
                                    })
                                ->when(request()->ticket_number, function($q){
                                        $q->where('security_code', 'like', request()->ticket_number.'%');
                                    })
                                ->when(request()->started_date && request()->ended_date, function($q){
                                        $q->whereBetween('created_at',[request()->started_date,request()->ended_date]);
                                })->paginate(25);

        $customers = getCustomers();

       return view('scan-award.index', compact('awardScans', 'customers'));
    }

    public function exportExcel(){

        $fileName = '';

        if(request()->award_level){
            $fileName = 'ScanLog-Report ('.request()->award_level.').xlsx';
        }elseif(request()->award_label){
            $fileName = 'ScanLog-Report ('.request()->award_label.').xlsx';
        }elseif(request()->ticket_number){
            $fileName = 'ScanLog-Report ('.request()->ticket_number.').xlsx';
        }
        elseif(request()->started_date && request()->ended_date){
            $fileName = 'ScanLog-Report ('.request()->started_date.' – '.request()->ended_date.').xlsx';
        }else{
            $fileName = 'ScanLog-Report.xlsx';
        }

        return Excel::download(new ScanAwardExport, $fileName);
    }

    public function getScanSummary(){

        $awardScans = ScannedAward::with('customer')

                                ->when(request()->award_level, function($q){
                                        $q->where('award_level', request()->award_level);
                                    })
                                ->when(request()->customer_id, function($q){
                                        $q->where('user_id', request()->customer_id);
                                    })
                                ->when(request()->award_label, function($q){
                                        $q->where('abstract', request()->award_label);
                                    })
                                ->when(request()->ticket_number, function($q){
                                        $q->where('security_code', 'like', request()->ticket_number.'%');
                                    })
                                ->when(request()->started_date && request()->ended_date, function($q){
                                        $q->whereBetween('created_at',[request()->started_date,request()->ended_date]);
                                })
                                ->select(
                                    'user_id',
                                    DB::raw('count(id) as total_ticket'),
                                    DB::raw('sum(award_amount) as total_amount')
                                )
                                ->groupBy('user_id')
                                ->paginate(25);

        $customers = getCustomers();

        return view('scan-award.scan-summary', compact('awardScans', 'customers'));
    }

    public function downloadScanSummary(){

     return Excel::download(new ScanSummaryExport, 'summary.xlsx');

    }
}
