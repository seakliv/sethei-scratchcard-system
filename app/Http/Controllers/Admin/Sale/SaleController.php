<?php

namespace App\Http\Controllers\Admin\Sale;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Customer;
use App\Model\Product;
use App\Model\ProductItem;
use App\Model\Sale;
use App\Model\SaleDetail;
use App\Exports\SaleExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use PDF;
use Validator;
use App\Constants\ScratchCard;
use App\User;
class SaleController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-sale')->only(['index']);
        $this->middleware('permission:sale-modification')->only(['update','delete']);
        $this->middleware('permission:add-new-sale')->only(['create','store']);
        $this->middleware('permission:export-sale')->only(['generatePdf','generateExcel']);
    }

    public function index()
    {
        $sales = Sale::orderBy('reference_no', 'DESC')
                    ->when(request()->reference_no, function($q){
                        $q->where('reference_no', request()->reference_no);
                    })
                    ->when(request()->customer_id, function($q){
                        $q->where('customer_id', request()->customer_id);
                    })
                    ->when(request()->started_date && request()->ended_date, function($q){
                        $q->whereBetween('sale_date',[request()->started_date,request()->ended_date]);
                    })
                    ->paginate('10');

        $customers = getCustomers();

        return view('sale.index', compact('sales', 'customers'));
    }

    public function create()
    {
        $referenceNumber = $this->getGenerateReferenceNumber();
        $customers = getCustomers();

        $products = Product::with(['product_unit'])
                            ->whereHas('product_items', function($q)
                            {
                                $q->where('is_sold', 0);
                            })
                            ->get();

        foreach($products as $key => $item){
            $products[$key]['unit'] = $item['product_unit'] ? $item['product_unit']['name'] : '';
            unset($products[$key]['product_unit']);
        }

        return view('sale.add-update', \compact('customers', 'products', 'referenceNumber'));
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'book_code' => 'required'
        ]);

        if($validator->fails()){
            return back()->withError('No Customer Select or No content added!!!');
        }


        $this->saveDB($request->all());

        return \redirect()->route('sales.index')->withSuccess("You have added 1 sale record sucessfully");
    }

    public function show($refNo)
    {
        $sale = Sale::with('customer')->where('reference_no',$refNo)->first();
        $saleItems = SaleDetail::with('product')->where('reference_no', $refNo)
                                ->select('game_code', 'price', DB::raw('count(book_no) as book_qty'), DB::raw('sum(price) as total_price'))
                                ->groupBy('game_code')
                                ->groupBy('price')
                                ->get();



        $sale = [
            'reference_no' => $refNo,
            'sale_date' => $sale->sale_date,
            'total' => $sale->total_price,
            'customer' =>  isset($sale->customer) ? $sale->customer->username : '',
            'phone' => isset($sale->customer) ? $sale->customer->phone : '',
            'address' => isset($sale->customer->province) ? $sale->customer->province->name : ''
        ];

        foreach($saleItems as $item){
            $sale['item'][] = $item;
        }

        return response()->json(['success' => 200, 'message' => 'success', 'data' => $sale]);

    }

    public function edit($refNo)
    {
        $customers = getCustomers();
        $products = Product::with(['product_unit'])
                            ->whereHas('product_items', function($q)
                            {
                                $q->where('is_sold', 0);
                            })
                            ->get();

        foreach($products as $key => $item){
            $products[$key]['unit'] = $item['product_unit'] ? $item['product_unit']['name'] : '';
            unset($products[$key]['product_unit']);
        }

        $sale = Sale::where('reference_no',$refNo)->first();
        $saleItems = SaleDetail::where('reference_no', $refNo)
                                ->select('code', 'game_code','batch_no', 'box_code', 'ticket_price', 'ticket_qty', 'price', 'book_no')
                                ->orderBy('book_no','ASC')
                                ->get();

        $sale = [
            'reference_no' => $refNo,
            'sale_date' => $sale->sale_date,
            'customer_id' => $sale->customer_id,
            'total_price' => $sale->total_price,
            'discount' => $sale->discount,
            'grand_price' => $sale->grand_price
        ];

        foreach($saleItems as $item){
            $sale['product'][] = $item;
            $sale['book_code'][] = $item['game_code'].$item['batch_no'].$item['book_no'];
        }

        return view('sale.add-update', \compact('sale', 'products', 'customers'));
    }

    public function update(Request $request, $refNo)
    {

        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'book_code' => 'required'
        ]);

        if($validator->fails()){
            return back()->withError("Make sure you have add content !!!");
        }

        $data = $request->all();

        $this->updateDB($request->all(), $refNo);
        return \redirect()->route('sales.index')->withSuccess("You have updated 1 sale recode sucessfully");
    }

    public function destroy($refNo)
    {
        DB::beginTransaction();
        try {
            $saleProduct = Sale::where('reference_no', $refNo)->get();
            $saleProductDetails = SaleDetail::where('reference_no', $refNo)->get();

            foreach($saleProductDetails as $item){
                $productItem = ProductItem::where('game_code', $item->game_code)
                                    ->where('batch_no', $item->batch_no)
                                    ->where('box_code', $item->box_code)
                                    ->where('book_no', $item->book_no)
                                    ->first();
		if($productItem)
                $productItem->update([
                    'is_sold' => 0,
                    'is_activated' => 0,
                    'activated_by' => null,
                    'customer_id' => null
                ]);
            }

            if($saleProduct && $saleProductDetails){
                $saleProduct->each->delete();
                $saleProductDetails->each->delete();
            }
            $result = true;
            DB::commit();

        } catch (Exception $ex) {
            $result = false;
            DB::rollback();
        }

        return response()->json(['success' => $result]);
    }

    public function saveDB($data, $refNo = null){

        $sale['reference_no'] = $this->getGenerateReferenceNumber();

        $totalProductQty = count($data['book_code']);

        $bookPrice = 0;


        DB::beginTransaction();
        try {

            foreach($data['book_code'] as $key => $item){
                $value = ScratchCard::getDetailBookCode($item);

                $product = Product::select('ticket_price','ticket_qty')->where('game_code', $value['game_code'])->first();

                $productItems = ProductItem::where('game_code', $value['game_code'])
                                            ->where('book_no', $value['book_no'])
                                            ->where('is_sold', 0)
                                            ->where('is_activated', 0)
                                            ->get();

                $saleDetail['reference_no'] = $sale['reference_no'];
                $saleDetail['code'] = $item;
                $saleDetail['game_code'] = $value['game_code'];
                $saleDetail['batch_no'] = '';
                $saleDetail['box_code'] = '';
                $saleDetail['book_no'] = $value['book_no'];
                $saleDetail['ticket_price'] = $product->ticket_price;
                $saleDetail['ticket_qty'] = $product->ticket_qty;
                $saleDetail['price'] = $product->ticket_price * $product->ticket_qty;
                $saleDetail['created_by'] = auth()->id();

                $saleProductDetail = new SaleDetail();
                $saleProductDetail->fill($saleDetail);
                $saleProductDetail->save();

                foreach($productItems as $proItem){
                $proItem->update([
                    'is_sold' => 1,
                    'is_activated' => 1,
                    'customer_id' => $data['customer_id'],
                    'activated_by' => auth()->id(),
                ]);
                }
                          

            }

            $saleDetails = SaleDetail::where('reference_no', $sale['reference_no'])->get();
            $totalPrice = $saleDetails->sum('price');

            $sale['total_product_qty'] = $totalProductQty;
            $sale['total_price'] = $totalPrice;
            $sale['customer_id'] = $data['customer_id'];
            $sale['sale_date'] = $data['sale_date'];
            $sale['created_by'] = auth()->id();
            $sale['discount'] = $data['discount'];
            $sale['grand_price'] = $totalPrice - ($totalPrice * $data['discount'] / 100);

            $saleProduct = new Sale();
            $saleProduct->fill($sale);
            $saleProduct->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
        }

    }


    public function updateDB($data, $refNo = null){

        DB::beginTransaction();
            try {
                if(isset($data['removeBookCode'])){
                    foreach(json_decode($data['removeBookCode']) as $items){
                        $this->deleteSaleItem($items, $refNo);
                    }
                }

                if(isset($data['book_code'])){
                    $bookPrice = 0;
                    foreach($data['book_code'] as $key => $item){

                        $value = ScratchCard::getDetailBookCode($item);

                        $product = Product::select('ticket_price','ticket_qty')->where('game_code', $value['game_code'])->first();

                        $productItems = ProductItem::where('game_code', $value['game_code'])
                                                    ->where('batch_no', $value['batch_no'])
                                                    ->where('book_no', $value['book_no'])
                                                    ->where('is_sold', 0)
                                                    ->where('is_activated', 0)
                                                    ->get();

                            foreach($productItems as $proItem){
                                $proItem->update([
                                    'is_sold' => 1,
                                    'is_activated' => 1,
                                    'customer_id' => $data['customer_id'],
                                    'activated_by' => auth()->id(),
                                ]);
                                $saleDetail['reference_no'] = $data['reference_no'];
                                $saleDetail['code'] = $item;
                                $saleDetail['game_code'] = $proItem->game_code;
                                $saleDetail['batch_no'] = $proItem->batch_no;
                                $saleDetail['box_code'] = $proItem->box_code;
                                $saleDetail['book_no'] = $proItem->book_no;
                                $saleDetail['ticket_price'] = $product->ticket_price;
                                $saleDetail['ticket_qty'] = $product->ticket_qty;
                                $saleDetail['price'] = $product->ticket_price * $product->ticket_qty;
                                $saleDetail['created_by'] = auth()->id();

                                $saleProductDetail = isset($id) ? SaleDetail::findOrFail($id) : new SaleDetail();
                                $saleProductDetail->fill($saleDetail);
                                $saleProductDetail->save();
                            }
                    }
                }

                $saleDetail = SaleDetail::where('reference_no', $refNo)->get();
                $totalPrice = $saleDetail->sum('price');
                $totalProductQty = $saleDetail->count();

                $sale['total_product_qty'] = $totalProductQty;
                $sale['total_price'] = $totalPrice;
                $sale['customer_id'] = $data['customer_id'];
                $sale['sale_date'] = $data['sale_date'];
                $sale['discount'] = $data['discount'];
                $sale['grand_price'] = $totalPrice - ($totalPrice * $data['discount'] / 100);

                $saleProduct = Sale::where('reference_no', $refNo)->first();;
                $saleProduct->fill($sale);
                $saleProduct->save();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
        }
    }

    public function deleteSaleItem($items, $refNo){

        $item = $items;
        $saleProductDetails = SaleDetail::where('reference_no', $refNo)
                                        ->where('game_code', $item->game_code)
                                        ->where('batch_no', $item->batch_no)
                                        ->where('box_code', $item->box_code)
                                        ->where('book_no', $item->book_no)
                                        ->get();

        if($saleProductDetails && $saleProductDetails->count() > 0){
            foreach($saleProductDetails as $saleItem){
                $productItem = ProductItem::where('game_code', $saleItem->game_code)
                                    ->where('batch_no', $saleItem->batch_no)
                                    ->where('box_code', $saleItem->box_code)
                                    ->where('book_no', $saleItem->book_no)
                                    ->first();
                $productItem->update([
                    'is_sold' => 0,
                    'is_activated' => 0,
                    'activated_by' => null,
                    'customer_id' => null
                ]);
            }

            $saleProductDetails->each->delete();
        }
    }

    public function getGenerateReferenceNumber(){

        $latestSale = Sale::orderBy('id','DESC')->first();

        if($latestSale){
            $number = substr($latestSale->reference_no, -5);
            $lognumber = 'SALE'.date("Ymd").addPrefixStringPad($number+1,5,'0',STR_PAD_LEFT);
        }else{
            $lognumber = 'SALE'.date("Ymd").addPrefixStringPad(1,5,'0',STR_PAD_LEFT);
        }

        return $lognumber;
    }

    public function getSaleDetailByRefNo($refNo){
        $data = [];

        $sale = Sale::with('saleDetails','customer')->where('reference_no',$refNo)->first();

        $data['sale'] = $sale->toArray();

        foreach($sale->saleDetails as $detail){

            $data['detail'][] = [
                'reference_no' => $refNo,
                'code' => $detail->code,
                'game_code' => $detail->game_code,
                'game_name' => $detail->product->name,
                'batch_no' => $detail->batch_no,
                'box_code' => $detail->box_code,
                'book_no' => $detail->book_no,
                'ticket_qty' => $detail->ticket_qty,
                'ticket_price' => $detail->ticket_price,
                'price' => $detail->price,
                'unit' => $detail->product->product_unit->name,
            ];

        }

        return response()->json(['success' => true, 'data' => $data,'message' => 'Success']);
    }

    public function generatePdf(){

        $sale =  Sale::with('saleDetails','customer')->where('reference_no',request()->refNo)->first();

        $pdf = PDF::loadView('Pdf.sale.index', ['sale' => $sale]);

        return $pdf->download(request()->refNo.'.pdf');
    }

    public function generateExcel(){

        $fileName = '';


        if(request()->reference_no){
            $fileName = 'Sale-Report ('.request()->reference_no.').xlsx';
        }
        elseif(request()->started_date && request()->ended_date){
            $fileName = 'Sale-Report ('.request()->started_date.' – '.request()->ended_date.').xlsx';
        }else{
            $fileName = 'Sale-Report.xlsx';
        }

        return Excel::download(new SaleExport, $fileName);
    }

    public function getProductByBookCode(){

	    $code = request()->bookCode;
	    
        $value = ScratchCard::getDetailBookCode($code);

        if(!$value){
            return response()->json(['success' => false, 'message' => "Please input a valid book serial code."]);
        }
        $product = Product::where('game_code', $value['game_code'])->first();
        
        // $productItem = ProductItem::where('game_code', $value['game_code'])
        //                             ->where('book_no', $value['book_no'])
        //                             ->where('is_activated', 0)
        //                             ->first();

        // if(!$product || !$productItem ){
        //     return response()->json(['success' => false, 'message' => "Your book serial code is already sold out."]);
        // }

        $data = [
            'product_name' => $product->name,
            'game_code' => $product->game_code,
            'batch_no' => '',
            'ticket_price' => $product->ticket_price,
            'ticket_qty' => $product->ticket_qty,
            'book_no' => $value['book_no']
        ];

        return response()->json(['success' => 200, 'data' => $data]);
    }

    public function getProductByBoxCode(){

        $data = [];
        $code = request()->boxCode;
        $bookCodes = request()->get('bookCodes',[]);
        $value = ScratchCard::getDetailBoxBarCode($code);

        if(!$value){
            return response()->json(['success' => false, 'message' => "Please input a valid box serial code."]);
        }

        $product = Product::where('game_code', $value['game_code'])->first();

        $productItems = ProductItem::where('game_code', $value['game_code'])
                                    ->where('batch_no', $value['batch_no'])
                                    ->where('box_code', $value['box_code'])
                                    ->where('is_activated', 0)
                                    ->where('is_sold', 0)
                                    ->whereNotIn('code',$bookCodes)
                                    ->get();

        foreach($productItems as $item){
            $data[] = [
                'product_name' => $product->name,
                'code' => $item->code,
                'game_code' => $product->game_code,
                'batch_no' => $item->batch_no,
                'box_code' => $item->box_code,
                'ticket_price' => $product->ticket_price,
                'ticket_qty' => $product->ticket_qty,
                'book_no' => $item->book_no
            ];
        }

        return response()->json(['success' => 200, 'data' => $data]);
    }
}
