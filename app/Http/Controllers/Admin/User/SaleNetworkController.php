<?php

namespace App\Http\Controllers\Admin\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserAccount;
use App\Constants\Account;
use App\User;
use DB;
use Carbon\Carbon;
class SaleNetworkController extends Controller
{
   
    public function index()
    {
        $level = request()->get('level') ? request()->get('level') + 1 : 1;
        $level = $level > 8 ? 8 : $level;
        $parent_id = request()->get('parent_id') ?? 0;
        $state = request()->get('state') ?? 'all';
        $keyword = request()->get('keyword') ?? '';
        
        $users = User::select('id','username','account_number','parent_id','level','com_direct','user_type','state','win_money_5d','commission_5d','current_commission_5d')
                        ->with('cashAccount','children')->where(['com_direct' => 0]);
      
        $users = $users->when($state != 'all',function($q) use ($state){
                        return $q->where(['state' => $state]);
                    })
                    ->when($keyword == '',function($q) use ($level, $parent_id){
                            return $q->where(['user_type' => 2, 'Level' => $level, 'parent_id' => $parent_id]);
                    })->when($keyword != '',function($q) use ($keyword){
                        return $q->where('username','LIKE','%'.$keyword.'%')
                                 ->orWhere('account_number','LIKE','%'.$keyword.'%');
                    })
                    ->when(isset(auth()->user()->roles[0]) && auth()->user()->roles[0]->id != 1, function($q){
                        return $q->where('id','!=',45);
                    })
                    ->orderBy('id','DESC')->paginate(20);
       
        // Breadcrumbs
        $arrayId = [];
        $topLevelUsers = [];
        $thisUser = User::select('id','username','level','l1_id','l2_id','l3_id','l4_id','l5_id','l6_id','l7_id','l8_id')
                        ->where('id',$parent_id)->first(); 
       
        if($thisUser){
            for($i = 1; $i < $thisUser->level; $i++){  
                $arrayId[] = $thisUser->{'l'.$i.'_id'};
            }
            $topLevelUsers = User::select('id','username','level')->whereIn('id', $arrayId)->orderBy('level','ASC')->get();
            
            $topLevelUsers[] = $thisUser;
        }
       
        return view('user.sale-network.index',compact('users','topLevelUsers'));
    }

    
    public function create()
    {
        //
    }

   
    public function store(Request $request)
    {
        //
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $user = User::find($id);
        return view('user.sale-network.add-update',compact('user'));
    }

    
    public function update(Request $request, $id)
    {
        // $request->validate([
        //     'username' => 'required|unique:users,username,'.$id,
        //     'account_number' => 'required|unique:users,account_number,'.$id
        // ]);

        if($request->password){
            $this->validate($request,[
                'password' => 'required|confirmed|min:6',
                'password_confirmation' => 'required',
            ]);
        }

        if($request->pay_pass){
            $this->validate($request,[
                'pay_pass' => 'required|confirmed|min:6',
                'pay_pass_confirmation' => 'required'
            ]);
        }
        
       $user = $this->saveDB($request->all(), $id);
       
       return redirect()->route('users.sale-network.index', [ 'level'=>$user->level-1, 'parent_id'=>$user->parent_id ]);
    }

  
    public function destroy($id)
    {
        //
    }

    public function saveDB($data , $id = null){
    
            DB::beginTransaction();
            try {
                $user = isset($id) ? User::find($id) : new User;
                if(!$user) return redirect()->back()->withError('There is no record found!');
                $data['pass_salt'] = isset($id) ? $user->pass_salt : rand(10000000,99999999);
                $auth = auth()->user();
    
                if(!isset($id)){  
                    if($data['parent_id']>0){
                        $data['level'] += 1;
                        $parent = User::find($data['parent_id']);
                        
                        for($i=1;$i<$parent->level;$i++){
                            $data['l'.$i.'_id'] = $parent->{'l'.$i.'_id'};
                        }
                        $data['l'.$parent->level.'_id'] = $parent->id;
                        
                    } 
                    if(!$auth->can('add-new-user-account') && $auth->can('request-add-new-user-account')){
                        $data['state'] = 0;
                        $data['is_approved'] = 0;
                    }
                }else{
                    $change[] = 'edit setting';
                    $data['parent_id'] = $user->parent_id;
                    $data['level'] = $user->level;
    
                    if($auth->can('add-new-user-account')){
                        $data['is_approved'] = 1;
                    }
                }
    
                if($data['password'] && ($data['password'] == $data['password_confirmation'])){                
                    $data['password'] = md5($data['password'].$data['pass_salt']);
                    if(isset($id)){
                        $change[] = 'edit passowrd';
                    }
                }else{
                    unset($data['password']);
                }
    
                if($data['pay_pass'] && ($data['pay_pass'] == $data['pay_pass_confirmation'])){
                    $data['pay_pass'] = md5($data['pay_pass'].$data['pass_salt']);
                    if(isset($id)){
                        $change[] = 'edit pay password';
                    }
                }else{
                    unset($data['pay_pass']);
                }
    
                $user->fill($data);
                if($user->save()){
                    if(!$user->cashAccount) 
                        UserAccount::createCashAccount($user->id);
            
                    if(isset($data['id_card_image']) && $data['id_card_image']){
                        $user->clearMediaCollection('id_card');
                        $user->addMediaFromRequest('id_card_image')->toMediaCollection('id_card');
                    }
                    // Create ManagerLog for create new user
                }
                DB::commit();
                return $user;
            }catch (Exception $ex) {
                DB::rollback();
            }
           
    }
}
