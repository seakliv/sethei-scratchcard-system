<?php

namespace App\Http\Controllers\Admin\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProductItem;
use App\Model\Product;

class StockController extends Controller
{
    public function index()
    {
        $productItems = ProductItem::with('product','product.product_unit')
                                    ->orderBy('game_code','ASC')
                                    ->orderBy('batch_no','ASC')
                                    ->orderBy('box_code','ASC')
                                    ->orderBy('book_no','ASC')
                                    ->when(request()->is_sold && request()->is_sold != 'any', function($q){
                                        $q->where('is_sold', request('is_sold'));
                                    })
                                    ->when(request()->is_activated && request()->is_activated != 'any', function($q){
                                        $q->where('is_activated',request()->is_activated);
                                    })
                                    ->when(request()->game_code && request()->game_code != 'any', function($q){
                                        $q->where('game_code',request()->game_code);
                                    })
                                    ->when(request()->batch_no, function($q){
                                        $q->where('batch_no',request()->batch_no);
                                    })
                                    ->when(request()->box_code, function($q){
                                        $q->where('box_code',request()->box_code);
                                    })
                                    ->when(request()->book_no, function($q){
                                        $q->where('book_no',request()->book_no);
                                    })
                                    ->paginate(100);

        $products = Product::all()->keyBy('game_code');

        return view('product.stock.index',compact('productItems','products'));
    }
}
