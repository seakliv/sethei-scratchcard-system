<?php

namespace App\Http\Controllers\Admin\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;
use App\Model\ProductUnit;
use DB;

class ProductController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-product')->only(['index']);
        $this->middleware('permission:product-modification')->only(['update','delete']);
        $this->middleware('permission:add-new-product')->only(['create','store']);
    }

    public function index()
    {
        $products = Product::with('product_unit','available_product_items')
                ->when(request()->game_code, function($q){
                    $q->where('game_code',request()->game_code);
                })
                ->when(request()->name, function($q){
                    $q->where('name',request()->name);
                })
                ->orderBy('game_code','ASC')
                ->paginate(20);

        return view('product.index',compact('products'));
    }

    public function create()
    {
        $units = ProductUnit::orderBy('name','ASC')->pluck('name','id')->toArray();

        return view('product.add-update',compact('units'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'game_code' => 'required|max:5|min:5|unique:products,game_code',
            'name' => 'required|unique:products,name',
            'ticket_price' => 'required|numeric',
            'ticket_qty' => 'required|numeric'
        ]);

        $this->saveDB($request->all());

        return redirect()->route('products.index')->withSuccess('You have just added a Product successfully!');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $units = ProductUnit::orderBy('name','ASC')->pluck('name','id')->toArray();
        $product = Product::find($id);

        return view('product.add-update',compact('product','units'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'game_code' => 'required|max:5|min:5|unique:products,game_code,'.$id,
            'name' => 'required|unique:products,name,'.$id
        ]);

        $this->saveDB($request->all(),$id);

        return redirect()->route('products.index')->withSuccess('You have just update a Product successfully!');
    }


    public function destroy($id)
    {
        $product = Product::find($id);

        DB::beginTransaction();
        if($product){
            try{
                $product->delete();
                $result = "true";
                DB::commit();
            }catch(Exception $ex){
                $result = "false";
                DB::rollback();
            }

        }
        return response()->json(['success' => $result]);
    }

    public function saveDB($data,$id = null){

        DB::beginTransaction();
        try{

            if($id > 0 && $id != null){
                $product = Product::find($id);
                $data['updated_by'] = auth()->user()->id;

            }else{
                $product = new Product();
                $data['created_by'] = auth()->user()->id;
                $data['updated_by'] = auth()->user()->id;
            }

            $product->fill($data);
            $product->save($data);
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
        }
    }
}
