<?php

namespace App\Http\Controllers\API\Purchase;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController;
use App\Constants\ScratchCard;
use App\Model\ProductItem;
use App\Model\Purchase;
use DB;

class PurchaseController extends BaseController
{
    public function cardPurchase(){
        $serialNo = '';
        $depoName = '';
        $barcodeScan = request()->barcode;
        $value = ScratchCard::getDetailBookCode($barcodeScan);
        if(!$value){
            return $this->sendError('Your code is invalid');
        }

        $user = auth()->user();
        if($user->is_freelancer != 1 || $user->can_scan != 1){
            return $this->sendError(api_trans('award.can_scan'));
        }

        $productItem = ProductItem::with('customer')
                                    ->where('game_code', $value['game_code'])
                                    ->where('batch_no', $value['batch_no'])
                                    ->where('book_no', $value['book_no'])
                                    ->where('is_activated', 1)
                                    ->whereNotNull('customer_id')
                                    ->when(auth()->id() != 985, function($q){
                                        $q->where('customer_id','!=',985);
                                    })
                                    ->first();

        if(!$productItem){
            return $this->sendError(api_trans('award.card_is_activated'));
        }

        $depoName = $productItem->customer ? $productItem->customer->username: '';                           

        $serialNo = $value['game_code'].$value['batch_no'].$value['book_no'];
        
        $cardPurchase = Purchase::where('game_code', $value['game_code'])
                        ->where('batch_no', $value['batch_no'])
                        ->where('book_no', $value['book_no'])
                        ->where('serial_no', $serialNo)
                        ->first();            

        if($cardPurchase){
            return $this->sendError('This book has been scanned alreday');
        }                

        try {
            
            $data = [
                'game_code' => $value['game_code'],
                'batch_no' => $value['batch_no'],
                'book_no' => $value['book_no'],
                'serial_no' => $serialNo,
                'user_id' => $user->id,
                'depo_name' => $depoName
            ];
       
            DB::beginTransaction();
            $purchase = new Purchase();
            $purchase->fill($data);
            $purchase->save();
            DB::commit();
            
            //code...
        } catch (Exception $ex) {
            DB::rollback();
        }
       
        return $this->sendResponse('You Scan new Book successfully','You Scan new Book successfully');
    }

    public function getPurchaseReport(){
        $purchases = Purchase::whereUserId(auth()->id())
                    ->orderBy('id', 'DESC')
                    ->paginate(5)
                    ->toArray();
                
        $purchases = $purchases['data'];
        
        if(!$purchases)  return $this->sendResponse($purchases, api_trans('scan.no_scan')); //use lang translate from scan
        
        return $this->sendResponse($purchases, api_trans('scan.success'));
                   
    }
}
