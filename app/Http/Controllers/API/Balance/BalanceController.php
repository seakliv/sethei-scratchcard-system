<?php

namespace App\Http\Controllers\API\Balance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController;
use App\User;
use App\Model\UserAccountLog;
use App\Model\UserAccount;
use Carbon\Carbon;
use DB;
use Exception;

class BalanceController extends BaseController
{
    public function winToMain(){
        
        $this->validate(request(),[
            'amount' => 'required'
        ]);

        $amount = request()->get('amount');
        $user = User::where('id', auth()->guard('api')->user()->id)->first();    
        
        $cash_account = $user->cashAccount;
        
        if(!$cash_account){ 
            $cash_account = UserAccount::createCashAccount($user->id);
        }

        $mainAccount = $cash_account;  

        if($amount > 0){

            $log = [
                'user_id' => $user->id,
                'account_id' => $mainAccount->id,
                'log_type' => 1,
                'is_transfer' => 0,
                'amount' => $amount, 
                'win_money' => 0,
                'commission' => 0,
                'to_type' => 1,
                'to_user_id' => $user->id,
                'to_account_id' => $mainAccount->id,
                'manager_id' => 0,
                'log_number' => null,
                'created_at' => Carbon::now()
            ];

            if($user->win_money_card - $amount >= 0){

                 $user->win_money_card -= $amount;
                 $mainAccount->balance += $amount;
                 $user->update([
                    'win_money_card' => $user->win_money_card 
                 ]);
                 $mainAccount->update([
                     'balance' => $mainAccount->balance
                 ]); 

                 $log['abstract'] = 'LANG_WIN_CARD_MONEY_TO_BALANCE';
                 $log['balance'] =  $mainAccount->balance; 

             }else{
                 return $this->sendError(api_trans('account.insufficient_balance'));
             }
             
             UserAccountLog::create($log);

             return $this->sendResponse([
                'main_balance' => $mainAccount->balance,
                'win_money' => $user->win_money_card,
                'main_balance_format' => number_format($mainAccount->balance).' (R)',
                'win_money_format' => number_format($user->win_money_card).' (R)',
             ], api_trans('account.balance_transfer'));
        
        }else{
            return $this->sendError(api_trans('account.insufficient_balance'));   
        }
       
    }

    public function getBalance(){
        $userId = auth()->user()->id;
        $user = User::whereId($userId)->first();

        $cash_account = $user->cashAccount;
        
        if(!$cash_account){ 
            $cash_account = UserAccount::createCashAccount($user->id);
        } 

        $mainBalance = $cash_account;

        $card_bonus_account = $user->cardBonusAccount;
        
        if(!$card_bonus_account){ 
            $card_bonus_account = UserAccount::createCardBonusAccount($user->id);
        }
        $cardBonusBalance = $card_bonus_account;
    
       
        return $this->sendResponse([
            'main_balance' => $mainBalance->balance,
            'win_money' => $user->win_money_card,
            'card_bonus_return' => $cardBonusBalance->balance,
            'main_balance_format' => number_format($mainBalance->balance).' (R)',
            'win_money_format' => number_format($user->win_money_card).' (R)',
            'card_bonus_return_format' => number_format($cardBonusBalance->balance).' (R)'
        ], 'success');
    }

    public function getTransferLog(){
        $userId = auth()->user()->id;
        $logTransfers = UserAccountLog::with('toUser')
                                        ->where('user_id', $userId)
                                        ->whereIn('abstract', ['LANG_LABEL_TRANS', 'LANG_WIN_CARD_MONEY_TO_BALANCE'])
                                        ->orderBy('created_at', 'desc')
                                        ->paginate(15)
                                        ->toArray();
        
        // dd($logTransfers['data'][0]['to_user']);
        $data = [];
        $log_type = '';
       
        foreach($logTransfers['data'] as $item){
            
            if($item['log_type'] == 2){
                $log_type = api_trans('log_label.transferred');
            }elseif($item['log_type'] == 1){
                if($item['to_user']['id'] != auth()->id()){
                    $log_type = api_trans('log_label.received');
                }else{
                    $log_type = api_trans('log_label.win_to_balance');
                }  
            }

            $data [] = [
                'amount' => number_format($item['amount']).' (R)',
                'balance' => number_format($item['balance']).' (R)',
                'log_type' => $log_type,
                'created_at' => date('Y-m-d H:i A',strtotime($item['created_at'])),
                'to_user' => $item['to_user']['id'] != auth()->id() ? $item['to_user']['account_number'] : ''
            ];
        }                                

        return $this->sendResponse($data, 'success');
    }

    //** this function using for transfer bonus money 300 return card */ 
    public function transferBonus(){

        $users = User::whereHas(
            'cardBonusAccount', function ($query) {
                    $query->where('type_id', 6)->where('balance', '>', 0);
                }
            )
            ->with('cardBonusAccount')
            ->where('can_scan', 1)
            ->where('l1_id', 4548)
            ->where('level', 4) 
            ->get();

        $data = [];
       // dd($users->pluck('username')->toArray());
       DB::beginTransaction();
        try {
            
            foreach($users as $user){
                $returnBonusAccount = $user->cardBonusAccount;
                $mainAccount = $user->cashAccount;
                $amount = $returnBonusAccount->balance;
    
                $log = [
                    'user_id' => $user->id,
                    'account_id' => $mainAccount->id,
                    'log_type' => 1,
                    'is_transfer' => 0,
                    'amount' => $amount, 
                    'win_money' => 0,
                    'commission' => 0,
                    'to_type' => 1,
                    'to_user_id' => $user->id,
                    'to_account_id' => $mainAccount->id,
                    'manager_id' => 0,
                    'log_number' => null,
                    'created_at' => Carbon::now()
                ];
    
                $mainAccount->update([
                    'balance' => $mainAccount->balance + $amount
                ]);
                $returnBonusAccount->update([
                    'balance' => 0
                ]);
                
                $log['balance'] = $mainAccount->balance;
                $log['abstract'] = 'LANG_RETURN_BONUS_CARD_TO_MAIN_BALANCE';
             
                UserAccountLog::create($log);
    
                $data [] = $user;
            }
            
            DB::commit();
            
        } catch (Excepton $ex) {
            //throw $th;
            DB::rollback();
            dd($ex->getMessage());
        }
        
        dd($data);
    }
    //** end */

    function transferToOtherAccount(){
        $this->validate(request(),[
            'account_number' => 'required',
            'amount' => 'required',
            'pay_password' => 'required'
        ]);

        $account_number = request()->get('account_number');
        $amount = request()->get('amount');
        $pay_password = request()->get('pay_password');
        $user = auth()->user();
        
        $accounts = UserAccount::whereUserId($user->id)->get();

        if($user->account_number == $account_number) return $this->sendError('Can\'t be yourself');

        //** id = 4548 is id_line depo normal user in line 5d, cant transfer to depo card line*/
        if($user->l1_id != 4548) return $this->sendError('Can\'t transfer to Card Line');

        $main_account = $user->cashAccount;

        if($amount == 0 || $amount < 0) return $this->sendError(api_trans('account.insufficient_balance'));

        if($main_account->balance < $amount) return $this->sendError(api_trans('account.insufficient_balance'));

        $to_user = User::whereAccountNumber($account_number)->first();

        if(!$to_user) return $this->sendError(api_trans('auth.not_found'));

        //** id = 4548 is id_line depo scratch card, id = 3541 is is sethei company account */
        if($to_user->l1_id != 4548 && $to_user->id != 3541) return $this->sendError(api_trans('auth.not_found'));

        if(!$to_user->state) return $this->sendError('This user is disabled');

        if($user->pay_pass != md5($pay_password.$user->pass_salt)){
            return $this->sendError(api_trans('account.wrong_pay_password'));
        }

        $to_user_main_account = $to_user->cashAccount;

        $to_user_main_account->balance += $amount;
        $main_account->balance -= $amount;

        $log = [
            'user_id' => $user->id,
            'account_id' => $main_account->id,
            'log_type' => 2,
            'is_transfer' => 1,
            'amount' => $amount,
            'balance' => $main_account->balance,
            'to_type' => 2,
            'to_user_id' => $to_user->id,
            'to_account_id' => $to_user_main_account->id,
            'manager_id' => 0,
            'abstract' => 'LANG_LABEL_TRANS',
            'log_number' => UserAccountLog::generateLogNumber(2,$user->id),
            'created_at' => Carbon::now()
        ];

        $to_log = [
            'user_id' => $to_user->id,
            'account_id' => $to_user_main_account->id,
            'log_type' => 1,
            'is_transfer' => 1,
            'amount' => $amount,
            'balance' => $to_user_main_account->balance,
            'to_type' => 2,
            'to_user_id' => $user->id,
            'to_account_id' => $main_account->id,
            'manager_id' => 0,
            'abstract' => 'LANG_LABEL_TRANS',
            'log_number' => UserAccountLog::generateLogNumber(1,$to_user->id),
            'created_at' => Carbon::now()
        ];

        UserAccountLog::insert([$log,$to_log]);

        $to_user_main_account->save();
        $main_account->save();

        $card_bonus_account = $user->cardBonusAccount;
        
        if(!$card_bonus_account){ 
            $card_bonus_account = UserAccount::createCardBonusAccount($user->id);
        }
        $cardBonusBalance = $card_bonus_account;

        // return $this->sendResponse([
        //     'main_balance' => $main_account->balance,
        //     'win_money_card' => $user->win_money_card,
        // ], 'success');

        return $this->sendResponse([
            'main_balance' => $main_account->balance,
            'win_money_card' => $user->win_money_card,
            'card_bonus_return' => $cardBonusBalance->balance,
            'main_balance_format' => number_format($main_account->balance).' (R)',
            'win_money_format' => number_format($user->win_money_card).' (R)',
            'card_bonus_return_format' => number_format($cardBonusBalance->balance).' (R)'
        ], 'success');
    }

    public function getUsernameByAccountNumber(){
        
        $this->validate(request(),[
            'account_number' => 'required'
        ]);

        $user = User::whereAccountNumber(request()->get('account_number'))
                        ->where('account_number','<>',auth()->user()->account_number)->first();

        if($user){
            //return \response()->json(['username' => $user->username]);
            return $this->sendResponse($user->username, 'success');
        }else{
            return $this->sendError(api_trans('auth.not_found'));
        }
    }

    //public function getTrasferLog()

}
