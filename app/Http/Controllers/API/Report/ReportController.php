<?php

namespace App\Http\Controllers\API\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController;
use App\Model\ScannedAward;
use DB;


class ReportController extends BaseController
{

    public function getReportRange(){
        $page = request()->get('page',1);
        $limit = 15;
        $offset = ($page - 1) * $limit;
    }

    public function getAllScanned(){

        $user_id = request()->user_id ?: auth()->id();

        //***Get Report Group by date only***//
        // $scans = ScannedAward::select(DB::raw('DATE(created_at) as date'))
        //                         ->whereUserId($user_id)
        //                         ->distinct()
        //                         ->orderBy('date','DESC')
        //                         ->paginate(15)
        //                         ->toArray();

        $scans = ScannedAward::whereUserId($user_id)
                                ->orderBy('created_at','DESC')
                                ->paginate(15)
                                ->toArray();                        
        
        $scans['scans'] = $scans['data'];
        unset($scans['data']);
        
        // dd($scans);
        foreach($scans['scans'] as $i => $scan){
            $scans['scans'][$i]['security_code'] =  $scan['security_code'];
            $scans['scans'][$i]['award_amount'] = number_format((double)$scan['award_amount']).' (R)';
            $scans['scans'][$i]['created_at'] = date('Y-m-d H:i A',strtotime($scan['created_at']));
        }
    
        if(!$scans['scans'])  return $this->sendResponse($scans, api_trans('scan.no_scan'));

        return $this->sendResponse($scans, api_trans('scan.success'));
    }

    public function getScanDetail(){

        $date = request()->get('date');
        $user_id = request()->user_id ?: auth()->id(); 
        
        $scanDetail = ScannedAward::whereUserId($user_id)
                                ->whereDate('created_at', '=', $date)
                                ->orderBy('id','DESC')
                                ->get();

        $scans = [];
        foreach($scanDetail as $key => $item){
            $scans[] = [
                'date' => Date($item->created_at),
                'award_amount' => $item->award_amount,
                'code_scanned' => $item->security_code 
            ];
        } 
        
        if(!$scans) return $this->sendResponse($scans, 'No data available');

        return $this->sendResponse($scans, 'success');                        

    }
}
