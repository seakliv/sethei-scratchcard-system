<?php

namespace App\Http\Controllers\API\User;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController;
use DB;
use App\Model\UserAccount;
use App\Model\UserAccountLog;
use App\User;
use App\Constants\UserBallAccount;
use App\Constants\Account;
use Carbon\Carbon;

class AccountLogController extends BaseController
{
    public function getAccountLog(){
        $key = request()->key;
        $logs = [];
        $abstract = '';

        $user = auth()->user();
        $languages = getAdminLanguageData();
        if(!$user)
            return $this->sendError(api_trans('auth.permission_denied'));

        switch($key){
            case 'transfer':
                $abstract = 'LANG_LABEL_TRANS';
                break;  
            case 'commission':
                $abstract = 'LANG_COMMISSION_TO_BALANCE';
                break; 
            case 'winmoney':
                $abstract = 'LANG_WIN_MONEY_TO_BALANCE';
                break;   
        }
        
        $logs = UserAccountLog::select('id','created_at','to_user_id','amount','balance','abstract','log_type','is_transfer')
                                ->whereUserId($user->id)
                                ->whereAbstract($abstract)
                                ->with('toUser')
                                ->orderBy('id','DESC')
                                ->paginate(15)
                                ->toArray(); 
        $logsFormat = [];
        foreach($logs['data'] as $log){
            $toUser = isset($log['to_user']) ? ['name' => $log['to_user']['username'], 'account_no' => $log['to_user']['account_number']] : [];
            $logsFormat[] = [
                'id' => $log['id'], 
                'date' => date('Y-m-d H:i A',strtotime($log['created_at'])), 
                'to_user_name' => isset($log['to_user']) ? $log['to_user']['username'] : null,
                'to_user_account' => isset($log['to_user']) ? $log['to_user']['account_number'] : null,
                'amount' => currencyFormat($log['amount']),
                'balance' => currencyFormat($log['balance']),
                'abstract' => $languages[$log['abstract']],
                'type' => $languages[Account::accountInOut($log['log_type'])],
                'is_transfer' => $log['is_transfer'],
                'human_time' => Carbon::parse(date('Y-m-d H:i:s',strtotime($log['created_at'])))->diffForHumans()
            ];
        }
        $logs['logs'] = $logsFormat;
        unset($logs['data']);

        return $this->sendResponse($logs, 'OK');
    }

    public function getAccountLogDetail($id){
        $languages = getAdminLanguageData();
        $log = UserAccountLog::select('id','amount','balance','abstract','created_at','to_user_id','log_type','is_transfer')
                                ->with('toUser')
                                ->whereId($id)->whereUserId(auth()->id())->first();
        if($log){
            $log = $log->toArray();
            $log = [
                'id' => $log['id'],
                'amount' => currencyFormat($log['amount']),
                'balance' => currencyFormat($log['balance']),
                'created_at' => date('Y-m-d H:i A',strtotime($log['created_at'])),
                'abstract' => $languages[$log['abstract']],
                'to_user_name' => $log['to_user']['username'],
                'to_user_account_no' => $log['to_user']['account_number'],
                'type' => $languages[Account::accountInOut($log['log_type'])],
                'is_transfer' => $log['is_transfer'],
                'human_time' => Carbon::parse(date('Y-m-d H:i:s',strtotime($log['created_at'])))->diffForHumans()
            ];
        }
        return $this->sendResponse($log, 'OK');
    }
}
