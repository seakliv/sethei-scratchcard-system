<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController;
use App\User;
use Validator;
use Illuminate\Support\Facades\Auth;
use DB;

class AuthController extends BaseController
{
    public function login(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);
            
        if($validator->fails()){
            return $this->sendError(api_trans('auth.field_required'));      
        }
        
        $user = User::where(['username' => $request->username])->orWhere('account_number',$request->username)->first();
        if(!$user || $user->password != md5($request->password.$user->pass_salt) 
            || $user->state !=1){ 
            return  $this->sendError(api_trans('auth.failed'));
        }

        //** use this condition for allow login only line depo-card and Test App */
        if( $user->id != 985 && $user->l1_id != 4548){return $this->sendError(api_trans('auth.failed'));}

        //** user this condition to clear all account who login the same account with many devices */
        DB::table('oauth_access_tokens')->whereUserId($user->id)->update(['revoked' => 1]);
        
        $accessToken = $user->createToken('Scratch Card')->accessToken;

        return $this->sendResponse([
                    'username' => $user->username,
                    'id' => $user->id,
                    'accountNo' => $user->account_number,
                    'isFreelancer' => $user->is_freelancer == 1 ? 1 : 0,
                    'accessToken' => $accessToken],
                    api_trans('auth.successful_login'));
    }

    public function logout(){
        DB::table('oauth_access_tokens')->whereUserId(auth()->id())->update(['revoked' => 1]);
        return $this->sendResponse('',api_trans('auth.successful_logout'));
    }

    function changePassword(){
        $this->validate(request(),[
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_new_password' => 'required',
            'type' => 'required'
        ]);
        
        $user = auth()->user();
        $old_password = request()->get('old_password');
        $new_password = request()->get('new_password');
        $confirm_new_password = request()->get('confirm_new_password');
        $type = request()->get('type');

        if(strlen($new_password) < 6){
            return $this->sendError(api_trans('passwords.password'));
        }

        if($new_password != $confirm_new_password){
            return $this->sendError(api_trans('passwords.password_not_match'));
        }

        if($old_password == $new_password){
            return $this->sendError(api_trans('passwords.new_password_same_as_old_password'));
        }

        $in_system_password = $type == 0 ? $user->password : $user->pay_pass;

        if($in_system_password == md5($new_password.$user->pass_salt)){
            return $this->sendError(api_trans('passwords.new_password_same_as_old_password'));
        }

        if($in_system_password != md5($old_password.$user->pass_salt)){
            return $this->sendError(api_trans('passwords.old_password_not_correct'));
        }

        $new_password = md5($new_password.$user->pass_salt);
        if($type == 0)
            $user->password = $new_password;
        else
            $user->pay_pass = $new_password;

        $user->save();
        return $this->sendResponse(api_trans('passwords.change_password_success'),'sucess');
    }
}
