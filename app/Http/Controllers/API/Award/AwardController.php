<?php

namespace App\Http\Controllers\API\Award;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\BaseController;
use App\Model\Award;
use App\User;
use App\Constants\ScratchCard;
use App\Model\UserAccountLog;
use App\Model\UserAccount;
use App\Model\ScannedAward;
use Carbon\Carbon;
use App\Model\Product;
use App\Model\ProductItem;



class AwardController extends BaseController
{
    public function verifyAward(){

        $securityCode = request()->code;

        $award = Award::whereSecurityCode(ScratchCard::encryptSecurityCode($securityCode))->first();

        if(!$award) {
            return $this->sendError(api_trans('award.invalid_code'));
        }

        $user = auth()->user();
        if($user->can_scan != 1 && !($user->username == 'Test App' || $user->username == 'cmanloksio')){
            return $this->sendError(api_trans('award.can_scan'));
        }

        $productItem = ProductItem::where('game_code', $award->game_code)
                                    ->where('batch_no', $award->batch_no)
                                    ->where('book_no', $award->book_no)
                                    ->where('is_activated', 1)
                                    ->whereNotNull('customer_id')
                                    ->when(auth()->id() != 985, function($q){
                                        $q->where('customer_id','!=',985);
                                    })
                                    ->first();

        if(!$productItem && !($user->username == 'Test App' || $user->username == 'cmanloksio')){
            // return $this->sendError(api_trans('award.card_is_activated'));
        }

        if($award->is_claimed){
            return $this->sendError(api_trans('award.is_claimed'));
        }

        $product = Product::where('game_code', $award->game_code)->first();
        $productPrice = $product->ticket_price;

        $cash_account = $user->cashAccount;
        if(!$cash_account){
            $cash_account = UserAccount::createCashAccount($user->id);
        }

        $userLog = [
            'user_id' =>  $user->id,
            'account_id' => $cash_account->id,
            'amount' => 0,
            'win_money' => 0,
            'abstract' => '',
            'log_type' => 1,
            'to_type' => 1
        ];

        $scanLog = [
            'award_level' => $award->award_level,
            'security_code' => $securityCode,
            'user_id' => $user->id,
            'award_id' => $award->id,
            'award_amount' => 0,
            'abstract' => '',
        ];

        $winMoney = 0;
        $type = '';

        $card_bonus_account = $user->cardBonusAccount;
        if(!$card_bonus_account){
            $card_bonus_account = UserAccount::createCardBonusAccount($user->id);
        }

        if($award->award_level === 15){
            $winMoney = $productPrice;

            $userLog['amount'] = $productPrice;
            $userLog['win_money'] = $winMoney + $user->win_money_card;
            $userLog['abstract'] = 'LANG_LABEL_WIN_CARD_BONUS';
            UserAccountLog::create($userLog);

            $scanLog['award_amount'] = $productPrice;
            $scanLog['abstract'] = 'Win Bonus';
            ScannedAward::create($scanLog);
        }else{
            $winMoney= $award->award_amount;

            $scanLog['award_amount'] = $winMoney;
            $scanLog['abstract'] = 'Win Money';
            ScannedAward::create($scanLog);

            $userLog['amount'] = $award->award_amount;
            $userLog['win_money'] = $winMoney + $user->win_money_card;
            $userLog['abstract'] = 'LANG_LABEL_WIN_CARD_SUM';
            UserAccountLog::create($userLog);
        }

        $award->update([
            'claimed_by' => auth()->user()->id,
            'is_claimed' => TRUE
        ]);

        $user->update([
            'win_money_card' => $winMoney + $user->win_money_card
        ]);

        $type = "Win";
        return $this->sendResponse([
            'type' => $type,
            'bonus_return' => $card_bonus_account->balance,
            'level' => $award->award_level,
            'amount' => $winMoney,
            'user_win_balance' => $user->win_money_card,
        ], api_trans('award.success').number_format($winMoney).' (R) '
           .api_trans('award.success_level'). $award->award_level
           .api_trans('award.success_transfer'));
    }

}

