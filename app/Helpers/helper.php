<?php
use App\Model\Currency;
use App\Model\Exchange;
use App\Model\Language;
use App\Model\LanguageAdmin;
use App\User;

function getCustomers(){
    return User::whereState(1)->whereUserType(2)->whereComDirect(0)
                ->where('l1_id',4548)
                ->when(auth()->id() == 44, function($q){
                    $q->orWhere('id','985');
                })
                ->pluck('username', 'id');
}

function pushNotification($title,$body){
    $pushNotifications = notificationInstance();
    $payload = [
        'title' => $title,
        'description' => $body,
        'channel_id' => str_random(10)
    ];

    $publishResponse = $pushNotifications->publishToInterests(
    ["result"],
    [
        "fcm" => [
            'data' => ['result' => json_encode($payload)],
            "notification" => [
                "title" => $title,
                "body" => $body,
            ],
        ],
    ]
    );
}

function pushNotificationToUser($title,$body,$userIds = []){
    $beamsClient = notificationInstance();
    $payload = [
        'title' => $title,
        'description' => $body,
        'channel_id' => str_random(10)
    ];
    $publishResponse = $beamsClient->publishToUsers(
        $userIds,
        array(
          "fcm" => array(
            'data' => ['result' => json_encode($payload)],
            "notification" => array(
              "title" => $title,
              "body" => $body
            )
          )
    ));
}

function notificationInstance(){
    return new \Pusher\PushNotifications\PushNotifications(array(
        "instanceId" => "a9c53072-ab8d-4ff5-9950-3ba6c0a95651",
        "secretKey" => "E9817E476B7951FFF4B450D715E3D93515383D0A744AC0F79DF899D341A6AFA1",
    ));
}

function getFrontendGeneralSetting(){

    $settings = [];
    $settings['exchangeRate'] = [
        'dollarToRiel' => exchangeRateFromDollarToRiel(),
        'rielToDollar' => exchangeRateFromRielToDollar(),
    ];
    $settings['language'] = getAdminLanguageData();
    $settings['avaiableLanguage'] = getAvailableLanguage();
    return $settings;
}

function getAvailableLanguage(){
    $language = Language::orderBy('sort','ASC')->get()->keyBy('language_field');
    return $language;
}
function getAdminLanguageData(){
    $currentLang = 'English';
    if(app()->getLocale() == 'en'){
        $currentLang = 'english';
    }elseif(app()->getLocale() == 'zh'){
        $currentLang = 'chinese';
    }elseif(app()->getLocale() == 'kh'){
        $currentLang = 'khmer';
    }
    return LanguageAdmin::all()->pluck($currentLang,'label')->toArray();
}

function api_trans($msg,$key = []){
    return trans($msg,$key, getRequestLanguage());
}
function getRequestLanguage(){
    $lang = request()->header('Accept-Language') ?? 'en';
    return $lang;
}

function getUserLevelTitle($level){
    $languages = getAdminLanguageData();
    $title = $languages['LANG_LABEL_L1_TITLE'];
    switch($level){
        case 1: $title = $languages['LANG_LABEL_L1_TITLE']; break;
        case 2: $title = $languages['LANG_LABEL_L2_TITLE']; break;
        case 3: $title = $languages['LANG_LABEL_L3_TITLE']; break;
        case 4: $title = $languages['LANG_LABEL_L4_TITLE']; break;
        case 5: $title = $languages['LANG_LABEL_L5_TITLE']; break;
        case 6: $title = $languages['LANG_LABEL_L6_TITLE']; break;
        case 7: $title = $languages['LANG_LABEL_L7_TITLE']; break;
        case 8: $title = $languages['LANG_LABEL_L8_TITLE']; break;
        case 9: $title = $languages['LANG_LABEL_THISUSER']; break;
    }
    return $title;
}

function currencyFormat($amount,$decimal = 2){
    return number_format($amount, $decimal);
}
function stringToDouble($str){
    return doubleval(str_replace(',','',$str));
}
function exchangeRateFromRielToDollar(){
    $currency = Exchange::where('from_currency',1)->first();
    return $currency ? 1 / $currency->rate : 0;
}
function exchangeRateFromDollarToRiel(){
    $currency = Exchange::where('from_currency',1)->first();
    return $currency ? $currency->rate : 0;
}

function bookNumberFormat($no){
    return addPrefixStringPad($no,7,'0',STR_PAD_LEFT);
}

function addPrefixStringPad($input,$length,$string){
    return str_pad($input, $length, $string, STR_PAD_LEFT);
}
function addSuffixStringPad($input,$length,$string){
    return str_pad($input, $length, $string, STR_PAD_RIGHT);
}

function permissions(){
    return [
        'en' => [
            'manager' => [
                'module' => 'Manager',
                'permissions' => [
                    ['text' => 'View Manager Account', 'value' => 'view-manager-account'],
                    ['text' => 'Add New Manager Account', 'value' => 'add-new-manager-account'],
                    ['text' => 'Manager Account Modification', 'value' => 'manager-account-modification'],
                    ['text' => 'View Role', 'value' => 'view-role'],
                    ['text' => 'Add New Role', 'value' => 'add-new-role'],
                    ['text' => 'Role Modification', 'value' => 'role-modification']
                ]
            ],
            'product' => [
                'module' => 'Product',
                'permissions' => [
                    ['text' => 'View Product', 'value' => 'view-product'],
                    ['text' => 'Add New Stock', 'value' => 'add-new-product'],
                    ['text' => 'Stock Modification', 'value' => 'product-modification']

                ]
            ],
            'stock' => [
                'module' => 'Stock In',
                'permissions' => [
                    ['text' => 'View Stock', 'value' => 'view-stock'],
                    ['text' => 'Add New Stock', 'value' => 'add-new-stock'],
                    ['text' => 'Stock Modification', 'value' => 'stock-modification'],
                    ['text' => 'Export Stock', 'value' => 'export-stock'],

                ]
            ],
            'sale' => [
                'module' => 'Sale',
                'permissions' => [
                    ['text' => 'View Sale', 'value' => 'view-sale'],
                    ['text' => 'Add New Sale', 'value' => 'add-new-sale'],
                    ['text' => 'Sale Modification', 'value' => 'sale-modification'],
                    ['text' => 'Export Sale', 'value' => 'export-sale']

                ]
            ],
            'award' => [
                'module' => 'Award',
                'permissions' => [
                    ['text' => 'View Award Data', 'value' => 'view-award-data'],
                    ['text' => 'Import Award Data', 'value' => 'import-award-data'],
                ]
            ],
            'scanned-log' => [
                'module' => 'Scanned Log',
                'permissions' => [
                    ['text' => 'View Scanned Log', 'value' => 'view-scanned-log'],
                    ['text' => 'Export Scanned Log', 'value' => 'export-scanned-log'],
                ]
            ],

        ],
    ];
}


function echoTime($title){
    echo $title." ".date('Y-m-d-H:i:s.').preg_replace("/^.*\./i","", microtime(true))."<br>";
}
