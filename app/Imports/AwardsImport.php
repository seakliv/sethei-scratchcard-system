<?php

namespace App\Imports;

use App\Model\Award;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Constants\ScratchCard;
use Exception;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Illuminate\Contracts\Queue\ShouldQueue;

class AwardsImport implements ToModel, WithStartRow, WithBatchInserts, WithChunkReading, ShouldQueue
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {   
        ini_set('memory_limit',-1);
        ini_set('max_execution_time',0);

        $detailCode = ScratchCard::getDetailSecurityCode($row[0]);

        if(!$detailCode && count($detailCode) <= 0) return;
        
        return new Award([
            'game_code' => $row[1], 
            'batch_no' => $detailCode['batch_no'], 
            'book_no' => $row[2], 
            'ticket_no' => $row[3], 
            'award_verification_code' => $detailCode['award_verification_code'],
            'logistic_code' => $row[1].$row[2].$row[3], 
            'security_code' => ScratchCard::encryptSecurityCode($row[0]),
            'box_barcode' => $row[4], 
            'award_amount' => $row[5],
            'imported_by' => auth()->id()
        ]);
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 2000;
    }

    public function startRow(): int
    {
        return 2;
    }
}
