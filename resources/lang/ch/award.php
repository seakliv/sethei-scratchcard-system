<?php

return [
    'invalid_code' => '哎呀！无效的QR码',
    'award_level' => '抱歉!请稍后再试',
    'is_claimed' => '您已经领奖了',
    'success' => '您扫描了刮刮卡并赢得了 ',
    'success_level' => '奖励级别 ',
    'success_transfer' => '! 款项将很快转入您的帐户!',
    'win_one_more' => '您的卡又赢了一张卡',
    'card_is_activated' => "您的卡未激活",
    'can_scan' => '您的帐户无扫描权限' 
];