<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '这些凭据与我们的记录不匹配。',
    'throttle' => '登录尝试次数过多。请在：seconds 秒后重试。',
    'successful_login' => '登录成功！',
    'successful_logout' => '登出成功！',
    'field_required' => '请填写必填字段！',
    'permission_denied' => '没有权限！',
    'not_found' => 'Account Not found',

];