<?php

return [
    'failed_to_trasfer,' => 'You transfer process is failed! Please try again!',
    'failed_amount_input' => 'The amount must be greater than 0!',
    'wrong_pay_password' => 'The pay password does not match!',
    'over_balance' => 'The amount is over your available balance!',
    'out_of_balance' => 'Please check your cash balance!',
    'unknown_account' => 'Unknown account proccess!',
    'unknown_target_user' => 'Unknown target user!',
    'insufficient_balance' => 'Insufficient balance',
    'balance_transfer' => 'Balance Transferred',
    'balance_recharge' => 'Balance Recharge',
    'balance_withdraw' => 'Balance Withdraw',
    'recharge_msg' => ':amount (R) has been recharged to your account by :name.',
    'withdraw_msg' => ':amount (R) has been withdrawed to your account by :name.',
    'receive_msg' => 'You receive :amount (R) from :name.'
];