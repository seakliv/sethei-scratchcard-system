<?php

return [

    'invalid_code' => 'Whoops! Invalid QR code',
    'award_level' => 'Sorry! Please try again later',
    'is_claimed' => 'You have already claimed the prize.',
    'success' => 'You scanned a scratchcard and win the prize of ',
    'success_level' => 'with award level ',
    'success_transfer' => '! The money will be transferred to your account shortly!',
    'win_one_more' => 'Your card won one more card',
    'card_is_activated' => "Your Card isn't activated",
    'can_scan' => 'Your Account No Permission to Scan' 
];