<?php
return [
    'transferred' => 'Transferred',
    'received' => 'Received',
    'win_to_balance' => 'Win to Balance',
    'to_user' => 'To User',
    'from_user' => 'From User'
];