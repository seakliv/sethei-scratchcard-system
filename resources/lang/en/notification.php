<?php

return [
    'ball_result_title_msg' => 'Sethei Ball Result!',
    'result_time' => 'Result Time',
    'result' => 'Result',
    'congratulation' => 'Congratulation!',
    'win_msg' => ':user won :amount $.',

];