<?php

return [
    'no_cycle'  => 'Cycleបានចប់!',
    'no_order' => 'មិនមានលេខភ្នាល់!',
    'out_of_range_bet' => 'លេខភ្នាល់ធំជាងលេខកំណត់!',
    'no_ticket' => 'មិនមានសំបុត្រភ្នាល់!',
    'no_config' => 'No game config found!',
    'cycle_stop' => 'Cycleបច្ចុប្បន្នបានចប់!',
    'failed_to_order' => 'ការបញ្ជាទិញរបស់អ្នកមិនបានជោកជ័យ! សូមព្យាយាមម្ដងទៀត!'
];