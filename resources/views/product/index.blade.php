@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Products</span></h4>
        </div>
        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                @can('view-stock')
                <a href="{{ route('product.stock.index') }}" class="btn btn-success btn-sm mr-2">
                    <i class="icon-home7 text-white"></i> View Stock
                </a>
                @endcan

                @can('add-new-stock')
                <a href="{{ route('stocks.index') }}" class="btn btn-success btn-sm mr-2">
                    <i class="icon-home7 text-white"></i> Stock In
                </a>
                @endcan

                @can('add-new-product')
                <a href="{{ route('products.create') }}" class="btn btn-primary btn-sm">
                    <i class="icon-stack-picture text-white"></i> Add New Product
                </a>
                @endcan
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Product</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        <div class="card-header">
            @include('includes.success-msg')
            @include('includes.error-msg')
            <div class="mb-1">
                <button class="btn btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#filter" aria-expanded="false" aria-controls="collapseExample">
                    <i class="icon-filter3"> </i> Filter
                </button>

                <div class="collapse mt-2" id="filter">
                    {{ Form::open(['route' => 'products.index', 'method' => 'GET']) }}
                        <div class="row mb-2">
                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <label for="">Game Code</label>
                                {{Form::text("game_code",request()->game_code,["class" => "form-control "])}}
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <label for="">Batch No</label>
                                {{Form::text("batch_no",request()->batch_no,["class" => "form-control "])}}
                            </div>
                        </div>
                        <button type="submit" class="btn btn-sm btn-success">Search</button>
                        <a href="{{route('products.index')}}" class="btn btn-sm btn-danger">Reset</a>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-slate-800">
                            <th>#</th>
                            <th>Game Code</th>
                            <th>In Stock</th>
                            <th>Tickets Per Book</th>
                            <th>Total Tickets</th>
                            <th>One Ticket Price (R)</th>
                            <th>Estimate Total Price ($)</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($products) && $products->count() > 0)
                            @foreach($products as $key => $row)
                            <tr>
                                <td>{{($products->perPage() * ($products->currentPage() - 1)) + $key + 1}}</td>
                                <td width="200px">{{ $row->game_code }} – {{ $row->name }}</td>
                                <td><a href="{{route('product.stock.index')}}?game_code= {{ $row->game_code }}"  class="text-primary"> {{ $row->product_items ? $row->available_product_items->count() : 0 }} </a>  {{ $row->product_unit ? $row->product_unit->name : ''}}</td>
                                <td >{{ currencyFormat($row->ticket_qty,0) }}</td>
                                <td >{{ currencyFormat($row->ticket_qty * $row->available_product_items->count(),0) }}</td>
                                <td>R {{ $row->ticket_price ? currencyFormat($row->ticket_price) : 0 }}</td>
                                <td>R {{ $row->available_product_items ? currencyFormat($row->available_product_items->count() * $row->ticket_qty * $row->ticket_price) : 0.00 }}</td>
                                <td width="300px" class="group-btn-action">
                                    <div class="btn-group">
                                        @can('view-stock')
                                            <a href="{{route('product.stock.index')  }}?game_code= {{ $row->game_code }}" class="btn btn-sm btn-outline bg-success border-success text-success-800 btn-icon border-2"><i class="icon-home7"></i> View Stock</a>
                                        @endcan

                                        <a href="{{route('products.edit',$row->id)  }}" class="btn btn-sm btn-outline bg-primary border-primary text-primary-800 btn-icon border-2 ml-1"><i class="icon-pencil7"></i> Edit</a>

                                        @can('product-modification')
                                            <button type="button" class="btn btn-sm btn-outline bg-danger border-danger text-danger-800 btn-icon border-2 delete ml-1" data-route="{{route('products.destroy',$row->id)}}"><i class="icon-trash"></i> Delete</button>
                                        @endcan
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8"> No data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if(isset($products) && count($products) > 0)
            <div class="card-footer">
                @if($products->hasPages())
                    <div class="mb-2">
                        {!! $products->appends(Input::except('page'))->render() !!}
                    </div>
                @endif
                <div>
                    Showing {{$products->firstItem()}} to {{$products->lastItem()}}
                    of  {{$products->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/demo_pages/form_inputs.js"></script>
<script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
@endsection
