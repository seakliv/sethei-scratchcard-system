@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">In Stock Data {{ request()->game_code && request()->game_code != 'any' && isset($products) ? ' – ' . $products[request()->game_code]->name. '( '.request()->game_code.' )' : '' }}</span></h4>
        </div>
        {{-- <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{ route('products.create') }}" class="btn btn-primary btn-sm">
                    <i class="icon-stack-picture text-white"></i> Add New
                </a>
            </div>
        </div> --}}
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('products.index')}}" class="breadcrumb-item"> Products</a>
                <span class="breadcrumb-item active">Stock Data</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        <div class="card-header">
            @include('includes.success-msg')
            @include('includes.error-msg')
            <div class="mb-1">
                <button class="btn btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#filter" aria-expanded="false" aria-controls="collapseExample">
                    <i class="icon-filter3"> </i> Filter
                </button>

                <div class="collapse mt-2" id="filter">
                    {{ Form::open(['route' => 'product.stock.index', 'method' => 'GET']) }}
                        <div class="row mb-2">
                            <div class="col-lg-2 col-md-3 col-sm-6">
                                <label for="">Game Code</label>
                                <select name="game_code" class="form-control">
                                    <option value="any">Any</option>
                                    @foreach($products as $item)
                                        <option value="{{$item->game_code}}" {{request()->game_code == $item->game_code ? 'selected' : ''}}>{{ $item->game_code }} – {{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-6">
                                <label for="">Batch No</label>
                                {{Form::text("batch_no",request()->batch_no,["class" => "form-control "])}}
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-6">
                                <label for="">Box Code</label>
                                {{Form::text("box_code",request()->box_code,["class" => "form-control "])}}
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-6">
                                <label for="">Book No</label>
                                {{Form::text("book_no",request()->book_no,["class" => "form-control "])}}
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-6">
                                <label for="">Stock Status</label>
                                <select name="is_sold" class="form-control">
                                    <option value="any" >Any</option>
                                    <option value="0" {{ request()->is_sold == '0' || !request()->is_sold ? 'selected' : ''}}>Available</option>
                                    <option value="1" {{ request()->is_sold == '1' ? 'selected' : ''}}>Sold Out</option>
                                </select>
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-6">
                                <label for="">Activated Status</label>
                                <select name="is_activated" class="form-control">
                                    <option value="any" >Any</option>
                                    <option value="0" {{ request()->is_activated == '0' ? 'selected' : ''}}>Activated</option>
                                    <option value="1" {{ request()->is_activated == '1' ? 'selected' : ''}}>Deactivated</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-sm btn-success">Search</button>
                        <a href="{{route('product.stock.index')}}" class="btn btn-sm btn-danger">Reset</a>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-slate-800">
                            <th>#</th>
                            <th>Serial Code</th>
                            <th>Game Code</th>
                            <th>Batch No</th>
                            <th>Box Code</th>
                            <th>Book No</th>
                            <th>Stock Status</th>
                            <th>Activated Status</th>
                            <th>Unit</th>
                            <th style="width:100px">Price/Unit ($)</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($productItems) && $productItems->count() > 0)
                            @foreach($productItems as $key => $row)
                            <tr>
                                <td>{{($productItems->perPage() * ($productItems->currentPage() - 1)) + $key + 1}}</td>
                                <td>{{ $row->code }}</td>
                                <td>{{ $row->game_code }} {{ $row->product ? ' – '.$row->product->name : '' }}</td>
                                <td>{{ $row->batch_no }}</td>
                                <td>{{ $row->box_code }}</td>
                                <td>{{ $row->book_no }}</td>
                                <td>
                                    @if($row->is_sold)
                                        <span class="badge badge-danger">Sold Out</span>
                                    @else
                                        <span class="badge badge-success">Available</span>
                                    @endif
                                </td>
                                <td>
                                    @if(!$row->is_activated)
                                        <span class="badge badge-danger">Deactivated</span>
                                    @else
                                        <span class="badge badge-success">Activated</span>
                                    @endif
                                </td>
                                <td>{{ $row->product && $row->product->product_unit ? $row->product->product_unit->name : 'N/A' }}</td>
                                <td >{{ $row->product ? 'R '. currencyFormat($row->product->ticket_price * $row->product->ticket_qty) : 0 }}</td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9"> No data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if(isset($productItems) && count($productItems) > 0)
            <div class="card-footer">
                @if($productItems->hasPages())
                    <div class="mb-2">
                        {!! $productItems->appends(Input::except('page'))->render() !!}
                    </div>
                @endif
                <div>
                    Showing {{$productItems->firstItem()}} to {{$productItems->lastItem()}}
                    of  {{$productItems->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/demo_pages/form_inputs.js"></script>
<script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
@endsection
