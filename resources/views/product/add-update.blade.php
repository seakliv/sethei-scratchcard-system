@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ isset($product) ? 'Update New Product' : 'Add New Product' }}</span></h4>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href=" {{ route('products.index') }} " class="btn btn-primary btn-sm">
                    <i class="icon-square-left mr-1"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ route('dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Product</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @if(isset($product))
            {{ Form::model($product,['route' => ['products.update',$product->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) }}
            @else
            {{ Form::open(['route' => 'products.store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
            @endif
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('game_code') ? 'text-danger' : '' }}">Game Code</label>
                        <div class="position-relative">
                            {{
                                Form::text("game_code",isset($product) ? $product->game_code : '',
                                    ["class" => "form-control ".($errors->has('game_code') ? 'border-danger' : ''),"placeholder" => ""])
                            }}
                            @if($errors->has('game_code'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('game_code'))
                            <span class="form-text text-danger">{{ $errors->first('game_code') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('name') ? 'text-danger':'' }}">Name</label>
                        <div class="position-relative">
                            {{
                                Form::text("name",isset($product) ? $product->name : '',
                                    ["class" => "form-control ".($errors->has('name') ? 'border-danger' : ''),"placeholder" => ""])
                            }}
                            @if($errors->has('name'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('name'))
                            <span class="form-text text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('unit_id')?'text-danger':'' }}">Product unit</label>
                        <div class="position-relative">
                            {{Form::select("unit_id",$units,isset($units) ? $units : null ,["class" => "form-control"])}}
                        </div>
                        @if($errors->has('unit_id'))
                            <div class="form-control-feedback text-danger">
                                <i class="icon-spam"></i>
                            </div>
                        @endif
                    </div>

                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('ticket_qty')?'text-danger':'' }}">Ticket qty in a book</label>
                        <div class="position-relative">
                            {{Form::number("ticket_qty",null,["class" => "form-control ".($errors->has('ticket_qty')?'border-danger':'')])}}
                            @if($errors->has('ticket_qty'))
                                <div class="form-control-feedback text-danger">
                                    <span class="form-text text-danger">{{ $errors->first('ticket_qty') }}</span>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('ticket_price')?'text-danger':'' }}">One Ticket Price (R)</label>
                        <div class="position-relative">
                            {{Form::number("ticket_price",null,["class" => "form-control ".($errors->has('ticket_price')?'border-danger':'')])}}
                            @if($errors->has('ticket_price'))
                                <div class="form-control-feedback text-danger">
                                    <span class="form-text text-danger">{{ $errors->first('ticket_price') }}</span>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @can('product-modification')
        <div class="card-footer">
            <button type="submit" class="btn btn-success">
                <i class="icon-folder mr-1"></i> {{ isset($product) ? 'Update' : 'Save'}}
            </button>
        </div>
        @endcan

        {{ Form::close() }}
    </div>
</div>
@endsection
