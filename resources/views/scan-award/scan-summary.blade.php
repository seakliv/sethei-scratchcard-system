@extends('layouts.master')
<style>
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
    #address{
                width:300px;

        }
    #address ul {

        margin: 0px;
        padding: 0px;
        overflow-wrap: break-word;
        text-decoration: none;
    }
</style>

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Awards Scanned Summary</span></h4>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                @if(request()->award_label === 'Return Bonus' && auth()->id() == 44)
                <a href="{{ route('transfer-bonus') }}" class="btn btn-primary btn-sm">
                    <i class="icon-file-plus2 text-white"></i> Transfer Bonus
                </a>
                @endif
                <a href="" data-route="{{ route('download-scan-summary') }}" target="_BLANK" class="btn bg-teal-400 download-excel btn-sm mr-2">
                    <i class="icon-download mr-1 text-white"></i> Download Excel
                </a>
            </div>
        </div>

    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Awards-Scanned-Summary</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        <div class="card-header">
            @include('includes.success-msg')
            @include('includes.error-msg')
            <div class="mb-1">
                <button class="btn btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#filter" aria-expanded="false" aria-controls="collapseExample">
                    <i class="icon-filter3"> </i> Filter
                </button>

                <div class="collapse mt-2" id="filter">
                    {{ Form::open(['route' => 'scan-summary', 'method' => 'GET']) }}
                        <div class="row mb-2">
                            {{-- <div class="col-lg-3 col-md-3 col-sm-6">
                                <label for="">Award Level</label>
                                {{Form::number("award_level", request()->award_level, ["class" => "form-control ", "placeholder" => "Enter Award-Level"])}}
                            </div> --}}

                            {{-- <div class="col-lg-3 col-md-3 col-sm-6">
                                <label for="">Ticket Number</label>
                                {{Form::text("ticket_number", request()->ticket_number, ["class" => "form-control ", "placeholder" => "Enter Ticket-Number"])}}
                            </div> --}}

                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <label for="">Award Lable</label>
                                {{ Form::select("award_label", ['Return Bonus' => 'Return Bonus', 'Win Money' => 'Win Money', 'Win Bonus' => 'Win Bonus'], request()->award_label, ["class" => "form-control", "placeholder" => "Select Label Award", "id" => "label"]) }}
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <label for="">Customer Name</label>
                                {{ Form::select("customer_id", $customers, request()->customer_id,["class" => "form-control", "placeholder" => "Select Customer Name", "id" => "customer"]) }}
                            </div>

                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <div class="form-group mb-0">
                                    <label class="form-group-float-label font-weight-semibold is-visible">Start Date</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text"><i class="icon-calendar3"></i></span>
                                        </span>
                                        {{Form::text("started_date",isset($_GET['started_date']) ? $_GET['started_date'] : date('Y-m-d 00:00:00',strtotime('first day of this month')),
                                            ["class" => "form-control datetimepicker"])
                                        }}
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <div class="form-group mb-0">
                                    <label class="form-group-float-label font-weight-semibold is-visible">Ended Date</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text"><i class="icon-calendar3"></i></span>
                                        </span>
                                        {{Form::text("ended_date",isset($_GET['ended_date']) ? $_GET['ended_date'] : date('Y-m-d 23:59:59',strtotime('last day of this month')),
                                            ["class" => "form-control datetimepicker"])
                                        }}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-sm btn-success">Search</button>
                        <a href="{{route('scan-summary')}}" class="btn btn-sm btn-danger">Reset</a>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-slate-800">
                            <th>#</th>
                            <th>Scanned By</th>
                            <th>Total Amount</th>
                            <th>Total Tickets</th>
                            {{-- <th>Total Win</th>
                            <th>Total Bonus</th>
                            <th>Total Return</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($awardScans) && $awardScans->count() > 0)
                            @foreach($awardScans as $key => $row)
                            <tr>
                                <td>{{($awardScans->perPage() * ($awardScans->currentPage() - 1)) + $key + 1}}</td>
                                <td>{{ $row->customer ? $row->customer->username : ''}}</td>
                                <td>(R) {{ $row->total_amount }}</td>
                                <td>{{ $row->total_ticket}}</td>
                                {{-- <td></td>
                                <td></td>
                                <td></td> --}}
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9"> No data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if(isset($awardScans) && count($awardScans) > 0)
            <div class="card-footer">
                @if($awardScans->hasPages())
                    <div class="mb-2">
                        {!! $awardScans->appends(Input::except('page'))->render() !!}
                    </div>
                @endif
                <div>
                    Showing {{$awardScans->firstItem()}} to {{$awardScans->lastItem()}}
                    of  {{$awardScans->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>

@endsection

@section('page-script')
<script src="/global_assets/js/demo_pages/form_inputs.js"></script>
<script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>

<script>
    if($('.download-excel').length > 0){
        $('.download-excel').attr('href',$('.download-excel').data('route') + location.search)
    }
</script>

@endsection


