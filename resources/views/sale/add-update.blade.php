@extends('layouts.master')

@section('custom-css')
    <style>
        table.total td{
            font-size: 13px;
        }
        table.total tr:last-child td{
            font-weight: bold;
        }

        /* Chrome, Safari, Edge, Opera */
        input[name="discount"]::-webkit-outer-spin-button,
        input[name="discount"]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[name="discount"][type=number] {
            -moz-appearance:textfield;
        }
    </style>
@endsection

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('sales.index')}}" class="breadcrumb-item">Sales</a>
                <span class="breadcrumb-item active">{{ isset($sale) ? 'Edit' : 'Add' }}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @if(isset($sale))
        {{ Form::model($sale,['route' => ['sales.update', $sale['reference_no']], 'method' => 'PUT', 'id' => 'saleform']) }}
        @else
        {{ Form::open(['route' => 'sales.store', 'method' => 'POST', 'id' => 'saleform']) }}
        @endif
        @csrf
        <div class="card-body">
            @include('includes.success-msg')
            @include('includes.error-msg')
            <div id="error-message"></div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('reference_no')?'text-danger':'' }}">Invoice No</label>
                        <div class="position-relative">
                            @if(isset($sale))
                                {{Form::text("reference_no",isset($sale) ? $sale['reference_no'] : '',
                                    ["class" => "form-control ".($errors->has('reference_no')?'border-danger':''), 'readonly', "id" => "reference_no"])
                                }}
                            @else
                                {{Form::text("reference_no",isset($referenceNumber) ? $referenceNumber : '',
                                    ["class" => "form-control ".($errors->has('reference_no')?'border-danger':''), "id" => "reference_no", 'readonly'])
                                }}
                            @endif
                        </div>

                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('sale_date')?'text-danger':'' }}">Sale Date</label>
                        <div class="position-relative">
                            {{
                                Form::text("sale_date",isset($sale) ? $sale['sale_date'] : date('Y-m-d H:i:s'),
                                    ["class" => "form-control ".($errors->has('sale_date') ? 'border-danger' : ''),"placeholder" => "",'readonly'])
                            }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group select2">
                        <label class="form-group-float-label font-weight-semibold is-visible">Customer <span class="text-danger">*</span></label>
                        {{ Form::select("customer_id", $customers, isset($sale) ? $sale['customer_id'] : null,
                            ["class" => "form-control", "placeholder" => "Select Customer (Buyer)", "id" => "customer",
                            auth()->user()->can('sale-modification') || !isset($sale) ? '' : 'disabled']) }}
                    </div>
                </div>

                @if(!isset($sale) || auth()->user()->can('sale-modification'))
                <div class="col-lg-3">
                    <div class="form-group select2">
                        <label class="form-group-float-label font-weight-semibold is-visible">Book Serial Code<span class="text-danger">*</span></label>
                        <div class="input-group">

                            <input type='text' class='form-control serial-code' name='input_book_serial' id='input_book_serial' />

                            <span class="input-group-prepend">
                            <button class="btn btn-success" type="button" data-url = "{{route('sale.get-product')}}" id="btnAddBookCode"><i class="icon-add"></i> Add</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group select2">
                        <label class="form-group-float-label font-weight-semibold is-visible">Box Serial Code<span class="text-danger">*</span></label>
                        <div class="input-group">

                            <input type='text' class='form-control serial-code' name='input_box_serial' id='input_box_serial' />

                            <span class="input-group-prepend">
                            <button class="btn btn-success" type="button" data-url = "{{route('sale.get-products')}}" id="btnAddBoxCode"><i class="icon-add"></i> Add</button>
                            </span>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="table">
                            <thead>
                                <tr class="bg-slate-800">
                                    <th>#</th>
                                    <th>Game Code</th>
                                    <th>Batch Number</th>
                                    <th>Box Number</th>
                                    <th>Book Number</th>
                                    {{-- <th style="width:100px">Quantity</th> --}}
                                    <th>Book Qty</th>
                                    <th style="min-width:120px;width:120px">Ticket Qty</th>
                                    <th style="min-width:120px;width:120px">Ticket Price (R)</th>
                                    <th style="min-width:100px;width:100px">Total Price (R)</th>
                                    <th style="min-width:100px;width:100px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($sale))
                                    @if(isset($sale['product']))
                                    @foreach($sale['product'] as $index => $item)
                                        <tr class='dynamic-added' data-book_code={{ $item->code }}>
                                            <td> {{ $index + 1 }} </td>
                                            <td>{{ $item->game_code }} – {{ $item->product->name }}</td>
                                            <td>{{ $item->batch_no }}</td>
                                            <td>{{ $item->box_code }}</td>
                                            <td>{{ $item->book_no }} </td>
                                            <td>1</td>
                                            <td>{{ $item->ticket_qty }}</td>
                                            <td>R {{ currencyFormat($item->ticket_price) }}</td>
                                            <td class="item-price" data-item_price={{$item->price}}>R {{ currencyFormat($item->price) }}</td>
                                            <td class="group-btn-action">
                                                <div class="group-btn-action">
                                                    @can('sale-modification')
                                                    <button type="button" class="btn btn-sm btn-outline bg-danger border-danger text-danger-800 btn-icon border-2 remove server"
                                                      data-game_code="{{ $item->game_code}}" data-batch_no="{{ $item->batch_no}}"
                                                      data-box_code="{{ $item->box_code}}" data-book_no={{$item->book_no}}>
                                                        <i class="icon-trash"></i> Remove
                                                    </button>
                                                    @endcan
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    @endif
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="offset-lg-9 col-lg-3 offset-md-6 col-md-6">
                    <div class="card">
                        <div class="card-body p-2">
                            <table class="table total">
                                <tr>
                                    <td class="">Total Price</td>
                                    <td class="text-right" id="total-price" data-total_price="{{isset($sale) ? $sale['total_price'] : 0}}">R {{ isset($sale) ? currencyFormat($sale['total_price']) : 0 }}</td>
                                </tr>
                                <tr>
                                    <td>Discount %</td>
                                    <td class="text-right">
                                        <div class="form-group mb-0 float-right" style="height:30px">
                                            <div class="input-group" style="width:100px">
                                                <input style="height:30px" type="number" class="form-control text-right" name="discount" id="discount" value="{{ isset($sale) ? $sale['discount'] : 0 }}">
                                                <div class="input-group-append" style="height:30px">
                                                    <span class="input-group-text">%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Grand Total</td>
                                    <td class="text-right" id="grand-price">R {{ isset($sale) ? currencyFormat($sale['grand_price']) : 0 }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="action" id="action" value="{{isset($sale) ? 'update' : 'add' }}">
        </div>

        @canany(['sale-modification','add-new-sale'])
        <div class="card-footer">
            <button type="button" class="btn btn-success" id="btn-submit">
                <i class="icon-folder mr-1"></i> {{ isset($sale) ? 'Update' : $settings['language']['LANG_BTN_SAVE'] }}
            </button>
        </div>
        @endcanany

        <input type="hidden" name="action" value="{{isset($sale) ? 'update' : 'add' }}">
        @if(isset($sale))
            <input type="hidden" name="removeBookCode">
        @endif

        {{ Form::close() }}
    </div>
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/global_assets/js/demo_pages/form_checkboxes_radios.js"></script>

<script>

    $('document').ready(function(){

        addBookCode();
        addBoxCode();
        addDiscount();

        $('#btn-submit').click(function(){
            $('#saleform').submit();
        })

        $(document).on("click",".remove",function() {

            if($(this).hasClass('server')){
                var game_code = $(this).data('game_code');
                var batch_no = $(this).data('batch_no');
                var book_no = $(this).data('book_no');
                var box_code = $(this).data('box_code')

                var current_value = $("input[name='removeBookCode']").val()
                var value = current_value ? JSON.parse(current_value) : [];
                value.push({
                    game_code: game_code,
                    batch_no: batch_no,
                    box_code: box_code,
                    book_no: book_no
                });

                $("input[name='removeBookCode']").val(JSON.stringify(value))
            }

            $(this).parents('tr').remove();
            updateGrandPrice();

        });

        function addDiscount(){
            $("#discount").on('keyup', function (e) {
                var grand_total = $('#total-price').data('total_price') - ( $('#total-price').data('total_price') * $("#discount").val() / 100 );
                $('#grand-price').text('R '+ grand_total.toLocaleString('en-US',{ minimumFractionDigits: 2 }))
            });
        }

        function addBookCode(){
            $("input[name='input_book_serial']").focus();
            $("input[name='input_book_serial']").on("keyup", function(event) {
                if (event.keyCode === 13) {
                    event.preventDefault();
                    $('#btnAddBookCode').click();
                }
            });

            $('#btnAddBookCode').click(function(){
                if($('#input_book_serial').val() == ""){
                    showErrorMessage("Oh snap! Please input a valid code"); return;
                }

                if($('#customer').val() == ""){
                    showErrorMessage("Please select a customer"); return;
                }

                var bookCode = $('#input_book_serial').val().slice(0,-3)+'001';

                if($("input[name='book_code[]']").length > 0){
                    var bookCodes = $("input[name='book_code[]']").map(function(){return $(this).val();}).get()

                    if($.inArray(bookCode,bookCodes) != -1){
                        showErrorMessage('Oh snaps! Your book serial code is already sold out.');
                        $('.serial-code').val('')
                        return;
                    }
                }

                var url = $(this).data('url');
                var row;

                $.ajax({
                    type: "GET",
                    url: url,
                    data : { bookCode },
                    dataType: 'json',
                    success: function(response){
                        if(response.success){
                            var data = response.data;
                            row = "<tr class='dynamic-added' data-book_code='"+ bookCode +"'>"+
                                '<td>'+ (parseInt($('#table tbody tr').length) + 1) +'</td>'+
                                '<td>'+ data.game_code + ' – ' + data.product_name + '</td>'+
                                '<td>'+ (data.batch_no? data.batch_no : 'None') + '</td>'+
                                '<td>'+ (data.box_code ? data.box_code : 'None') + '</td>'+
                                '<td>' + data.book_no + '</td>'+
                                '<td>1</td>'+
                                '<td>' + data.ticket_qty  + '</td>'+
                                '<td> R '+ data.ticket_price.toFixed(2) +'</td>'+
                                '<td class="item-price" data-item_price='+data.ticket_price * data.ticket_qty+'> R ' + (data.ticket_price * data.ticket_qty).toFixed(2) + '</td>'+
                                '<td class="group-btn-action">'+
                                    '<div class="group-btn-action">'+
                                        '<button type="button" class="btn btn-sm btn-outline bg-danger border-danger text-danger-800 btn-icon border-2 remove "><i class="icon-trash"></i> Remove</button>'+
                                    '</div> '+
                                    '<input type ="hidden" name= "book_code[]" value="'+bookCode+'">'
                                '</td>'+
                            '</tr>';
                            $('#table').append(row);
                            updateGrandPrice();
                        }else{
                            showErrorMessage('Oh snap! '+response.message)
                        }
                    }

                });

                $(".serial-code").val("");

            });
        }

        function addBoxCode(){
            $("input[name='input_box_serial']").on("keyup", function(event) {
                if (event.keyCode === 13) {
                    event.preventDefault();
                    $('#btnAddBoxCode').click();
                }
            });

            $('#btnAddBoxCode').click(function(){
                if($('#input_box_serial').val() == ""){
                    showErrorMessage("Oh snap! Please input a valid code"); return;
                }

                if($('#customer').val() == ""){
                    showErrorMessage("Please select a customer"); return;
                }

                var boxCode = $('#input_box_serial').val();
                var bookCodes = $("input[name='book_code[]']").map(function(){return $(this).val();}).get()

                var url = $(this).data('url');
                var row;

                $.ajax({
                    type: "GET",
                    url: url,
                    data : { boxCode: boxCode, bookCodes: bookCodes  },
                    dataType: 'json',
                    success: function(response){
                        if(response.success){

                            $.each(response.data, function( index, data ) {
                                row = "<tr class='dynamic-added' data-book_code='"+ data.code +"'>"+
                                    '<td>'+ (parseInt($('#table tbody tr').length) + 1) +'</td>'+
                                    '<td>'+ data.game_code + ' – ' + data.product_name + '</td>'+
                                    '<td>'+ data.batch_no + '</td>'+
                                    '<td>'+ data.box_code + '</td>'+
                                    '<td>' + data.book_no + '</td>'+
                                    '<td>1</td>'+
                                    '<td>' + data.ticket_qty  + '</td>'+
                                    '<td> R '+ data.ticket_price.toFixed(2) +'</td>'+
                                    '<td class="item-price" data-item_price='+data.ticket_price * data.ticket_qty+'> R ' + (data.ticket_price * data.ticket_qty).toFixed(2) + '</td>'+
                                    '<td class="group-btn-action">'+
                                        '<div class="group-btn-action">'+
                                            '<button type="button" class="btn btn-sm btn-outline bg-danger border-danger text-danger-800 btn-icon border-2 remove "><i class="icon-trash"></i> Remove</button>'+
                                        '</div> '+
                                        '<input type ="hidden" name= "book_code[]" value="'+data.code+'">'
                                    '</td>'+
                                '</tr>';
                                $('#table').append(row);
                            });
                            updateGrandPrice();
                        }else{
                            showErrorMessage('Oh snap! '+response.message)
                        }
                    }

                });

                $(".serial-code").val("");

            });
        }

        function updateGrandPrice(){
            var total_price = 0;

            $('.item-price').each(function(){
                total_price += parseInt($(this).data('item_price'));
            })

            $('#total-price').data('total_price',total_price)
            $('#total-price').text('R '+ total_price.toLocaleString('en-US',{ minimumFractionDigits: 2 }));

            var grand_total = total_price - ( total_price * $("#discount").val() / 100 );
            $('#grand-price').text('R '+ grand_total.toLocaleString('en-US',{ minimumFractionDigits: 2 }))
        }

        function showErrorMessage(msg){
            var element = '<div class="alert bg-danger text-white alert-styled-left alert-dismissible" style="display:none;">' +
                            '<button type="button" class="close" data-dismiss="alert"><span>×</span></button>'+
                            '<span class="font-weight-semibold" id="error-message">'+msg+'</span>'+
                            '</div>'

            $('#error-message').html(element);
            $('.alert').css('display', 'block');
            setTimeout(function(){
                $('.alert').css('display', 'none');
            },3000)
        }

    });

</script>
@endsection
