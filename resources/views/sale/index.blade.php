@extends('layouts.master')
<style>
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
    #address{
        width:300px;

    }
    #address ul {

        margin: 0px;
        padding: 0px;
        overflow-wrap: break-word;
        text-decoration: none;
    }
</style>

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Product Sale</span></h4>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                @canany(['export-sale'])
                    <a href="" target="_BLANK" data-route="{{ route('sale.excel.download') }}" class="btn bg-teal-400 btn-sm download-excel mr-2">
                        <i class="icon-download mr-1 text-white"></i> Download Excel
                    </a>
                @endcanany

                @canany(['add-new-sale'])
                <a href="{{ route('sales.create') }}" class="btn btn-primary btn-sm">
                    <i class="icon-file-plus2 text-white"></i> Add New Sale
                </a>
                @endcanany
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Sale</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        <div class="card-header">
            @include('includes.success-msg')
            @include('includes.error-msg')
            <div class="mb-1">
                <button class="btn btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#filter" aria-expanded="false" aria-controls="collapseExample">
                    <i class="icon-filter3"> </i> Filter
                </button>

                <div class="collapse mt-2" id="filter">
                    {{ Form::open(['route' => 'sales.index', 'method' => 'GET']) }}
                        <div class="row mb-2">
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <label for="">Reference No</label>
                                {{Form::text("reference_no", request()->reference_no, ["class" => "form-control ", "placeholder" => "Enter Reference"])}}
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <label for="">Customer Name</label>
                                {{ Form::select("customer_id", $customers, request()->customer_id,["class" => "form-control", "placeholder" => "Select Customer Name", "id" => "customer"]) }}
                            </div>

                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <div class="form-group mb-0">
                                    <label class="form-group-float-label font-weight-semibold is-visible">Start Date</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text"><i class="icon-calendar3"></i></span>
                                        </span>
                                        {{Form::text("started_date",isset($_GET['started_date']) ? $_GET['started_date'] : date('Y-m-d 00:00:00',strtotime('first day of this month')),
                                            ["class" => "form-control datetimepicker"])
                                        }}
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <div class="form-group mb-0">
                                    <label class="form-group-float-label font-weight-semibold is-visible">Ended Date</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text"><i class="icon-calendar3"></i></span>
                                        </span>
                                        {{Form::text("ended_date",isset($_GET['ended_date']) ? $_GET['ended_date'] : date('Y-m-d 23:59:59',strtotime('last day of this month')),
                                            ["class" => "form-control datetimepicker"])
                                        }}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-sm btn-success">Search</button>
                        <a href="{{route('sales.index')}}" class="btn btn-sm btn-danger">Reset</a>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-slate-800">
                            <th>#</th>
                            <th>Ref.No</th>
                            <th>Total Product Qty</th>
                            <th>Grand Price</th>
                            <th>Sold At</th>
                            <th>Customer Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($sales) && $sales->count() > 0)
                            @foreach($sales as $key => $row)
                            <tr>
                                <td>{{($sales->perPage() * ($sales->currentPage() - 1)) + $key + 1}}</td>
                                <td><a href="{{route('sales.edit',$row->reference_no)}}">{{ $row->reference_no }} <i class="icon-link" style="font-size:12px"></i></a></td>
                                <td>{{ $row->total_product_qty }} Book(s)</td>
                                <td>R {{ currencyFormat($row->grand_price) }}</td>
                                <td>{{ $row->created_at }}</td>
                                <td>{{ $row->customer ? $row->customer->username : '' }}</td>
                                <td class="group-btn-action">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-outline bg-success border-success text-success-800 btn-icon border-2 view-invoice"
                                                data-route="{{ route('sale.detail',$row->reference_no) }}"
                                                data-toggle="modal" data-target="#invoice_modal"><i class="icon-eye"></i> View</a></button>

                                        @canany(['sale-modification'])
                                            <a href="{{ route('sales.edit',$row->reference_no) }}" class="btn btn-sm btn-outline bg-primary border-primary text-primary-800 btn-icon border-2 sale-edit" ><i class="icon-pencil7"></i>  {{ $settings['language']['LANG_LABEL_EDIT']}}</a>
                                            <button type="button" class="btn btn-sm btn-outline bg-danger border-danger text-danger-800 btn-icon border-2 delete" data-route="{{ route('sales.destroy', $row->reference_no)}}" id="sale-delete"><i class="icon-trash"></i> Delete</a>
                                        @endcanany
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9"> No data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if(isset($sales) && count($sales) > 0)
            <div class="card-footer">
                @if($sales->hasPages())
                    <div class="mb-2">
                        {!! $sales->appends(Input::except('page'))->render() !!}
                    </div>
                @endif
                <div>
                    Showing {{$sales->firstItem()}} to {{$sales->lastItem()}}
                    of  {{$sales->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>

<div id="invoice_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">INVOICE </h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="header">
                    <img src="./assets/images/logo.png" width="100px" class="img-fluid float-right" alt="Responsive image">
                    <h4 class=" mt-0 mb-1"><b>Sethei Company</b></h4>
                    <div id="address">
                        <ul  style="list-style-type:none;">
                            <li><b>Address:</b> #47 St.217, Phum 9, Sangkat Monorom, Khan 7Makara, Phnom Penh.</li>
                        </ul>
                    </div>
                </div>
                <div class="header">
                    <h4 class=" mt-3 mb-1"><b>Customer</b></h4>
                    <div id="address">
                        <ul  style="list-style-type:none;">
                            <li><b>Province:</b> <span id="customer-province"></span></li>
                            <li><b>Name:</b> <span id="customer-name"></span></li>
                            <li><b>Address:</b> <span id="customer-address"></span></li>
                            <li><b>Phone:</b> <span id="customer-phone"></span></li>
                        </ul>
                    </div>
                </div>
                <h4 class="mt-3 mb-3 text-center font-weight-bold">INVOICE</h4>
                <div class="row mb-1">
                    <div class="col-md-3">Reference No</div>
                    <div class="col-md-6">
                        <div class="font-weight-bold" id="refNo"></div>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-md-3">Sale Date</div>
                    <div class="col-md-6">
                        <div id = "saleDate"></div>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-md-3">Total Qty</div>
                    <div class="col-md-6">
                        <div id = "totalQty"></div>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-md-3">Total Price</div>
                    <div class="col-md-6">
                        <div id = "totalPrice"></div>
                    </div>
                </div>
                <div class="row mb-1">
                    <div class="col-md-3">Discount (%)</div>
                    <div class="col-md-6">
                        <div id = "discount"></div>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-3">Grand Price</div>
                    <div class="col-md-6">
                        <div class="font-weight-bold" id = "grandPrice"></div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered" id="table">
                        <thead>
                            <tr class="bg-slate-800">
                                <th>#</th>
                                <th>Serial Code</th>
                                <th>Game Code</th>
                                <th>Batch</th>
                                <th>Box</th>
                                <th>Book No</th>
                                <th>Book Qty</th>
                                <th>Ticket Qty</th>
                                <th>Ticket Price (R)</th>
                                <th>Total Price (R)</th>
                            </tr>
                        </thead>
                        <tbody id="tableBody">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                @can('export-stock')
                <a href="" data-route="{{ route('sale.pdf.download') }}" class="btn btn-primary generate-pdf" target="_BLANK">Download PDF</a>
                <a href="" data-route="{{ route('sale.excel.download') }}" class="btn btn-success generate-excel" target="_BLANK">Download Excel</a>
                @endcan
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')
<script src="/global_assets/js/demo_pages/form_inputs.js"></script>
<script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>

<script>
    $(document).ready(function(){

        if($('#invoice_modal').length > 0){
            $('.view-invoice').click(function(){
                $.ajax({
                    type:'GET',
                    url:$(this).data('route'),
                    dataType: "JSON",
                    data: {},
                    success:function(response){
                        if(response.success){
                            var data = response.data;
                            var tbody = $('#table tbody');
                            var total_tickets = 0;

                            tbody.html('');

                            $.each(data['detail'], function( index, value ) {
                                var no = index + 1;
                                var row = '<tr>'+
                                            '<td>'+ no +'</td>'+
                                            '<td>'+ value.code +'</td>'+
                                            '<td>'+ value.game_code + '<br>' + value.game_name +'</td>'+
                                            '<td>'+ value.batch_no +'</td>'+
                                            '<td>'+ value.box_code +'</td>'+
                                            '<td>'+ value.book_no +'</td>'+
                                            '<td>1</td>'+
                                            '<td>' + value.ticket_qty  + '</td>'+
                                            '<td>'+ currencyFormat(value.ticket_price) +'</td>'+
                                            '<td>'+ currencyFormat(value.price) +'</td>'
                                        '</tr>'

                                tbody.append(row);
                                total_tickets += parseInt(value.ticket_qty)
                            });


                            $("#saleDate").html(data['sale'].sale_date);
                            $("#refNo").html(data['sale'].reference_no);
                            $("#totalQty").html(data['sale'].total_product_qty.toLocaleString('en-US') + ' books = ' + total_tickets.toLocaleString('en-US') + ' tickets');
                            $("#totalPrice").html(currencyFormat(data['sale'].total_price));
                            $("#discount").html(data['sale'].discount + '%');
                            $("#grandPrice").html(currencyFormat(data['sale'].grand_price));
                            $("#customer-address").html(data['sale']['customer'].address)
                            $("#customer-name").html(data['sale']['customer'].username)
                            $("#customer-province").html(data['sale']['customer'].province)
                            $("#customer-phone").html(data['sale']['customer'].phone)
                            $('#invoice_modal .modal-title').html('Invoice #'+data['sale'].reference_no)

                            var refNo = $('#refNo').text();
                            $('.generate-pdf').attr('href',$('.generate-pdf').data('route') + '?refNo=' + refNo)
                            $('.generate-excel').attr('href',$('.generate-excel').data('route') + '?reference_no=' + refNo)

                        }else{
                            swalOnResult('Error While Loading Information!','error');
                        }
                    },
                });
            });
        }

    });

    if($('.download-excel').length > 0){
        $('.download-excel').attr('href',$('.download-excel').data('route') + location.search)
    }

    function currencyFormat(value){
        return 'R '+ value.toLocaleString('en-US',{ minimumFractionDigits: 2 })
    }

    var swalOnResult = function(title, type){
        swal({
            title: title,
            text: "You clicked the button!",
            icon: type,
            timer: 1500,
        });
    }

</script>

@endsection


