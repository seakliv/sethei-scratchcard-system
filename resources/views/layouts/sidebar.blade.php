
<li class="nav-item">
    <a href="{{route('dashboard')}}" class="nav-link"><i class="icon-home4"></i> <span>Dashboard</span></a>
</li>

<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Scratch Card Data</div> <i class="icon-menu" title="Main"></i></li>

@canany(['view-product'])
<li class="nav-item">
    <a href="{{ route('products.index') }}" class="nav-link" data-active="{{ in_array(Route::currentRouteName(), ['products.create','products.edit']) ? true : false}}">
        <i class="icon-stack-picture"></i> <span>Product</span>
    </a>
</li>
@endcanany

@canany(['view-stock'])
    <li class="nav-item">
        <a href="{{ route('stocks.index') }}" class="nav-link" data-active="{{ in_array(Route::currentRouteName(), ['stocks.create','stocks.edit']) ? true : false}}">
            <i class="icon-home7"></i> <span>Stock In</span>
        </a>
    </li>
@endcanany

@canany(['view-sale'])
    <li class="nav-item">
        <a href="{{ route('sales.index') }}" class="nav-link" data-active="{{ in_array(Route::currentRouteName(), ['sales.create','sales.edit']) ? true : false}}">
            <i class="icon-cash"></i> <span>Sale</span>
        </a>
    </li>
@endcanany

@canany(['view-award-data'])
<li class="nav-item">
    <a href="{{ route('awards.index') }}" class="nav-link"><i class="icon-gift"></i> <span>Award Data</span></a>
</li>
@endcanany

@can('view-scanned-log')
<li class="nav-item">
    <a href="{{ route('scan-award') }}" class="nav-link"><i class="icon-stack-check"></i> <span>Scanned Log</span></a>
</li>
@endcan

<li class="nav-item">
    <a href="{{ route('scan-summary') }}" class="nav-link"><i class="icon-stats-bars"></i> <span>Scanned Summary</span></a>
</li>

<!--<li class="nav-item">
    <a href="{{ route('purchase') }}" class="nav-link"><i class="icon-barcode2"></i> <span>Freelancer Scanned</span></a>
</li>-->

<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">System Setting</div> <i class="icon-menu" title="Main"></i></li>
@canany(['view-manager-account','add-new-manager-account','manager-account-modification','view-role','add-new-role','role-modification','view-manager-log'])
    <li class="nav-item nav-item-submenu
        {{ in_array(Route::currentRouteName(), ['managers.create','managers.edit','managers.roles.create','managers.roles.edit'])
            ? ' nav-item-expanded nav-item-open' : ''}}">
        <a href="#" class="nav-link"><i class="icon-user-check"></i> <span>{{ $settings['language']['LANG_MENU_MANAGER']}}</span></a>

        <ul class="nav nav-group-sub" data-submenu-title="Layouts">
            @canany(['view-manager-account','add-new-manager-account','manager-account-modification'])
            <li class="nav-item"><a href="{{ route('managers.index') }}" class="nav-link {{ in_array(Route::currentRouteName(), ['managers.create','managers.edit']) ? ' active' : ''}}">{{ $settings['language']['LANG_MENU_MANAGER_LIST']}}</a></li>
            @endcanany
            @canany(['add-new-role','view-role','role-modification'])
            <li class="nav-item"><a href="{{ route('managers.roles.index') }}" class="nav-link {{ in_array(Route::currentRouteName(), ['managers.roles.create','managers.roles.edit']) ? ' active' : ''}}">{{ $settings['language']['LANG_MENU_MANAGER_ROLE']}}</a></li>
            @endcanany
        </ul>
    </li>
@endcanany
