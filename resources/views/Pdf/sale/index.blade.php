<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="https://fonts.googleapis.com/css?family=Battambang&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Hanuman&display=swap" rel="stylesheet">


    <style type="text/css" media="all">
        .kh-font {
            font-family: 'Battambang', cursive; line-height: 10px;
            /* font-family: 'Hanuman', serif; */
        }

        #address{
                width:350px;
                text-decoration: none;
        }
        .header {
            width:100%;
        }

        #address ul {

            margin: 0px;
            padding: 0px;
            overflow-wrap: break-word;

        }

        table {

            border-collapse: collapse;
        }

        .summary table, th, td {
            border: 1px solid black;
            padding: 10px 5px;
            text-align: center;
            font-size: 13px
        }

    </style>
</head>
<body>
    <div class="container-fluid summary">
        <div class="col-lg-12">
            <form>
                <div class="header">
                    <img src="./assets/images/logo.png" width="100px" style="float:right;" alt="Responsive image">
                    <h3 style="margin-bottom:0px">Sethei Lottery Company</h3>
                    <div id="address" style="float-right">
                        <ul  style="list-style-type:none;">
                            <li><b>Address:</b> #47 St.217, Phum 9, Sangkat Monorom, Khan 7Makara, Phnom Penh.</li>
                        </ul>
                    </div>
                </div>

                 <div class="header">
                    <h3 style="margin-bottom:5px"><b>Customer</b></h3>
                    <div id="address">
                        <ul  style="list-style-type:none;">
                            <li style="margin-bottom:3px"><b>Province:</b> {{ $sale->customer->province }}</li>
                            <li style="margin-bottom:3px"><b>Name:</b> {{ $sale->customer->username }}</li>
                            <li style="margin-bottom:3px"><b>Address:</b> {{ $sale->customer->address }}</li>
                            <li style="margin-bottom:3px"><b>Phone:</b> {{ $sale->customer->phone }}</li>
                        </ul>
                    </div>
                </div>

                <h4 class="mb-3 text-center font-weight-bold" style="text-align:center; margin-top:20px">SALE INVOICE</h4>

                <div class="row" style="margin-top:7px;">
                    <div >Reference No : <b>{{ isset($sale) ? $sale->reference_no : '' }}</b></div>
                </div>
                <div style="margin-top:5px; margin-bottom:5px; margin-bottom:5px;">
                    <div>Sale Date : {{ isset($sale) ? $sale->sale_date : '' }}</div>
                </div>
                <div class="row" style="margin-top:5px; margin-bottom:5px;">
                    <div>Total Qty : {{ isset($sale) ? $sale->total_product_qty.' book(s) = '. currencyFormat($sale->saleDetails->sum('ticket_qty'),0).' tickets' : '' }}</div>
                </div>
                <div class="row" style="margin-top:5px; margin-bottom:5px;">
                    <div >Total Price : R {{ isset($sale) ? currencyFormat($sale->total_price) : '' }}</div>
                </div>
                <div style="margin-top:5px; margin-bottom:5px;">
                    <div>Discount : {{ isset($sale) ? $sale->discount : '' }} %</div>
                </div>
                <div class="row" style="margin-top:5px; margin-bottom:5px;">
                    <div>Grand Total : <b>R {{ isset($sale) ? currencyFormat($sale->grand_price) : '' }}</b></div>
                </div>
                <table class="table" style="width:100%">
                    <thead>
                        <tr style="background-color:black;color:white">
                            <th>#</th>
                            <th>Serial Code</th>
                            <th>Game</th>
                            <th>Batch</th>
                            <th>Box</th>
                            <th>Book No</th>
                            <th>Book Qty</th>
                            <th>Ticket Qty</th>
                            <th>Ticket Price</th>
                            <th>Total Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($sale->saleDetails) && $sale->saleDetails->count() > 0)
                            @foreach($sale->saleDetails as $key => $row)
                            <tr>
                                <td>{{ $key + 1}}</td>
                                <td>{{ $row->code }}</td>
                                <td>{{ $row->game_code }}</td>
                                <td>{{ $row->batch_no }}</td>
                                <td>{{ $row->box_code }}</td>
                                <td>{{ $row->book_no }}</td>
                                <td>1</td>
                                <td>{{ $row->ticket_qty }}</td>
                                <td>R {{ currencyFormat($row->ticket_price)}}</td>
                                <td>R {{ currencyFormat($row->price) }}</td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="10"> No data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </form>
            <div class="footer" style="margin-top:70px">
                <div class="reciever" style="display:inline-block; width:33%">
                    Prepared by
                    <div style="margin-top:70px; margin-bottom:5px">Admin Department</div>
                    <div>Date: _______________</div>
                </div>
            </div>
        </div>
    </div>


</body>
</html>
