<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="https://fonts.googleapis.com/css?family=Battambang&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Hanuman&display=swap" rel="stylesheet">


    <style type="text/css" media="all">
        .kh-font {
            font-family: 'Battambang', cursive; line-height: 10px;
            /* font-family: 'Hanuman', serif; */
        }

        #address{
                width:300px;
                text-decoration: none;
        }
        .header {
            width:100%;
        }

        #address ul {

            margin: 0px;
            padding: 0px;
            overflow-wrap: break-word;

        }

        table {

            border-collapse: collapse;
        }

        .summary table, th, td {
            border: 1px solid black;
            padding: 5px 3px;
            text-align: center;
            font-size: 13px
        }
        .page-break {
            page-break-after: always;
        }

        .detail table tr td{
            padding: 5px
        }
    </style>
</head>
<body>
    <div class="container-fluid summary" style="page-break-after: auto;">
        <div class="col-lg-12">
            <form>
                <div class="header">
                    <img src="./assets/images/logo.png" width="100px" style="float:right;" alt="Responsive image">
                    <h3 style="margin-top: 0px;">Sethei Lottery Company</h3>
                    <div id="address" style="float-right">
                        <ul  style="list-style-type:none;">
                            <li>Address: #47 St.217, Phum 9, Sangkat Monorom, Khan 7Makara, Phnom Penh.</li>
                        </ul>
                    </div>
                </div>

                <h4 class="mb-3 text-center font-weight-bold" style="text-align:center; margin-top:30px">STOCK IN INVOICE</h4>

                <div class="row" style="margin-top:7px;">
                    <div >Reference No : {{ isset($stockLog) ? $stockLog->reference_no : '' }}</div>

                </div>
                <div style="margin-top:5px; margin-bottom:5px;">
                    <div class="col-lg-3">Stock In Date : {{ isset($stockLog) ? $stockLog->stock_in_date : '' }}</div>
                </div>
                <div style="margin-top:5px; margin-bottom:5px;">
                    <div class="col-lg-3">Total Qty : {{ isset($stockLog) ? currencyFormat($stockLog->stockLogDetails->sum('book_qty'),0). ' books = '. currencyFormat($stockLog->stockLogDetails[0]->product->ticket_qty * $stockLog->stockLogDetails->sum('book_qty'),0) .' tickets' : '' }}</div>
                </div>
                <table class="table" style="width:100%">
                    <thead>
                        <tr style="background-color:black;color:white">
                            <th>#</th>
                            <th>Serial Code</th>
                            <th>Game</th>
                            <th>Batch</th>
                            <th>Box</th>
                            <th>Start Book</th>
                            <th>End Book</th>
                            <th>Qty</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($stockLog->stockLogDetails) && $stockLog->stockLogDetails->count() > 0)
                            @foreach($stockLog->stockLogDetails as $key => $row)
                            <tr>
                                <td>{{ $key + 1}}</td>
                                <td>{{ $row->code }}</td>
                                <td>{{ $row->game_code }}</td>
                                <td>{{ $row->batch_no }}</td>
                                <td>{{ $row->box_code }}</td>
                                <td>{{ $row->start_book_no }}</td>
                                <td>{{ $row->end_book_no }}</td>
                                <td>{{ $row->book_qty }} {{ $row->product->product_unit->name }}</td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9"> No data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </form>
            <div class="footer" style="margin-top:40px">
                <div class="reciever" style="display:inline-block; width:33%">
                    Prepared by
                    <div style="margin-top:70px; margin-bottom:5px">Admin Department</div>
                    <div>Date: _______________</div>
                </div>
                
            </div>
        </div>
    </div>
    <div style="page-break-after: auto;">
    {{-- <div class="page-break"></div>
    <div class="container-fluid detail">
        <div class="col-lg-12">
            <form>
                <div class="header">
                    <img src="./assets/images/logo.png" width="100px" style="float:right;margin-top:30px" alt="Responsive image">
                    <h3>Sethei Lottery Company</h3>
                    <div id="address" style="float-right">
                        <ul  style="list-style-type:none;">
                            <li>Address: #47 St.217, Phum 9, Sangkat Monorom, Khan 7Makara, Phnom Penh.</li>
                        </ul>
                    </div>
                    <h4 class="mt-3 mb-3 text-center font-weight-bold" style="text-align:center;">STOCK IN INVOICE DETAIL</h4>
                </div>
                <div class="row" style="margin-top:10px;">
                    <div >Reference No : {{ isset($stockLog) ? $stockLog->reference_no : '' }}<</div>

                </div>
                <div style="margin-top:10px; margin-bottom:10px;">
                    <div class="col-lg-3">Stock In Date : {{ isset($stockLog) ? $stockLog->stock_in_date : '' }}</div>
                </div>
                <table class="table" style="width:100%">
                    <thead>
                        <tr style="background-color:black;color:white">
                            <th>#</th>
                            <th>Game Code</th>
                            <th>Book Serial Code</th>
                            <th>Book No</th>
                            <th>Ticket Qty</th>
                            <th>Ticket Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($stockLog->stockLogDetails) && $stockLog->stockLogDetails->count() > 0)
                            @foreach($stockLog->stockLogDetails as $i => $log)
                                @foreach($log->productItems as $j => $row)
                                <tr>
                                    <td>{{ $i + $j + 1}}</td>
                                    <td>{{ $log->game_code }}</td>
                                    <td>{{ $row->code }}</td>
                                    <td>{{ $row->book_no }}</td>
                                    <td>{{ $row->ticket_qty }}</td>
                                    <td>R {{ currencyFormat($row->ticket_price) }}</td>
                                </tr>
                                @endforeach
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8"> No data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </form>
        </div>
    </div> --}}
{{-- </body> --}}
<script type="text/php">
    if (isset($pdf)) {
        $text = "{PAGE_NUM}";
        $font = null;
        $size = 12;
        $color = null;
        $word_space = 0.0;  //  default
        $char_space = 0.0;  //  default
        $angle = 0.0;   //  default
        $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
        $x = ($pdf->get_width() - $width + 50) / 2;
        $y = $pdf->get_height() - 35;
        $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
    }
</script>
{{-- </html> --}}
