@extends('layouts.master')

<style>
        #address{
                width:400px;
                text-decoration: none;
        }
        #address ul {
            
            margin: 0px;
            padding: 0px;
            overflow-wrap: break-word;
    }
        }
</style>
@section('content')

<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Stock In Report</span></h4>
        </div>
        @canany(['export-stock'])
            <div class="header-elements d-none">
                <div class="d-flex justify-content-center"> 
                    <a href="" data-route = "{{ route('stock.download') }}" class="btn bg-teal-400 btn-sm download-excel-all">
                        <i class="icon-download mr-1 text-white"></i> Export Excel
                    </a>
                </div>
            </div>
        @endcanany
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Stock in</span>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div class="card">
        <div class="card-header">
            @include('includes.success-msg')
            @include('includes.error-msg')
            <div class="mb-1">
                <button class="btn btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#filter" aria-expanded="false" aria-controls="collapseExample">
                    <i class="icon-filter3"> </i> Filter
                </button> 
                
                <div class="collapse mt-2" id="filter"> 
                    {{ Form::open(['route' => 'stock.report', 'method' => 'GET']) }}
                        <div class="row mb-2">
                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <label for="">Reference No</label>
                                {{Form::text("reference_no",request()->reference_no,["class" => "form-control "])}}
                            </div>

                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <div class="form-group mb-0">
                                    <label class="form-group-float-label font-weight-semibold is-visible">Start Date</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text"><i class="icon-calendar3"></i></span>
                                        </span>
                                        {{Form::text("started_date",isset($_GET['started_date']) ? $_GET['started_date'] : date('Y-m-d 00:00:00',strtotime('first day of this month')),
                                            ["class" => "form-control datetimepicker"])
                                        }}
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <div class="form-group mb-0">
                                    <label class="form-group-float-label font-weight-semibold is-visible">Ended Date</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text"><i class="icon-calendar3"></i></span>
                                        </span>
                                        {{Form::text("ended_date",isset($_GET['ended_date']) ? $_GET['ended_date'] : date('Y-m-d 23:59:59',strtotime('last day of this month')),
                                            ["class" => "form-control datetimepicker"])
                                        }}
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <label for="">Created By</label>
                                <select name="created_by" class="form-control">
                                    <option value="any">Any</option>
                                    @foreach($managers as $manager)
                                        <option value="{{$manager->id}}" {{request()->created_by == $manager->id ? 'selected' : ''}}>{{ $manager->username }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-sm btn-success">Search</button>
                        <a href="{{route('stocks.index')}}" class="btn btn-sm btn-danger">Reset</a>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-slate-800">
                            <th>#</th>
                            <th>Reference No</th>
                            <th>Total Product Qty</th>
                            <th>Total Book Qty</th>
                            <th>Stock In Date</th>
                            <th>Stock In By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($stockLogs) && $stockLogs->count() > 0)
                            @foreach($stockLogs as $key => $row)
                            <tr>
                                <td>{{ ($stockLogs->perPage() * ($stockLogs->currentPage() - 1)) + $key + 1}}</td>
                                <td>{{ $row->reference_no }}</td>
                                <td>{{ $row->total_product_qty }} Item(s)</td>
                                <td>{{ $row->total_book_qty }}</td>
                                <td>{{ $row->stock_in_date }}</td>
                                <td>{{ $row->user ? $row->user->username : 'N/A' }}</td>
                                <td class="group-btn-action">   
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-outline bg-success border-success text-success-800 btn-icon border-2 print-invoice" 
                                        data-route="{{ route('stocklog.detail',$row->reference_no) }}" 
                                        data-toggle="modal" data-target="#invoice_modal"><i class="icon-eye8 mr-1"></i>View</a>
                                    </div>  
                                </td>
                            </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="9"> No data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if(isset($stockLogs) && count($stockLogs) > 0)
            <div class="mt-2">
                @if($stockLogs->hasPages())
                    <div class="mb-2">
                        {!! $stockLogs->appends(Input::except('page'))->render() !!}
                    </div>
                @endif
                <div>
                    Showing {{$stockLogs->firstItem()}} to {{$stockLogs->lastItem()}}
                    of  {{$stockLogs->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>

<div id="invoice_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">INVOICE</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="header">
                    <img src="/assets/images/logo.png" width="100px" class="img-fluid float-right" alt="Responsive image">
                    <h4 class=" mt-3 mb-3">Sethei Company</h4>
                    <div id="address">
                        <ul  style="list-style-type:none;">
                            <li>Address: #47 St.217, Phum 9, Sangkat Monorom, Khan 7Makara, Phnom Penh.</li>
                            <li>Tel: 070 517 436 - 099 581 218 </li>
                            <li>Email: SetheiLottery@gmail.com</li>
                        </ul>
                    </div>
                    <h4 class="mt-3 mb-3 text-center font-weight-bold">INVOICE</h4>
                </div>
                <div class="row mb-1">
                    <div class="col-lg-3">Reference No</div>
                    <div class="col-lg-6">
                        <div id="refNo"></div>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-lg-3">Stock In Date</div>
                    <div class="col-lg-6">
                        <div id = "stockInDate"></div>
                    </div>
                </div>
                <table class="table table-bordered" id="table">
                    <thead>
                        <tr class="bg-slate-800">
                            <th>#</th>
                            <th>Game Code</th>
                            <th>Game Name</th>
                            <th>Batch No</th>
                            <th>Box Code</th>
                            <th>Qty</th>
                            <th>Unit</th>
                        </tr>
                    </thead>
                    <tbody id="tableBody">
                    </tbody>
                </table>
                <div class="form-group row mt-3 float-right">
                    <label class="col-lg-4 col-form-label">Total Qty</label>
                    <div class="col-lg-8">
                        <div class="position-relative">
                            {{
                                Form::text("reference_no",isset($refNo) ? $refNo : (isset($stock) ? $stock['reference_no'] : ''),
                                    ["class" => "form-control total".($errors->has('reference_no') ? 'border-danger' : ''),"placeholder" => "",'readonly'])
                            }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-Success print-window">Print</button>
                <a href="" data-route="{{ route('pdfs.index') }}" class="btn btn-primary generate-pdf" target="_BLANK">Download PDF</a>
                @canany(['export-stock'])
                    <a href="" data-route="{{ route('stock.download') }}" class="btn bg-teal-400 download-excel">Download Excel</a>
                @endcanany
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')

<script src="/global_assets/js/demo_pages/form_inputs.js"></script>
<script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>

<script>

    $(document).ready(function(){

        $(window).on('load', function(){

             $('.download-excel-all').attr('href',$('.download-excel-all').data('route')+location.search);
            
        });
        
        if($('#invoice_modal').length > 0){

            $('.print-invoice').click(function(){
                // $('#body').html("<div class='text-center'><i class='icon-spinner2 spinner'></i></div>")
                $.ajax({
                    type:'GET',  
                    url:$(this).data('route'),
                    dataType: "JSON",
                    data: {},
                    success:function(response){
                        if(response.success){
                            var data = response.data;
                            var tbody = $('#table tbody');

                            $.each(data, function( index, value ) {
                                var no = index + 1;
                                var row = '<tr>'+
                                            '<td>'+ no +'</td>'+
                                            '<td>'+ value.game_code +'</td>'+
                                            '<td>'+ value.game_name +'</td>'+
                                            '<td>'+ value.batch_no +'</td>'+
                                            '<td>'+ value.box_code +'</td>'+
                                            '<td>'+ value.qty +'</td>'+
                                            '<td>'+ value.unit +'</td>'+
                                        '</tr>'

                            tbody.append(row);
                            });

                            $("#stockInDate").html(data[0].stock_in_date);
                            $("#refNo").html(data[0].reference_no);
                            $(".total").val(data[0].total);

                            var refNo = $('#refNo').text();
                            $('.generate-pdf').attr('href',$('.generate-pdf').data('route') + '?refNo=' + refNo);

                            $('.download-excel').attr('href',$('.download-excel').data('route') + '?refNo=' + refNo);
                    

                        }else{
                            swalOnResult('Error While Loading Information!','error');
                        }
                    },
                    error:function(){
                    swalOnResult('Error While Loading Information!','error');

                    }
                }); 
            });

        }

        $(".modal").on("hidden.bs.modal", function(){
            $("#table tbody").html("");
            $('#invoice_modal').removeClass("printable");
        });

        $('.print-window').click(function() {  
            $('#invoice_modal').addClass("printable");
             window.print();
             $('#invoice_modal').modal('hide');
          });
         
    });

        var swalOnResult = function(title, type){
            swal({
                title: title,
                text: "You clicked the button!",
                icon: type,
                timer: 1500,
            });
        }
       

</script>


@endsection