@extends('layouts.master')
<style>
    #address{
            width:400px;
            text-decoration: none;
    }
    #address ul {
        
        margin: 0px;
        padding: 0px;
        overflow-wrap: break-word;
}
    }
</style>

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Customer Report</span></h4>
        </div>
        @canany(['export-customer'])
            <div class="header-elements d-none">
                <div class="d-flex justify-content-center"> 
                    <a href="" data-route = {{ route('customer.download') }} class="btn bg-teal-400 btn-sm download-excel-all">
                        <i class="icon-download mr-1 text-white"></i> Export Excel
                    </a>
                </div>
            </div>
        @endcanany
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Customer Report</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        <div class="card-header">
            @include('includes.success-msg')
            @include('includes.error-msg')
            <div class="mb-1">
                <button class="btn btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#filter" aria-expanded="false" aria-controls="collapseExample">
                    <i class="icon-filter3"> </i> Filter
                </button> 
                
                <div class="collapse mt-2" id="filter"> 
                    {{ Form::open(['route' => 'customer.report', 'method' => 'GET']) }}
                        <div class="row mb-2">
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <label for="">Customer Code</label>
                                {{Form::text("code",request()->code,["class" => "form-control ", "placeholder" => "Enter Code"])}}
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <label for="">Username</label>
                                {{Form::text("username",request()->username, ["class" => "form-control ", "placeholder" => "Enter username"])}}
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <label for="">Province</label>
                                {{ Form::select("province_id", $provinces,request()->province_id,["class" => "form-control", "placeholder" => "Select Province", "id" => "province"]) }}
                                {{-- {{Form::text("province_id",request()->province_id, ["class" => "form-control "])}} --}}
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <label for="">User Status</label>
                                <select name="state" class="form-control">
                                    <option value="" >Any</option>
                                    <option value="1" {{ request()->state == '1' ? 'selected' : ''}}>Active</option>
                                    <option value="0" {{ request()->state == '0' ? 'selected' : ''}}>Deactive</option>
                                </select>
                            </div>
                        </div>
        
                        <button type="submit" class="btn btn-sm btn-success">Search</button>
                        <a href="{{route('customers.index')}}" class="btn btn-sm btn-danger">Reset</a>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-slate-800">
                            <th>#</th>
                            <th>Code</th>
                            <th>Username</th>
                            <th>Phone</th>
                            <th>Province</th>
                            <th>District</th>
                            <th>State</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($customers) && $customers->count() > 0)
                            @foreach($customers as $key => $row)
                            <tr>
                                <td>{{($customers->perPage() * ($customers->currentPage() - 1)) + $key + 1}}</td>
                                <td>{{ $row->code }}</td>
                                <td>{{ $row->username }}</td>
                                <td>{{ $row->phone }}</td>
                                <td>{{ $row->province ? $row->province->name : '' }}</td>
                                <td>{{ $row->district ? $row->district->name : '' }}</td>
                                
                                <td>
                                    @if($row->state == 1)
                                        <span class="badge badge-success">Active</span>
                                    @else 
                                        <span class="badge badge-danger">Deactive</span>
                                    @endif
                                </td>
                                <td class="group-btn-action">   
                                    {{-- <div class="btn-group">
                                       <a href="{{ route('customer.download','id='.$row->id) }}" class="btn bg-teal-400">Download</a>
                                    </div>   --}}
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-outline bg-success border-success text-success-800 btn-icon border-2 print-invoice" 
                                        data-route="{{ route('customer.detail',$row->id) }}" 
                                        data-toggle="modal" data-target="#invoice_modal"><i class="icon-eye8 mr-1"></i>View</a>
                                    </div>  
                                </td>
                            </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="9"> No data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if(isset($customers) && count($customers) > 0)
            <div class="card-footer">
                @if($customers->hasPages())
                    <div class="mb-2">
                        {!! $customers->appends(Input::except('page'))->render() !!}
                    </div>
                @endif
                <div>
                    Showing {{$customers->firstItem()}} to {{$customers->lastItem()}}
                    of  {{$customers->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>

<div id="invoice_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">Report Details</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="header">
                <h4 class="mt-3 text-center font-weight-bold">Customer Buy Detail</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered responsive" id="table">
                    <thead>
                        <tr class="bg-slate-800">
                            <th>#</th>
                            <th>Ref.No</th>
                            <th>Game Code</th>
                            <th>Game Name</th>
                            <th>Batch No</th>
                            <th>Box No</th>
                            <th>Book Code</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody id="tableBody">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                @canany(['export-customer'])
                    <a href="" data-route="{{ route('customer.download') }}" class="btn btn-primary download-excel" >Download Excel</a>
                @endcanany
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')

<script src="/global_assets/js/demo_pages/form_inputs.js"></script>
<script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>

<script>
    $(document).ready(function(){
        if($('#invoice_modal').length > 0){
              $('.print-invoice').click(function(){
                  $.ajax({
                      type:'GET',  
                      url:$(this).data('route'),
                      dataType: "JSON",
                      data: {},
                      success:function(response){
                          if(response.success){
                              var data = response.data;
                              var tbody = $('#table tbody');
  
                              $.each(data, function( index, value ) {
                                    var no = index + 1;
                                    var row = '<tr>'+
                                                '<td>'+ no +'</td>'+
                                                '<td>'+ value.reference_no +'</td>'+
                                                '<td>'+ value.game_code +'</td>'+
                                                '<td>'+ value.game_name +'</td>'+
                                                '<td>'+ value.batch_no +'</td>'+                                             
                                                '<td>'+ value.box_code +'</td>'+
                                                '<td>'+ value.book_no +'</td>'+   
                                                '<td>'+ value.price +'</td>'+     
                                            '</tr>'
                                    tbody.append(row);      
                                    $('.download-excel').attr('href',$('.download-excel').data('route') + '?id=' + value.id);
                              });
  
                          }else{
                              swalOnResult('Error While Loading Information!','error');
                          }
                      },
                      error:function(){
                         swalOnResult('Error While Loading Information!','error');

                      }
                  }); 
              });
          }

        $(window).on('load', function(){
           $('.download-excel-all').attr('href',$('.download-excel-all').data('route')+location.search);
        });

      $(".modal").on("hidden.bs.modal", function(){
          $("#table tbody").html("");
          $('#invoice_modal').removeClass("printable");
      });             
    
    });  

    var swalOnResult = function(title, type){
        swal({
            title: title,
            text: "You clicked the button!",
            icon: type,
            timer: 1500,
        });
    }
     

</script>
@endsection
