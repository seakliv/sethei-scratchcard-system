@extends('layouts.master')
@php $auth = auth()->user(); @endphp
@section('content')
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex py-2">
                <h4><span class="font-weight-semibold">{{ isset($user) ? 'Edit' : 'Add' }} {{getUserLevelTitle(request()->level + 1)}}</span></h4>
            </div>

            <div class="header-elements d-none">
                <div class="d-flex justify-content-center">
                    <a href="{{route('users.sale-network.index')}}" class="btn btn-primary btn-sm">
                        <i class="icon-square-left mr-1"></i> Back
                    </a>
                </div>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                    <a href="{{route('users.sale-network.index')}}" class="breadcrumb-item">User</a>
                    <span class="breadcrumb-item active">{{ isset($user) ? 'Edit' : 'Add' }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="card">
            @if(isset($user))
            {{ Form::model($user,['route' => ['users.sale-network.update',$user->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) }}
            @else
            {{ Form::open(['route' => 'users.sale-network.store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
            @endif
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        @if(!isset($user))
                        <div class="form-group" style="margin-bottom: 34px;">
                            <label class="d-block font-weight-semibold mb-2">{{ $settings['language']['LANG_LABEL_USERNAME'] }}</label>
                            <div class="custom-control custom-radio custom-control-inline">
                                {{ Form::radio('user_type', '1', null,['class' => 'custom-control-input','id' => 'custom_radio_inline_player']) }}
                                <label class="custom-control-label" for="custom_radio_inline_player">{{ $settings['language']['LANG_LABEL_PLAYER'] }}</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                {{ Form::radio('user_type', '2', true,['class' => 'custom-control-input','id' => 'custom_radio_inline_agent']) }}
                                <label class="custom-control-label" for="custom_radio_inline_agent">{{ $settings['language']['LANG_LABEL_L8_TITLE'] }}</label>
                            </div>
                        </div>
                        @endif
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('username')?'text-danger':'' }}">{{ $settings['language']['LANG_LABEL_USERNAME'] }}</label>
                            <div class="position-relative">
                                {{Form::text("username",isset($user) ? $user->username : '',
                                    ["class" => "form-control ".($errors->has('username')?'border-danger':''),"placeholder" => "Enter user name"])
                                }}
                                @if($errors->has('username'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                                {{ Form::hidden('parent_id', isset($parent_id) ? $parent_id :'') }}
                                {{ Form::hidden('level', isset($level) ? $level : '') }}
                            </div>
                            @if($errors->has('username'))
                                <span class="form-text text-danger">{{ $errors->first('username') }}</span>
                            @endif
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('account_number')?'text-danger':'' }}">{{ $settings['language']['LANG_LABEL_ACCOUNT_NUM'] }}</label>
                            <div class="position-relative">
                                {{Form::text("account_number",isset($user) ? $user->account_number : '',
                                    ["class" => "form-control ".($errors->has('account_number')?'border-danger':''),"placeholder" => "Enter account number"])
                                }}
                                @if($errors->has('account_number'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                            </div>
                            @if($errors->has('account_number'))
                                <span class="form-text text-danger">{{ $errors->first('account_number') }}</span>
                            @endif
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible"> Real Name </label>
                            <div class="position-relative">
                                {{Form::text("real_name",isset($user) ? $user->real_name : '',
                                    ["class" => "form-control","placeholder" => "Enter Real Name"])
                                }}
                            </div>
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible">Email</label>
                            <div class="position-relative">
                                {{Form::email("email",isset($user) ? $user->email : '',
                                    ["class" => "form-control","placeholder" => "Enter email"])
                                }}
                            </div>
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_PHONE'] }}</label>
                            <div class="position-relative">
                                {{Form::text("phone",isset($user) ? $user->phone : '',
                                    ["class" => "form-control","placeholder" => "Enter phone"])
                                }}
                            </div>
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_ADRESS'] }}</label>
                            <div class="position-relative">
                                {{Form::text("address",isset($user) ? $user->address : '',
                                    ["class" => "form-control","placeholder" => "Enter address"])
                                }}
                            </div>
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_PROVINCE'] }} </label>
                            <div class="position-relative">
                                {{Form::text("province",isset($user) ? $user->province : '',
                                    ["class" => "form-control","placeholder" => "Enter province"])
                                }}
                            </div>
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_DISTRICT'] }}</label>
                            <div class="position-relative">
                                {{Form::text("district",isset($user) ? $user->district : '',
                                    ["class" => "form-control","placeholder" => "Enter district"])
                                }}
                            </div>
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_CONTACT_NO'] }}</label>
                            <div class="position-relative">
                                {{Form::text("contact_number",isset($user) ? $user->contact_number : '',
                                    ["class" => "form-control","placeholder" => "Enter contact number"])
                                }}
                            </div>
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_SALE_NAME'] }}</label>
                            <div class="position-relative">
                                {{Form::text("sale_supervisor",isset($user) ? $user->sale_supervisor : '',
                                    ["class" => "form-control","placeholder" => "Enter sale supervisor name"])
                                }}
                            </div>
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_SALE_PHONE'] }}</label>
                            <div class="position-relative">
                                {{Form::text("sale_area_manager",isset($user) ? $user->sale_area_manager : '',
                                    ["class" => "form-control","placeholder" => "Enter sale area manager name"])
                                }}
                            </div>
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_SALE_MANAGER'] }}</label>
                            <div class="position-relative">
                                {{Form::text("sale_director",isset($user) ? $user->sale_director : '',
                                    ["class" => "form-control","placeholder" => "Enter sale director name"])
                                }}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('password')?'text-danger':'' }}">Login Password</label>
                            <div class="position-relative">
                                {{Form::password("password",
                                    ["class" => "form-control ".($errors->has('password')?'border-danger':''),"placeholder" => "Enter more than 6 characters",'autocomplete' => 'off'])
                                }}
                                @if($errors->has('password'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                            </div>
                            @if($errors->has('password'))
                                <span class="form-text text-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('password_confirmation ')?'text-danger':'' }}">Confirm Login Password</label>
                            <div class="position-relative">
                                {{Form::password("password_confirmation",
                                    ["class" => "form-control ".($errors->has('password_confirmation')?'border-danger':''),"placeholder" => "Enter more than 6 characters", 'autocomplete' => 'off'])
                                }}
                                @if($errors->has('password_confirmation'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                            </div>
                            @if($errors->has('password_confirmation'))
                                <span class="form-text text-danger">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('pay_pass')?'text-danger':'' }}">{{ $settings['language']['LANG_LABEL_PAYPASS'] }}</label>
                            <div class="position-relative">
                                {{Form::password("pay_pass",
                                    ["class" => "form-control ".($errors->has('pay_pass')?'border-danger':''),"placeholder" => "Enter more than 6 characters",'autocomplete' => 'off'])
                                }}
                                @if($errors->has('pay_pass'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                            </div>
                            @if($errors->has('pay_pass'))
                                <span class="form-text text-danger">{{ $errors->first('pay_pass') }}</span>
                            @endif
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('pay_pass_confirmation ')?'text-danger':'' }}">Confirm Payment Password</label>
                            <div class="position-relative">
                                {{Form::password("pay_pass_confirmation",
                                    ["class" => "form-control ".($errors->has('pay_pass_confirmation')?'border-danger':''),"placeholder" => "Enter more than 6 characters", 'autocomplete' => 'off'
                                    ])
                                }}
                                @if($errors->has('pay_pass_confirmation'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                            </div>
                            @if($errors->has('pay_pass_confirmation'))
                                <span class="form-text text-danger">{{ $errors->first('pay_pass_confirmation') }}</span>
                            @endif
                        </div>
    
                        {{-- <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                            <label class="form-group-float-label font-weight-semibold is-visible">ID Card Image</label>
                            <div class="position-relative">
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input {{$errors->has('id_card_image') ? 'is-invalid' : '' }}" name="id_card_image" accept="image/*">
                                        <label class="custom-file-label" for="customFile">Please Upload ID Card Image</label>
                                    </div>
                                </div>
                            </div>
                            @if(isset($user))
                                @php $image = $user->getFirstMedia('id_card'); @endphp
                                @if($image)
                                    <img class="w-100" src="{{$image->getUrl()}}" alt="">
                                @endif
                            @endif
                        </div> --}}
    
                        <div class="form-group mb-3 mb-md-2">
                            <label class="d-block font-weight-semibold">State</label>
                            <div class="form-check custom-control-inline">
                                <label class="form-check-label">
                                    <input type="radio" name="state" value="1" class="form-check-input-styled-success" data-fouc checked
                                    {{-- {{ (isset($user) && $user->state == 1) ? 'checked' : '' }} 
                                    {{$auth->can('add-new-user-account') ? 'checked' : ''}} 
                                    {{ !isset($user) || $auth->can('update-user-credentials-info') ? "" : 'disabled'}} --}}
                                    >
                                    Enable
                                </label>
                            </div>
                            
                            <div class="form-check custom-control-inline">
                                <label class="form-check-label">
                                    <input type="radio" name="state" value="0" class="form-check-input-styled-danger" data-fouc 
                                        {{-- {{ (isset($user) && $user->state == 0) ? 'checked' : '' }}  --}}
                                        {{-- {{ !isset($user) || $auth->can('update-user-credentials-info') ? "" : 'disabled' }} --}}
                                         >
                                        Disable
                                </label>
                            </div>
                        </div>
                        <div class="form-group form-group-float form-group-feedback form-group-feedback-right mb-0 h5">
                            <div>
                                <label class="form-group-float-label font-weight-semibold is-visible mr-1 mr-md-3">{{ $settings['language']['LANG_LABEL_REG_DATE'] }}:</label>
                                <label>{{ isset($user) ? $user->created_at : '' }}</label>
                            </div>
                            <div>
                                <label class="form-group-float-label font-weight-semibold is-visible mr-1 mr-md-3">Last Login Date:</label>
                                <label>{{ isset($user) && $user->last_login !=0 ? date('Y-m-d H:i:s',$user->last_login) : '' }}</label>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            {{-- @canany(['user-account-modification']) --}}
            <div class="card-footer">
                <button type="submit" class="btn btn-success">
                    <i class="icon-folder mr-1"></i> {{ $auth->can('add-new-user-account') ? isset($user) && $user->is_approved == 1 ? $settings['language']['LANG_BTN_SAVE'] : 'Approve' : 'Submit' }}
                </button>
            </div>
            {{-- @endcanany --}}
            {{ Form::close() }}
        </div>
    </div>
@endsection

@section('page-script')
    <script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script src="/global_assets/js/demo_pages/form_checkboxes_radios.js"></script>
@endsection