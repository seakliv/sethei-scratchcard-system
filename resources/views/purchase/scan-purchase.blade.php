@extends('layouts.master')
<style>
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
    #address{
                width:300px;

        }
    #address ul {

        margin: 0px;
        padding: 0px;
        overflow-wrap: break-word;
        text-decoration: none;
    }
</style>

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Freelancer Scanned Book</span></h4>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                {{-- @if(request()->award_label === 'Return Bonus' && auth()->id() == 44)
                <a href="{{ route('transfer-bonus') }}" class="btn btn-primary btn-sm">
                    <i class="icon-file-plus2 text-white"></i> Transfer Bonus
                </a>
                @endif --}}

                <a href="" data-route="{{ route('download-scan-purchase') }}" target="_BLANK" class="btn bg-teal-400 download-excel btn-sm mr-2">
                    <i class="icon-download mr-1 text-white"></i> Download Excel
                </a>

            </div>
        </div>

    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Freelancer-Scanned-Book</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        <div class="card-header">
            @include('includes.success-msg')
            @include('includes.error-msg')
            <div class="mb-1">
                <button class="btn btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#filter" aria-expanded="false" aria-controls="collapseExample">
                    <i class="icon-filter3"> </i> Filter
                </button>

                <div class="collapse mt-2" id="filter">
                    {{ Form::open(['route' => 'purchase', 'method' => 'GET']) }}
                        <div class="row mb-2">
                        
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <label for="">Freelancer Name</label>
                                {{ Form::select("user_id", $users, request()->user_id,["class" => "form-control", "placeholder" => "Select Freelancer Name", "id" => "user"]) }}
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-6">
                                <label for="">Game code</label>
                                {{Form::text("game_code", request()->game_code, ["class" => "form-control ", "placeholder" => "Enter Game Code"])}}
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-6">
                                <label for="">Batch No</label>
                                {{Form::text("batch_no", request()->batch_no, ["class" => "form-control ", "placeholder" => "Enter Batch No"])}}
                            </div>

                            <div class="col-lg-2 col-md-3 col-sm-6">
                                <label for="">Book No</label>
                                {{Form::text("book_no", request()->book_no, ["class" => "form-control ", "placeholder" => "Enter Book No"])}}
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <label for="">Serial No</label>
                                {{Form::text("serial_no", request()->serial_no, ["class" => "form-control ", "placeholder" => "Enter Serial No"])}}
                            </div>

                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <div class="form-group mb-0">
                                    <label class="form-group-float-label font-weight-semibold is-visible">Start Date</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text"><i class="icon-calendar3"></i></span>
                                        </span>
                                        {{Form::text("started_date",isset($_GET['started_date']) ? $_GET['started_date'] : date('Y-m-d 00:00:00',strtotime('first day of this month')),
                                            ["class" => "form-control datetimepicker"])
                                        }}
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <div class="form-group mb-0">
                                    <label class="form-group-float-label font-weight-semibold is-visible">Ended Date</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text"><i class="icon-calendar3"></i></span>
                                        </span>
                                        {{Form::text("ended_date",isset($_GET['ended_date']) ? $_GET['ended_date'] : date('Y-m-d 23:59:59',strtotime('last day of this month')),
                                            ["class" => "form-control datetimepicker"])
                                        }}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-sm btn-success">Search</button>
                        <a href="{{route('purchase')}}" class="btn btn-sm btn-danger">Reset</a>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-slate-800">
                            <th>#</th>
                            <th>Scanned By</th>
                            <th>Game Code</th>
                            <th>Batch No</th>
                            <th>Book No</th>
                            <th>Serial No</th>
                            <th>Depo Name</th>
                            <th>Scanned At</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($purchases) && $purchases->count() > 0)
                            @foreach($purchases as $key => $row)
                            <tr>
                                <td>{{($purchases->perPage() * ($purchases->currentPage() - 1)) + $key + 1}}</td>
                                <td>{{ $row->user ? $row->user->username : ''}}</td>
                                <td>{{ $row->game_code }}</td>
                                <td>{{ $row->batch_no}}</td>
                                <td>{{ $row->book_no}}</td>
                                <td>{{ $row->serial_no}}</td>
                                <td>{{ $row->depo_name}}</td>
                                <td>{{ $row->created_at}}</td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7"> No data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if(isset($purchases) && count($purchases) > 0)
            <div class="card-footer">
                @if($purchases->hasPages())
                    <div class="mb-2">
                        {!! $purchases->appends(Input::except('page'))->render() !!}
                    </div>
                @endif
                <div>
                    Showing {{$purchases->firstItem()}} to {{$purchases->lastItem()}}
                    of  {{$purchases->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>

@endsection

@section('page-script')
<script src="/global_assets/js/demo_pages/form_inputs.js"></script>
<script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>

<script>
    if($('.download-excel').length > 0){
        $('.download-excel').attr('href',$('.download-excel').data('route') + location.search)
    }
</script>

@endsection


