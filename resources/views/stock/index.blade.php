@extends('layouts.master')

<style>
    #address{
            width:300px;
            text-decoration: none;
    }
    #address ul {

        margin: 0px;
        padding: 0px;
        overflow-wrap: break-word;
}

</style>
@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Stock In Data</span></h4>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                @canany(['export-stock'])
                    <a href="" target="_BLANK" data-route="{{ route('stock.excel.download') }}" class="btn bg-teal-400 btn-sm download-excel mr-2">
                        <i class="icon-download mr-1 text-white"></i> Download Excel
                    </a>
                @endcanany
                @canany(['add-new-stock'])
                <a href="{{ route('stocks.create') }}" class="btn btn-primary btn-sm">
                    <i class="icon-home7 text-white"></i> Add Stock
                </a>
                @endcanany
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Stock in</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        <div class="card-header">
            @include('includes.success-msg')
            @include('includes.error-msg')
            <div class="mb-1">
                <button class="btn btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#filter" aria-expanded="false" aria-controls="collapseExample">
                    <i class="icon-filter3"> </i> Filter
                </button>

                <div class="collapse mt-2" id="filter">
                    {{ Form::open(['route' => 'stocks.index', 'method' => 'GET']) }}
                        <div class="row mb-2">
                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <label for="">Reference No</label>
                                {{Form::text("reference_no",request()->reference_no,["class" => "form-control "])}}
                            </div>

                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <div class="form-group mb-0">
                                    <label class="form-group-float-label font-weight-semibold is-visible">Start Date</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text"><i class="icon-calendar3"></i></span>
                                        </span>
                                        {{Form::text("started_date",isset($_GET['started_date']) ? $_GET['started_date'] : date('Y-m-d 00:00:00',strtotime('first day of this month')),
                                            ["class" => "form-control datetimepicker"])
                                        }}
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <div class="form-group mb-0">
                                    <label class="form-group-float-label font-weight-semibold is-visible">Ended Date</label>
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text"><i class="icon-calendar3"></i></span>
                                        </span>
                                        {{Form::text("ended_date",isset($_GET['ended_date']) ? $_GET['ended_date'] : date('Y-m-d 23:59:59',strtotime('last day of this month')),
                                            ["class" => "form-control datetimepicker"])
                                        }}
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <label for="">Created By</label>
                                <select name="created_by" class="form-control">
                                    <option value="any">Any</option>
                                    @foreach($managers as $manager)
                                        <option value="{{$manager->id}}" {{request()->created_by == $manager->id ? 'selected' : ''}}>{{ $manager->username }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-sm btn-success">Search</button>
                        <a href="{{route('stocks.index')}}" class="btn btn-sm btn-danger">Reset</a>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-slate-800">
                            <th>#</th>
                            <th>Reference No</th>
                            <th>Total Product Qty</th>
                            <th>Total Book Qty</th>
                            <th>Stock In Date</th>
                            <th>Stock In By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($stockLogs) && $stockLogs->count() > 0)
                            @foreach($stockLogs as $key => $row)
                            <tr>
                                <td>{{ ($stockLogs->perPage() * ($stockLogs->currentPage() - 1)) + $key + 1}}</td>
                                <td><a href="{{route('stocks.edit',$row->reference_no)}}">{{ $row->reference_no }} <i class="icon-link" style="font-size:12px"></i></a></td>
                                <td>{{ $row->total_product_qty }} Item(s)</td>
                                <td>{{ $row->total_book_qty }}</td>
                                <td>{{ $row->stock_in_date }}</td>
                                <td>{{ $row->user ? $row->user->username : 'N/A' }}</td>
                                <td class="group-btn-action">
                                    <div class="btn-group">

                                        <button type="button" class="btn btn-sm btn-outline bg-success border-success text-success-800 btn-icon border-2 view-invoice"
                                                data-route="{{ route('stocklog.detail',$row->reference_no) }}"
                                                data-toggle="modal" data-target="#invoice_modal"><i class="icon-eye"></i> View</a></button>

                                        @canany(['stock-modification'])
                                            <a href="{{route('stocks.edit',$row->reference_no)}}" class="btn btn-sm btn-outline bg-primary border-primary text-primary-800 btn-icon border-2"><i class="icon-pencil7"></i>  {{ $settings['language']['LANG_LABEL_EDIT']}}</a>
                                            <button type="button" class="btn btn-sm btn-outline bg-danger border-danger text-danger-800 btn-icon border-2 delete" data-route="{{route('stocks.destroy',$row->reference_no)}}"><i class="icon-trash"></i> Delete</a>
                                        @endcanany
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9"> No data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if(isset($stockLogs) && count($stockLogs) > 0)
            <div class="mt-2">
                @if($stockLogs->hasPages())
                    <div class="mb-2">
                        {!! $stockLogs->appends(Input::except('page'))->render() !!}
                    </div>
                @endif
                <div>
                    Showing {{$stockLogs->firstItem()}} to {{$stockLogs->lastItem()}}
                    of  {{$stockLogs->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
<div id="invoice_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">INVOICE </h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="header">
                    <img src="./assets/images/logo.png" width="100px" class="img-fluid float-right" alt="Responsive image">
                    <h4 class=" mt-3 mb-3">Sethei Company</h4>
                    <div id="address">
                        <ul  style="list-style-type:none;">
                            <li>Address: #47 St.217, Phum 9, Sangkat Monorom, Khan 7Makara, Phnom Penh.</li>
                        </ul>
                    </div>
                    <h4 class="mt-3 mb-3 text-center font-weight-bold">INVOICE</h4>
                </div>
                <div class="row mb-1">
                    <div class="col-lg-3">Reference No</div>
                    <div class="col-lg-6">
                        <div id="refNo"></div>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-lg-3">Stock In Date</div>
                    <div class="col-lg-6">
                        <div id = "stockInDate"></div>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-lg-3">Total Qty</div>
                    <div class="col-lg-6">
                        <div id = "totalQty"></div>
                    </div>
                </div>
                <table class="table table-bordered" id="table">
                    <thead>
                        <tr class="bg-slate-800">
                            <th>#</th>
                            <th>Box Serial Code</th>
                            <th>Game Code</th>
                            <th>Batch No</th>
                            <th>Box Code</th>
                            <th>Start Book No</th>
                            <th>End Book No</th>
                            <th>Qty</th>
                            <th>Unit</th>
                        </tr>
                    </thead>
                    <tbody id="tableBody">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                @can('export-stock')
                <a href="" data-route="{{ route('stock.pdf.download') }}" class="btn btn-primary generate-pdf" target="_BLANK">Download PDF</a>
                <a href="" data-route="{{ route('stock.excel.download') }}" class="btn btn-success generate-excel" target="_BLANK">Download Excel</a>
                @endcan
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/demo_pages/form_inputs.js"></script>
<script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>

<script>
    $(document).ready(function(){

        if($('#invoice_modal').length > 0){
            $('.view-invoice').click(function(){
                $.ajax({
                    type:'GET',
                    url:$(this).data('route'),
                    dataType: "JSON",
                    data: {},
                    success:function(response){
                        if(response.success){
                            var data = response.data;
                            var tbody = $('#table tbody');
                            tbody.html('')
                            $.each(data, function( index, value ) {
                                var no = index + 1;
                                var row = '<tr>'+
                                            '<td>'+ no +'</td>'+
                                            '<td>'+ value.code +'</td>'+
                                            '<td>'+ value.game_code + '–' + value.game_name +'</td>'+
                                            '<td>'+ value.batch_no +'</td>'+
                                            '<td>'+ value.box_code +'</td>'+
                                            '<td>'+ value.start_book_no +'</td>'+
                                            '<td>'+ value.end_book_no +'</td>'+
                                            '<td>'+ value.qty +'</td>'+
                                            '<td>'+ value.unit +'</td>'+
                                        '</tr>'

                            tbody.append(row);
                            });

                            $("#stockInDate").html(data[0].stock_in_date);
                            $("#refNo").html(data[0].reference_no);
                            $("#totalQty").html(data[0].total_book_qty.toLocaleString('en-US') + ' books = ' + data[0].total_ticket_qty.toLocaleString('en-US') + ' tickets');
                            $('#invoice_modal .modal-title').html('Invoice #'+data[0].reference_no)

                            var refNo = $('#refNo').text();
                            $('.generate-pdf').attr('href',$('.generate-pdf').data('route') + '?refNo=' + refNo)
                            $('.generate-excel').attr('href',$('.generate-excel').data('route') + '?reference_no=' + refNo)

                        }else{
                            swalOnResult('Error While Loading Information!','error');
                        }
                    },
                });
            });
        }

    });

    if($('.download-excel').length > 0){
        $('.download-excel').attr('href',$('.download-excel').data('route') + location.search)
    }

    var swalOnResult = function(title, type){
        swal({
            title: title,
            text: "You clicked the button!",
            icon: type,
            timer: 1500,
        });
    }



</script>

@endsection
