@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ isset($stock) ? 'Update' : 'Add' }} Stock</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('stocks.index')}}" class="breadcrumb-item">Stock</a>
                <span class="breadcrumb-item active">{{ isset($stock) ? 'Update' : 'Add' }} Stock</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    @if(isset($stock))
        {{ Form::model($stock,['route' => ['stocks.update',$stock['reference_no']], 'method' => 'PUT','id' => 'stockform']) }}
    @else
        {{ Form::open(['route' => 'stocks.store', 'method' => 'POST','id' => 'stockform']) }}
    @endif
    @csrf
    <div class="card">
        <div class="card-body">
            @include('includes.success-msg')
            @include('includes.error-msg')

            <div id="error-message"></div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Reference No</label>
                        <div class="col-lg-7">
                            <div class="position-relative">
                                {{
                                    Form::text("reference_no",isset($refNo) ? $refNo : (isset($stock) ? $stock['reference_no'] : ''),
                                        ["class" => "form-control ".($errors->has('reference_no') ? 'border-danger' : ''),"placeholder" => "",'readonly'])
                                }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Stock In Date</label>
                        <div class="col-lg-7">
                            <div class="position-relative">
                                {{
                                    Form::text("stock_in_date",isset($stock) ? $stock['stock_in_date'] : date('Y-m-d H:i:s'),
                                        ["class" => "form-control ".($errors->has('reference_no') ? 'border-danger' : ''),"placeholder" => "",'readonly'])
                                }}
                            </div>
                        </div>
                    </div>
                    @if(!isset($stock) || auth()->user()->can('stock-modification'))
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Box Serial Code <span class="text-danger">*</span></label>
                        <div class="col-lg-7">
                            <div class="input-group">
                                {{Form::text("box_barcode",isset($product) ? $product->box_barcode : '',
                                        ["class" => "form-control barcode".($errors->has('box_barcode') ? 'border-danger' : ''),"placeholder" => ""])}}

                                <span class="input-group-prepend">
                                    <button class="btn btn-success" type="button" id="btnAdd" data-route =" {{ route('box.barcode') }}" ><i class="icon-add"></i> Add</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>

            </div>
        </div>
        <div class="card-header">
            <div class="table-responsive">
                <table class="table table-bordered" id="table">
                    <thead>
                        <tr class="bg-slate-800">
                            <th>#</th>
                            <th>Game Code</th>
                            <th>Batch No</th>
                            <th>Box Code</th>
                            <th>Start Book No</th>
                            <th>End Book No</th>
                            <th>Qty</th>
                            <th>Unit</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                        @if(isset($stock))
                            <tbody id="tableBody">
                                @foreach($stock['data'] as $index => $row)
                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $row->game_code }} – {{ $row->product ? $row->product->name : ''}}</td>
                                        <td>{{ $row->batch_no}}</td>
                                        <td>{{ $row->box_code}}</td>
                                        <td>{{ $row->start_book_no}}</td>
                                        <td>{{ $row->end_book_no}}</td>
                                        <td>{{ $row->book_qty}}</td>
                                        <td>{{ $row->product_unit ? $row->product_unit->name : ''}}</td>
                                        <td class="group-btn-action">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-outline bg-primary border-primary text-primary-800 btn-icon view-book-list"
                                                    data-route="{{ route('stocklog.get-book-list') }}"
                                                    data-game_code="{{ $row->game_code}}" data-batch_no="{{ $row->batch_no}}" data-box_code="{{ $row->box_code}}"
                                                    data-toggle="modal" data-target="#book_list">
                                                    <i class="icon-eye"></i> View
                                                </button>

                                                @can('stock-modification')
                                                <button type="button" class="btn btn-sm btn-outline bg-danger border-danger text-danger-800 btn-icon border-2 remove server"
                                                    data-game_code="{{ $row->game_code}}" data-batch_no="{{ $row->batch_no}}" data-box_code="{{ $row->box_code}}" data-start_book_no="{{ $row->start_book_no}}" data-end_book_no="{{ $row->end_book_no}}">
                                                    <i class="icon-trash"></i> Remove
                                                </button>
                                                @endcan
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        @else
                            <tbody id="tableBody"></tbody>
                        @endif
                </table>
            </div>
        </div>

        @canany(['stock-modification','add-new-stock'])
        <div class="card-footer">
            <button type="button" class="btn btn-success" id="submitStock">
                <i class="icon-folder mr-1"></i> {{ isset($product) ? 'Update' : 'Save'}}
            </button>
        </div>
        @endcanany

        <input type="hidden" name="action" value="{{isset($stock) ? 'update' : 'add' }}">
        @if(isset($stock))
            <input type="hidden" name="removeBoxBarcodes">
        @endif
    </div>
    {{ Form::close() }}
</div>

<div id="book_list" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">Book List</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered book-list">
                    <thead>
                        <tr class="bg-slate-800">
                            <th>#</th>
                            <th>Game Code</th>
                            <th>Serial Code</th>
                            <th>Book No</th>
                            <th>Ticket Qty</th>
                            <th>Ticket Price</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')

<script>
    $(document).ready(function(){
        automateStockIn();
        $("input[name='box_barcode']").focus();

        $("input[name='box_barcode']").on("keyup", function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                $('#btnAdd').click();
            }
        });

        $('#btnAdd').click(function(){
            var url = $(this).data('route');
            var barcode = $("input[name='box_barcode']").val();


            if($("input[name='boxBarcodes[]']").length > 0){
                var boxBarcodes = $("input[name='boxBarcodes[]']").map(function(){return $(this).val();}).get()

                if($.inArray(barcode,boxBarcodes) != -1){
                    showErrorMessage('Oh snap! This product code was already added! Please try a new one.')
                    return;
                }
            }
            $.ajax({
                type : 'GET',
                url : url,
                data : { barcode },
                dataType: 'json',
                success:function(response){

                    if(response.success){
                        var product = response.data;
                        var tbody = $('#tableBody');
                        var row = '<tr>'+
                                    '<td>'+ parseInt(tbody.children('tr').length + 1) +'</td>'+
                                    '<td>'+ product.game_code + '–' + product.game_name + '</td>'+
                                    '<td>'+ product.batch_no +'</td>'+
                                    '<td>'+ product.box_code +'</td>'+
                                    '<td>'+ product.start_book_no +'</td>'+
                                    '<td>'+ product.end_book_no +'</td>'+
                                    '<td>'+ product.book_qty +'</td>'+
                                    '<td>'+ product.unit +'</td>'+
                                    '<td class="group-btn-action">'+
                                        '<div class="btn-group">'+

                                            '<button type="button" class="btn btn-sm btn-outline bg-danger border-danger text-danger-800 btn-icon border-2 remove "><i class="icon-trash"></i> Remove</button>'+
                                        '</div> '+
                                        '<input type ="hidden" name= "boxBarcodes[]" value="'+product.code+'">'
                                    '</td>'+
                                '</tr>'

                        tbody.append(row);
                        $("input[name='box_barcode']").val('')

                    }else{
                        showErrorMessage('Oh snap! '+response.message)
                    }

                }
            });


        });

        $(document).on("click",".remove",function() {

            if($(this).hasClass('server')){
                var game_code = $(this).data('game_code');
                var batch_no = $(this).data('batch_no');
                var box_code = $(this).data('box_code');
                var start_book_no = $(this).data('start_book_no');
                var end_book_no = $(this).data('end_book_no');

                var current_value = $("input[name='removeBoxBarcodes']").val()
                var value = current_value ? JSON.parse(current_value) : [];
                value.push({
                    game_code: game_code,
                    batch_no: batch_no,
                    box_code: box_code,
                    start_book_no: start_book_no,
                    end_book_no: end_book_no
                });

                $("input[name='removeBoxBarcodes']").val(JSON.stringify(value))

            }
            $(this).parents('tr').remove();
        });

        $('#submitStock').click(function(e){
            e.preventDefault();
            if(($("input[name='boxBarcodes[]']").length > 0 || $("input[name='removeBoxBarcodes']").val() != '') ){
                $("#stockform").submit();
            }else{
                if($("input[name='removeBoxBarcodes']").val() == ''){
                    showErrorMessage('Oh snap! No update changes!!!')
                }else{
                    showErrorMessage('Oh snap! No content added!!!')
                }
            }

        });

        function showErrorMessage(msg){
            var element = '<div class="alert bg-danger text-white alert-styled-left alert-dismissible" style="display:none;">' +
                            '<button type="button" class="close" data-dismiss="alert"><span>×</span></button>'+
                            '<span class="font-weight-semibold" id="error-message">'+msg+'</span>'+
                            '</div>'

            $('#error-message').html(element);
            $('.alert').css('display', 'block');
            setTimeout(function(){
                $('.alert').css('display', 'none');
            },3000)

        }

        function automateStockIn(){
            var game_code = 'P0001';
            var batch = '19001';
            var step = 50;
            var s_book_no = 1;

            for(var i = 1; i<=200;i++){
                var box = i.toString().padStart(5, '0');
                var s_book = s_book_no.toString().padStart(7,'0');
                var e_book = (parseInt(s_book_no) + parseInt(step) - 1).toString().padStart(7,'0');
                s_book_no = parseInt(s_book_no) + parseInt(step);

                var barcode = game_code + batch + box + s_book + e_book;

                $.ajax({
                    type : 'GET',
                    url : '/box/bar-code',
                    data : { barcode },
                    dataType: 'json',
                    success:function(response){

                        if(response.success){
                            var product = response.data;
                            var tbody = $('#tableBody');
                            var row = '<tr>'+
                                        '<td>'+ parseInt(tbody.children('tr').length + 1) +'</td>'+
                                        '<td>'+ product.game_code + '–' + product.game_name + '</td>'+
                                        '<td>'+ product.batch_no +'</td>'+
                                        '<td>'+ product.box_code +'</td>'+
                                        '<td>'+ product.start_book_no +'</td>'+
                                        '<td>'+ product.end_book_no +'</td>'+
                                        '<td>'+ product.book_qty +'</td>'+
                                        '<td>'+ product.unit +'</td>'+
                                        '<td class="group-btn-action">'+
                                            '<div class="btn-group">'+

                                                '<button type="button" class="btn btn-sm btn-outline bg-danger border-danger text-danger-800 btn-icon border-2 remove "><i class="icon-trash"></i> Remove</button>'+
                                            '</div> '+
                                            '<input type ="hidden" name= "boxBarcodes[]" value="'+product.code+'">'
                                        '</td>'+
                                    '</tr>'

                            tbody.append(row);
                            $("input[name='box_barcode']").val('')

                        }else{
                            showErrorMessage('Oh snap! '+response.message)
                        }

                    }
                });
            }
        }
    });

    $(document).ready(function(){

            if($('#book_list').length > 0){
                 $(document).on("click",".view-book-list",function(){
                    $.ajax({
                        type:'GET',
                        url:$(this).data('route'),
                        dataType: "JSON",
                        data: {
                            game_code: $(this).data('game_code'),
                            batch_no: $(this).data('batch_no'),
                            box_code: $(this).data('box_code')
                        },
                        success:function(response){
                            if(response.success){
                                var data = response.data;
                                var tbody = $('.table.book-list tbody');
                                tbody.html('')
                                $.each(data, function( index, value ) {
                                    var no = index + 1;
                                    var row = '<tr>'+
                                                '<td>'+ no +'</td>'+
                                                '<td>'+ value.game_code + '<br>' +  value.product.name +'</td>'+
                                                '<td>'+ value.code +'</td>'+
                                                '<td>'+ value.book_no +'</td>'+
                                                 '<td>'+ value.ticket_qty +'</td>'+
                                                  '<td>R '+ (value.ticket_price).toFixed(2) +'</td>'+
                                            '</tr>'

                                    tbody.append(row);
                                });

                            }else{
                                swalOnResult('Error While Loading Information!','error');
                            }
                        },
                    });
                });
            }
        });
</script>
@endsection

