@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Scratch Card Award</span></h4>
        </div>

        @can('import-award-data')
        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <button class="btn btn-link btn-primary btn-sm text-white" id="importExcelButton" 
                data-toggle="modal" data-target="#importModal">
                    <i class="icon-file-excel text-white mr-1"></i>Import Data 
                </button>
            </div>  
        </div>
        @endcan

    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Scratch Card Data</span>
                <span class="breadcrumb-item active">Award Data</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        <div class="card-header">
            <div class="mb-1">
                <button class="btn btn-sm btn-primary" type="button" data-toggle="collapse" data-target="#filter" aria-expanded="false" aria-controls="collapseExample">
                    <i class="icon-filter3"> </i> Filter
                </button> 
                
                <div class="collapse mt-2" id="filter"> 
                    {{ Form::open(['route' => 'awards.index', 'method' => 'GET']) }}
                        <div class="row mb-2">
                            <div class="col-lg-2 col-md-3 col-sm-6">
                                <label for="">Game Code</label>
                                {{Form::text("game_code",request()->game_code,["class" => "form-control "])}}
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-6">
                                <label for="">Batch No</label>
                                {{Form::text("batch_no",request()->batch_no,["class" => "form-control "])}}
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-6">
                                <label for="">Book No</label>
                                {{Form::text("book_no",request()->book_no,["class" => "form-control "])}}
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-6">
                                <label for="">Ticket No</label>
                                {{Form::text("ticket_no",request()->ticket_no,["class" => "form-control "])}}
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-6">
                                <label for="">Award Level</label>
                                {{Form::number("award_level",request()->award_level,["class" => "form-control "])}}
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-6">
                                <label for="">Award Status</label>
                                <select name="is_claimed" class="form-control">
                                    <option value="any" >Any</option>
                                    <option value="0" {{ request()->is_claimed == '0' ? 'selected' : ''}}>Available</option>
                                    <option value="1" {{ request()->is_claimed == '1' ? 'selected' : ''}}>Claimed</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-lg-3 col-md-5 col-sm-6">
                                <label for="">Security Code</label>
                                {{Form::text("security_code",request()->security_code,["class" => "form-control "])}}
                            </div>
                            <div class="col-lg-3 col-md-5 col-sm-6">
                                <label for="">Logistic Code</label>
                                {{Form::text("logistic_code",request()->logistic_code,["class" => "form-control "])}}
                            </div>
                        </div>
                        <button type="submit" class="btn btn-sm btn-success">Search</button>
                        <a href="{{route('awards.index')}}" class="btn btn-sm btn-danger">Reset</a>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-slate-800">
                            <th>#</th>
                            <th>Game Code</th>
                            <th>Batch Number</th>
                            <th>Book Number</th>
                            <th>Ticket Number</th>
                            {{-- <th>Award Level</th>
                            <th>Award Amount</th> --}}
                            <th>Status</th>
                            <th>Claimed By</th>
                            <th>Scanned At</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($awards) && $awards->count() > 0)
                            @foreach($awards as $key => $row)
                            <tr class="{{ $row->is_claimed ? 'table-danger' : ''}}">
                                <td>{{($awards->perPage() * ($awards->currentPage() - 1)) + $key + 1}}</td>
                                <td>{{ $row->game_code }}</td>
                                <td>{{ $row->batch_no }}</td>
                                <td>{{ $row->book_no }}</td>
                                <td>{{ $row->ticket_no }}</td>
                                {{-- <td>{{ $row->award_level }}</td>
                                <td>{{ $row->award_amount }}</td> --}}
                                <td>
                                    @if($row->is_claimed == 1)
                                        <span class="badge badge-danger">Claimed</span>
                                    @else 
                                        <span class="badge badge-success">Available</span>
                                    @endif
                                </td>
                                <td>{{ $row->user ? $row->user->username : 'N/A' }}</td>
                                <td>{{ $row->scanned_at }}</td>
                            </tr>
                            @endforeach
                        @else 
                            <tr>
                                <td colspan="9"> No data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            @if(isset($awards) && count($awards) > 0)
            <div class="card-footer">
                @if($awards->hasPages())
                    <div class="mb-2">
                        {!! $awards->appends(Input::except('page'))->render() !!}
                    </div>
                @endif
                <div>
                    Showing {{$awards->firstItem()}} to {{$awards->lastItem()}}
                    of  {{$awards->total()}} entries
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
<div id="importModal" class="modal fade" tabindex="-1" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">Import Award Data</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="importModalBody">
                <p>Upload excel file with extension (.xlsx) below and click Import button.</p>
                <form id="importUploadForm" method="POST" enctype='multipart/form-data'>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <input type="file" name="file" class="form-control-uniform" data-fouc>
                        </div>
                    </div>
                </form>
                <span class="text-danger error-message"></span>
            </div>
            <div class="modal-footer">
                <button type="button" id="importBtn" data-route="{{ route('awards.store') }}" class="btn btn-success float-right one-click-btn">Start Import</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/demo_pages/form_inputs.js"></script>
<script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
@endsection
